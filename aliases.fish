## Useful aliases
### Replacemnt
# Remplacement de vim par néovim
	alias vim='$EDITOR'

# Replace ls with exa
	alias ls='eza -@aalxhF --color-scale=all --color=always --git --header --icons --octal-permissions --group-directories-first' 		# liste au format long
	alias la='eza -@aaxhF --color-scale=all --color=always --git --header --icons --octal-permissions --group-directories-first' 		# grille avec les fichiers cachés
	alias ll='eza -x --color=always --git --icons --octal-permissions --group-directories-first' 									# grille sans les fichiers cachés
	# alias l.="ls | rg '[[:digit:]]{2}:[[:digit:]]{2}.{3,}[[:blank:]]*\..*/\$'\|'[[:digit:]]{2}:[[:digit:]]{2}.*[[:blank:]]\..*\$'" 	# liste uniquement les fichiers cachés
	alias ld='eza -@aabghHlmiSxUuF --color-scale --color=always --git --header --icons --octal-permissions --group-directories-first' 	# liste avec de nombreux détails
	alias lt1='lt 1' 																												# liste en arborescence sur 1 niveau
# Replace cd with z
#	alias cd='z'
	alias cdi='zi'

# Replace grep with ripgrep
	alias grep='rg --color=always --column'

# Replace du with dust
	alias du='dust'

# Replace df with dfc -dsT
	alias dfa='dfc -dsT'

# Replace df with dfrs
	alias df='dfrs -av --total'

# Replace df with dfrs
	alias dfb='dfrs -av --total'

# Replace find with fd
	alias find="fd"

# Replace mc with ranger
	alias mc="ranger"

# Replace rm with rip
	alias rm='rip'

# Replace watch with viddy
	alias watch='viddy --pty --shell fish'

# Replace bottom with more parameters
	alias btm='btm --enable_gpu --battery -W'

# Replace some more things with better alternatives
	alias cat='bat --style header --style rule --style snip --style changes --style header'
	[ ! -x /usr/bin/yay ] && [ -x /usr/bin/paru ] && alias yay='paru'

# Directly modify the config.fish and aliases.fish files
	alias confish="vim ~/.config/fish/config.fish"
	alias confali="vim ~/.config/fish/aliases.fish"

# Colorize some commands
	alias ip="ip -color"

# Common use
	alias grubup="sudo update-grub"
	alias fixpacman="sudo rm /var/lib/pacman/db.lck"
	alias clearpacmancache="sudo pacman -Sccv"
	alias tarnow='tar -acf '
	alias untar='tar -xvf '
	alias wget='wget -c '
	alias rmpkg="sudo pacman -Rdd"
	alias psmem='ps auxf | sort -nr -k 4'
	alias psmem10='ps auxf | sort -nr -k 4 | head -10'
	alias upd='/usr/bin/update --skip-mirrorlist'
	alias yain="paru -S"
	alias yarem="paru -R"
	alias yaupd="paru -Suy"
	alias yaupg="paru -Suuya"
	alias ..='cd ..'
	alias ...='cd ../..'
	alias ....='cd ../../..'
	alias .....='cd ../../../..'
	alias ......='cd ../../../../..'
	alias dir='dir --color=auto'
	alias vdir='vdir --color=auto'
	alias ag='ag --filename --heading --numbers --color-line-number "1;5;33" --color-match "1;7;30;41" --color-path "3;32"'
	alias agrep='ag --filename --numbers --color-line-number "1;5;33" --color-match "1;7;30;41" --color-path "3;32"'
	#alias egrep='egrep --color=auto'
	alias egrep='grep -E --color=auto'
	#alias fgrep='fgrep --color=auto'
	alias fgrep='grep -F --color=auto'
	alias grep='grep --color=auto'
	alias rg='rg --color always'
	alias hw='hwinfo --short'                          # Hardware Info
	alias big="expac -H M '%m\t%n' | sort -h | nl"     # Sort installed packages according to size in MB
	alias gitpkg='pacman -Q | grep -i "\-git" | wc -l' # List amount of -git packages
	# alias search='sk --ansi -i -c "ag -u --stats --color {}" --delimiter : --preview "bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}" --preview-window +{2}-/2'
	# alias search='ag --color -u $argv[2] | sk --ansi -i --delimiter : --preview "bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}" --preview-window +{2}-/2'

# Get fastest mirrors
	alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
	alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
	alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
	alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

# Help people new to Arch
	alias apt='man pacman'
	alias apt-get='man pacman'
	alias please='sudo'
	alias tb='nc termbin.com 9999'

# Cleanup orphaned packages
	alias cleanup='sudo pacman -Rns (pacman -Qtdq)'

# Get the error messages from journalctl
	alias jctl="journalctl -p 3 -xb"

# Recent installed packages
	alias ripkg="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -200 | nl"

# Python Lib aliases
	alias deepl="python -m deepl"
	alias deeplFRES="python -m deepl FR ES -t"
	alias deeplFREN="python -m deepl FR EN -t"
	alias deeplENFR="python -m deepl EN FR -t"
	alias deeplESFR="python -m deepl ES FR -t"
	alias tradcFRES="tradc FR ES"
	alias tradcESFR="tradc ES FR"
	alias tradcENFR="tradc EN FR"
	alias tradcFREN="tradc FR EN"
	alias trad="tradc EN FR"

# SSH
#	alias ssh-agent="fish_ssh_agent"

# Plasmashell
	alias restart-plasma="killall plasmashell > ~/plasma-restart.log 2>&1 && kstart plasmashell >> ~/plasma-restart.log 2>&1"

# Différentes informations
	alias mypublicip4='gettext -e "$scb\aIPv4: $scyo$(curl -s -4 ifconfig.me)$scn"'
	alias mypublicip6='gettext -e "$scb\aIPv6: $scbo$(curl -s -6 ifconfig.me)$scn"'
	alias ip4='gettext -e "$scyo$(curl -s -4 ifconfig.me)"'
	alias ip6='gettext -e "$scbo$(curl -s -6 ifconfig.me)"'
	#alias mypubips='echo -e "IP4: $(curl -s -4 ifconfig.me)\nIP6: $(curl -s -6 ifconfig.me)"'
