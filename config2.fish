## Set values
# Hide welcome message
set fish_greeting
set VIRTUAL_ENV_DISABLE_PROMPT "1"
set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

## Export variable need for qt-theme
if type "qtile" >> /dev/null 2>&1
   set -x QT_QPA_PLATFORMTHEME "qt5ct"
end

# Set settings for https://github.com/franciscolourenco/done
set -U __done_min_cmd_duration 10000
set -U __done_notification_urgency_level low


## Environment setup
# Apply .profile: use this to put fish compatible .profile stuff in
if test -f ~/.fish_profile
  source ~/.fish_profile
end

# Add ~/.local/bin to PATH
if test -d ~/.local/bin
    if not contains -- ~/.local/bin $PATH
        set -p PATH ~/.local/bin
    end
end

# Add depot_tools to PATH
if test -d ~/Applications/depot_tools
    if not contains -- ~/Applications/depot_tools $PATH
        set -p PATH ~/Applications/depot_tools
    end
end


## Starship prompt
if status --is-interactive
   source ("/usr/bin/starship" init fish --print-full-init | psub)
end


## Advanced command-not-found hook
source /usr/share/doc/find-the-command/ftc.fish


## Functions
# Functions needed for !! and !$ https://github.com/oh-my-fish/plugin-bang-bang
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end

if [ "$fish_key_bindings" = fish_vi_key_bindings ];
  bind -Minsert ! __history_previous_command
  bind -Minsert '$' __history_previous_command_arguments
else
  bind ! __history_previous_command
  bind '$' __history_previous_command_arguments
end

# Fish command history
function history
    builtin history --show-time='%F %T '
end

function backup --argument filename
    cp $filename $filename.bak
end

# Copy DIR1 DIR2
function copy
    set count (count $argv | tr -d \n)
    if test "$count" = 2; and test -d "$argv[1]"
	set from (echo $argv[1] | trim-right /)
	set to (echo $argv[2])
        command cp -r $from $to
    else
        command cp $argv
    end
end

## Useful aliases
# Replace ls with exa
alias ls='exa -@alxFh --color-scale --color=always --git --header --icons --octal-permissions --group-directories-first' 		# liste au format long
alias la='exa -@axFh --color-scale --color=always --git --header --icons --octal-permissions --group-directories-first' 		# grille avec les fichiers cachés
alias ll='exa -x --color=always --git --icons --octal-permissions --group-directories-first' 									# grille sans les fichiers cachés
alias l.="ls | rg '[[:digit:]]{2}:[[:digit:]]{2}.{3,}[[:blank:]]*\..*/\$'\|'[[:digit:]]{2}:[[:digit:]]{2}.*[[:blank:]]\..*\$'" 	# liste uniquement les fichiers cachés
alias ld='exa -@abFghHlmiSxUu --color-scale --color=always --git --header --icons --octal-permissions --group-directories-first' 	# liste avec de nombreux détails
alias lt1='lt 1' 																												# liste en arborescence sur 1 niveau

function lt --argument-names cmd_args --description "Commande ls avec de nombreux détails et en arborescence."
	# Parse des options CLI
	argparse v/version h/help n/nonall -- $argv
	or return
	
	# Définition des variables
	set --local lt_version 	1.0 										# Définition du numéro de la version
	set --local bl 			55BBFF 										# Définition de la couleur bleu
	set --local scn 		(set_color normal) 							# Définir le formatage du texte en normal
	set --local scu 		(set_color normal)(set_color -u) 			# Définir le formatage du texte en souligné
	set --local sci 		(set_color normal)(set_color -i) 			# Définir le formatage du texte en italique
	set --local sco 		(set_color normal)(set_color -o) 			# Définir le formatage du texte en gras
	set --local scb 		(set_color normal)(set_color $bl) 			# Définir le formatage du texte en bleu
	set --local scbi 		(set_color normal)(set_color -i $bl) 		# Définir le formatage du texte en bleu italique
	set --local scbu 		(set_color normal)(set_color -u $bl) 		# Définir le formatage du texte en bleu souligné
	set --local scbo 		(set_color normal)(set_color -o $bl) 		# Définir le formatage du texte en bleu gras
	set --local scr 		(set_color normal)(set_color red) 			# Définir le formatage du texte en rouge
	set --local scri 		(set_color normal)(set_color -i red) 		# Définir le formatage du texte en rouge italique
	set --local desc 		"Commande $sco\als$scn via sa réecriture $sci\aRUST$scn $sco\aexa$scn avec de nombreux détails, \nen arborescence avec la possibilité d'afficher les fichiers cachés \nainsi que de choisir la profondeur d'exploration récursive des dossiers."
	set lsn "exa -@lxFhT --color-scale --color=always --header --icons --octal-permissions --group-directories-first"
	set lst "exa -a@lxFhT --color-scale --color=always --header --icons --octal-permissions --group-directories-first"
		
	if set -q _flag_help
		gettext -e "$sco\aDescription :$scn\n"
		gettext -e "$desc\n"
		gettext -e "$sco\aUsage :$scn\n"
		gettext -e "\t$scb\alt$scn 			→ Liste des fichiers et dossiers (dont les fichiers cachés et liens) avec une arboresence complète\n" 
		gettext -e "\t$scb\alt $scbu{0..n}$scn 		→ Liste des fichiers et dossiers (dont les fichiers cachés et liens) avec une arboresence sur $scu\an$scn niveau de profondeur\n" 
		gettext -e "\t$scb\alt $scbu{0..n}$scn $scbu{/chemin/}$scn 	→ Liste des fichiers et dossiers (dont les fichiers cachés et liens) avec une arboresence sur $scu\an$scn niveau depuis le chemin $scu\a/chemin/$scn\n" 
		gettext -e "\t$scb\alt $scbi-n$scn 			→ Liste des fichiers et dossiers sans les fichiers cachés avec une arboresence complète\n" 
		gettext -e "\t$scb\alt $scbi-n $scbu{0..n}$scn 		→ Liste des fichiers et dossiers sans les fichiers cachés avec une arboresence sur $scu\an$scn niveau de profondeur\n"
		gettext -e "\t$scb\alt $scbi-n $scbu{0..n}$scn $scbu{/chemin/}$scn → Liste des fichiers et dossiers sans les fichiers cachés avec une arboresence sur $scu\an$scn niveau depuis le chemin $scu\a/chemin/$scn\n"
		gettext -e "\t$scb\alt $scbi{-h|--help}$scn 		→ Affiche cette aide\n"
		gettext -e "\t$scb\alt $scbi{-v|--version}$scn 	→ Affiche la version du script\n"
		return 1
	else if set -q _flag_version
		gettext -e "La version de ce script est : $lt_version"
		return 1
	else if set -q _flag_nonall
		set lst $lsn
		echo "position 1 : $lst"
	end
	
	echo "argv1 : $argv et le total d'argument est : $(count $argv)"

	if test (count $argv) -ge 2
		if command test $argv[1] -ge 0 -a -e $argv[2]
			echo "argv1 est un chiffre et argv2 est un fichier qui existe"
			eval $lst -L=$argv[1] $argv[2]
		else
			echo "erreur dans l'ordre des argument, voir ltest --help"
			return 1
		end
	else if command test -d $argv -a $argv -ge 0 &>/dev/null
		echo "$(set_color red)\$argv::\"$argv\"$(set_color yellow) est un chiffre et un dossier, veuillez préciser si :"
		while true
			read -l -P $(printf "\t$scb\aVous voulez préciser la $scbu\aprofondeur de la liste$scb [$scbo\aP$scb] ou $scbu\aouvrir le dossier$scb [$scbo\ad$scb] ! [$scbo\aP$scb/d] ") confirm
			switch $confirm
				case d D
					eval $lst $argv
					break
				case "" p P
					eval $lst -L=$argv
					break
			end
		end
	else if command test $argv -ge 0 &>/dev/null
		echo "argv est un chiffre"
		eval $lst -L=$argv
	else if command test -e $argv &>/dev/null
		echo "argv est un fichier qui existe"
		echo $stderr $stdin $stdout
		eval $lst $argv
	else
		gettext -e "lt :$scr Option inconnue : \"$cmd_args\"$scri\nVoir l'aide :$scb lt$scbi --help$scn" >&2
		return 1
	end
end
	
	

# Replace grep with ripgrep
alias grep='rg --color=always --column'

# Replace du with dust
alias du='dust'

# Replace find with fd
alias find='fd'

# Replace mc with ranger

# Colorize some commands
alias ip="ip -color"

# Replace some more things with better alternatives
alias cat='bat --style header --style rule --style snip --style changes --style header'
[ ! -x /usr/bin/yay ] && [ -x /usr/bin/paru ] && alias yay='paru'

# Common use
alias grubup="sudo update-grub"
alias fixpacman="sudo rm /var/lib/pacman/db.lck"
alias tarnow='tar -acf '
alias untar='tar -xvf '
alias wget='wget -c '
alias rmpkg="sudo pacman -Rdd"
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'
alias upd='/usr/bin/update'
alias yain="paru -S"
alias yarem="paru -R"
alias yaupd="paru -Suy"
alias yaupg="paru -Suuya"
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias rg='rg --color always'
alias hw='hwinfo --short'                          # Hardware Info
alias big="expac -H M '%m\t%n' | sort -h | nl"     # Sort installed packages according to size in MB
alias gitpkg='pacman -Q | grep -i "\-git" | wc -l' # List amount of -git packages

# Get fastest mirrors
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

# Help people new to Arch
alias apt='man pacman'
alias apt-get='man pacman'
alias please='sudo'
alias tb='nc termbin.com 9999'

# Cleanup orphaned packages
alias cleanup='sudo pacman -Rns (pacman -Qtdq)'

# Get the error messages from journalctl
alias jctl="journalctl -p 3 -xb"

# Recent installed packages
alias rip="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -200 | nl"

# Python Lib aliases
alias deepl="python -m deepl"
alias deeplFRES="python -m deepl FR ES -t"
alias deeplFREN="python -m deepl FR EN -t"
alias deeplENFR="python -m deepl EN FR -t"
alias deeplESFR="python -m deepl ES FR -t"
alias tradcFRES="tradc FR ES"
alias tradcESFR="tradc ES FR"
alias tradcENFR="tradc EN FR"
alias tradcFREN="tradc FR EN"

# Useful functions

# * Fonction utilisant le bibliothèque Python stego-lsb et  permettant de cacher un document dans une image par stéganographie.
function stegimg --argument-names cmd_args --description "Cacher un document dans une image par stéganographie"
	set --local stegimg_version	1.0
	set --local desc			"Cacher un document dans une image par stéganographie"
	set --local plib			stego-lsb
	set --local pcmd			stegolsb
	set --local img				"image"
	set --local src				"fichier source"
	set --local out				"fichier de destination"
	set --local ptf				"chemin d'accès"
	set --local scvio			(set_color normal)(set_color -oi 8CFF5A)	# Définir la couleur du texte en gras, italique et vert
	set --local scviu			(set_color normal)(set_color -ui 8CFF5A)	# Définir la couleur du texte en souligné, italique et vert
	set --local scov			(set_color normal)(set_color -o 8CFF5A)		# Définir la couleur du texte en gras et vert
	set --local scvi			(set_color normal)(set_color -i 8CFF5A)		# Définir la couleur du texte en italique et vert
	set --local scv				(set_color normal)(set_color 8CFF5A)		# Définir la couleur du texte en normal et vert
	set --local scoj			(set_color normal)(set_color -o FFF454)		# Définir la couleur du texte en gras et jaune
	set --local scij			(set_color normal)(set_color -i FFF454)		# Définir la couleur du texte en gras et jaune
	set --local scj				(set_color normal)(set_color FFF454)		# Définir la couleur du texte en normal et jaune
	set --local scob			(set_color normal)(set_color -o 323de2)		# Définir la couleur du texte en gras et bleu
	set --local scb				(set_color normal)(set_color 323de2)		# Définir la couleur du texte en normal et bleu
	set --local scoio			(set_color normal)(set_color -i B6AE5B)		# Définir la couleur du texte en italique et couleur gras
	set --local scon			(set_color normal)(set_color -o eec49a)		# Définir la couleur du texte en gras et normal
	set --local sco				(set_color normal)(set_color -o)			# Définir la couleur du texte en gras et gras
	set --local scou			(set_color normal)(set_color -ou)			# Définir la couleur du texte en gras et souligné
	set --local scui			(set_color normal)(set_color -ui)			# Définir la couleur du texte en souligné et italique
	set --local sci				(set_color normal)(set_color -i)			# Définir la couleur du texte en italique
	set --local scu				(set_color normal)(set_color -u)			# Définir la couleur du texte en souligné
	set --local scn				(set_color normal)							# Définir la couleur du texte en normal


	# TODO
	#	[ ] Ajouter une option qui permette de d'abord chiffrer le fichier source avant d'effectuer la stéganographie
	#	[x] Ajouter la completion des options
	#	[ ] Ajouter une option pour le nombre de LSB
	#	[ ] Ajouter le support i18n
	#	[ ] Supprimer les lignes commentées inutiles
	#	[ ] Rendre globales certaines des variables raccourcies pour $(set_color)


	# Parse CLI options
	while true
		switch "$cmd_args"
			case -v --version
				gettext -e "La version de ce script est : $stegimg_version"
				return 1
			case "" -h --help
				gettext -e "$scoio\aCe script permet de$scn : $desc via la librairie python \"stego-lsb\"\n"
				gettext -e "\tPour l'utiliser, veuillez d'abord $scoio\apréciser le type de manipulation à effectuer$scn, à savoir :\n"
				gettext -e "\t\t $scoj-t$scij ou$scoj --test$scn pour tester si$scb l'$img$scn est assez grande pour cacher le$scv $src$scn;\n"
#				gettext -e "\t\t $scoj-t$scij,$scoj --teste$scij ou$scoj --test$scn pour tester si$scb l'$img$scn est assez grande pour cacher le$scv $src$scn;\n"
				gettext -e "\t\t $scoj-c$scij ou$scoj --cache$scn pour effectuer l'opération précédente et cacher le$scv $src$scn;\n"
#				gettext -e "\t\t $scoj-c$scij,$scoj --cache$scij,$scoj -H$scij ou$scoj --hidden$scn pour effectuer l'opération précédente et cacher le$scv $src$scn;\n"
				gettext -e "\t\t $scoj-r$scij ou$scoj --recup$scn pour récupérer le $src caché dans une$scb $img$scn;\n"
#				gettext -e "\t\t $scoj-r$scij,$scoj --recup$scij ou$scoj --recover$scn pour récupérer le $src caché dans une$scb $img$scn;\n"
				gettext -e "\t\t $scoj-d$scij ou$scoj --detect$scn afin de tester un fichier et détecter s'il contient des données stéganographiques.\n"
				gettext -e "\tSuivi du $ptf vers le$scv fichier d'origine (l'$img)$scn suivie du$scv fichier à cacher ($src)$scn et enfin, du$scv $out$scn\n"
#				gettext -e "\tUne dernière option facultative permet de préciser le nombre de LSB concernés par la stéganographie.\n"

				gettext -e "\n$scoUsage :$scn\n"

				gettext -e "$scou\tPour tester simplement un fichier :\n$scn"
				gettext -e "$scon\t\tstegimg $scij{$scoj-t$scij,$scoj--test}$scn $scv\${$scviu$ptf vers l'$img$scv} \${$scviu$ptf vers le $src$scv}$scn\n"
#				gettext -e "$scon\t\tstegimg $scij{$scoj-t$scij,$scoj--teste$scij,$scoj--test}$scn $scv\${$scviu$ptf vers l'$img$scv} \${$scviu$ptf vers le $src$scv}$scn\n"
				gettext -e "$scou\tPour cacher un fichier dans une $img :$scn\n"
				gettext -e "$scon\t\tstegimg $scij{$scoj-c$scij,$scoj--cache$scij} $scv\${$scviu$ptf vers l'$img$scv} \${$scviu$ptf vers le $src$scv} \${$scviu$ptf vers le $out$scv}\n"
#				gettext -e "$scon\t\tstegimg $scij{$scoj-c$scij,$scoj--cache$scij,$scoj-H$scij,$scoj--hidden$scij} $scv\${$scviu$ptf vers l'$img$scv} \${$scviu$ptf vers le $src$scv} \${$scviu$ptf vers le $out$scv}\n"
				gettext -e "$scou\tPour récupérer un $src caché dans une $img :$scn\n"
				gettext -e "$scon\t\tstegimg $scij{$scoj-r$scij,$scoj--recup$scij} $scv\${$scviu$ptf vers le $src$scv} \${$scviu$ptf vers le $out$scv}\n"
#				gettext -e "$scon\t\tstegimg $scij{$scoj-r$scij,$scoj--recup$scij,$scoj--recover$scij} $scv\${$scviu$ptf vers le $src$scv} \${$scviu$ptf vers le $out$scv}\n"
				gettext -e "$scou\tPour détecter la présence de données stéganographiques dans un fichier :$scn\n"
				gettext -e "$scon\t\tstegimg $scij{$scoj-d$scij,$scoj--detect$scij} $scv\${$scviu$ptf vers l'$img$scv}$scn\n"
				return 1
			case -t --teste --test
				set --local option 1
				break
			case -c --cache -H --hidden
				set --local option 2
				break
			case -r --recup -- recover
				set --local option 3
				break
			case -d --detect
				set --local option 4
				break
			case \*
				gettext -e "stegimg: $(set_color red)Option inconnue: \"$cmd_arg\"$(set_color -i red)\nVoir l'aide :$(set_color normal) $(set_color -i)stegimg --help$(set_color normal)" >&2 && return 1
		end
	end


	# Check if the stego-lsb python librairy and the command line interface to the X11 clipboard xclip exists, if not, install it
	if not python -c "import $pcmd" &> /dev/null
		echo La librairie python (set_color -ui)$plib(set_color normal) n\'a pas été trouvée, installation...
		pip install $plib
	end


#	while true
#		switch "$argv[1]"
#			case -t --teste --test
#				set --local option 1
#			case -c --cache -H --hidden
#				set --local option 2
#			case -r --recup -- recover
#				set --local option 3
#			case -d --detect
#				set --local option 4
#		end
#	end

#	if test $option = 1
#		stegolsb steglsb -a -i $argv[2] -s $argv[3] -n 2
#	else if test $option = 2
#		stegolsb steglsb -h -i $argv[2] -s $argv[3] -o $argv[4] -n 2 -c 1
#	else if test $option = 3
#		stegolsb steglsb -r -i $argv[2] -o $argv[2] -n 2
#	else if test $option = 4
#		stegolsb stegdetect -i $argv[2] -n 2
#	end

	if test $option = 1
		stegolsb steglsb -a -i $argv[1] -s $argv[2] -n 2
	else if test $option = 2
		stegolsb steglsb -h -i $argv[1] -s $argv[2] -o $argv[3] -n 2 -c 1
	else if test $option = 3
		stegolsb steglsb -r -i $argv[1] -o $argv[2] -n 2
	else if test $option = 4
		stegolsb stegdetect -i $argv[1] -n 2
	end

end

# * Fonction utilisant la bibliothèque Python deepl-translate et permettant de traduire un texte dans différents langages et de le copier par la même occasion dans le presse-papier.
function tradc --argument-names cmd_arg --description "Outil de traduction en ligne de commande utilisant la librairie python \"deepl-translate\" et recopiant le résultat dans le presse-papier."
	set --local tradc_version 1.0
	set --local supported_languages BG ZH CS DA NL EN ET FI FR DE EL HU IT JA LV LT PL PT RO RU SK SL ES SV
	# TODO
	# - Add the translation list of supported languages shortcut for the help
	# - Remove the code that is not used !
	# - Make a GUI alternative for tradc

	# Check if the deepl-translate python librairy and the command line interface to the X11 clipboard xclip exists, if not, install it
	if ! python -c "import deepl" &> /dev/null
		echo La librairie python (set_color -ui)deepl-translate(set_color normal) n\'a pas été trouvée, installation...
		pip install deepl-translate
	end

	# Check if the command line interface to the X11 clipboard xclip exists, if not, install it
	if ! type xclip &> /dev/null
		echo La commande (set_color -ui)xclip(set_color normal) n\'a pas été trouvée, installation \(veuillez rentrer votre mot-de-passe afin d\'excuter pacman\)...
		pkexec pacman -Sq --noconfirm xclip
	end

	# Parse CLI options
	while true
		switch "$cmd_arg"
			case -v --version
				gettext "La version de ce script est : $tradc_version"
				return 1
			case "" -h --help
				gettext -e "Ce script permet de traduire un texte via la librairie python \"deepl-translate\"\n"
				gettext -e "\tPour l'utiliser, veuillez d'abord préciser la langue d'origine suivie de la langue de destination, suivit du texte :\n"
				gettext -e "\tVous pouvez préciser les langues soit en entier, soit via une abbréviation avec deux caractères."
				gettext -e ""
				gettext -e "\nUsage :\ntradc \$origine_lang \$destination_lang \"Le texte à traduire\"\nLangages supportés : "
				echo -e $supported_languages\n\t\t"    "
				return 1
			case $supported_languages
				break
			case \*
				gettext -e "tradc: $(set_color red)Option inconnue: \"$cmd_arg\"$(set_color -i red)\nVoir l'aide :$(set_color normal) $(set_color -i)tradc --help$(set_color normal)" >&2 && return 1
		end
	end
	#	set args (getopt -s sh abc: $argv); or help_exit
	#	set args (fish -c "for el in $args; echo \$el; end")

	#	set PARSED_OPTIONS $(getopt --options="h,v" --longoptions="help,version" --name "$0" -- "$argv")
	#	set PARSED_OPTIONS $(argparse --name=tradc 'h/help' 'v/version' -- $argv; or return)
	#	if [[ $status -ne 0 ]]; then
	#		echo -e "\033[1;31m\nFailed to parse CLI options\n\033[0m"
	#	end
	#	eval set -- "$PARSED_OPTIONS"
	#	while true
	#		switch (eval $argv[1])
	#			case -h or --help
	#			    echo -e "Ce script permet de traduire un texte via la librairie python \"deepl-translate\""
	#				echo -e "\tPour l'utiliser, veuillez d'abord préciser la langue d'origine suivie de la langue de destination, suivit du texte :"
	#				echo -e "\tVous pouvez préciser les langues soit en entier, soit via une abbréviation avec deux caractères."
	#			case -v or --version
	#			    echo -e $version
	#			case  --
	#				break
	#		end
	#	end
	#	while true
	#	switch (eval $argv[1])
	#		case -h or --help
	#		    echo -e "Ce script permet de traduire un texte via la librairie python \"deepl-translate\""
	#			echo -e "\tPour l'utiliser, veuillez d'abord préciser la langue d'origine suivie de la langue de destination, suivit du texte :"
	#			echo -e "\tVous pouvez préciser les langues soit en entier, soit via une abbréviation avec deux caractères."
	#		case -v or --version
	#		    echo -e $version
	#		case  --
	#			break
	#		end
	#	end
	set traduction (python -m deepl $argv[1] $argv[2] -t $argv[3])
	if test $argv[1] = "FR"
		set lang1 "français"
	else if test $argv[1] = "EN"
		set lang1 "anglais"
	else if test $argv[1] = "ES"
		set lang1 "espagnol"
	else if test $argv[1] = "DE"
		set lang1 "allemand"
	else if test $argv[1] = "IT"
		set lang1 "italien"
	end
	if test $argv[2] = "FR"
		set lang2 "français"
	else if test $argv[2] = "EN"
		set lang2 "anglais"
	else if test $argv[2] = "ES"
		set lang2 "espagnol"
	else if test $argv[2] = "DE"
		set lang2 "allemand"
	else if test $argv[2] = "IT"
		set lang2 "italien"
	end
	echo $traduction | xclip -selection clipboard && echo Le texte (set_color -ui)$lang1(set_color normal) (set_color 323de2)\"$argv[3]\"(set_color normal) se traduit en (set_color -ui)$lang2(set_color normal) (set_color 9d9410)\"$traduction\"; echo (set_color -o 1c9d0e)"Traduction copiée dans le presse-papier !";
end

# Fonction permettant de réparer le bogue GPGME
function fixgpgme --argument-names cmd_args --description "Fonction permettant de réparer l'erreur courante GPGME qui apparaît notamment suite à un problème de réseau lors de la mise-à-jour des dépôts."
	set --local fixgpgme_version 1.0

# TODO
#	[ ] Modifier la seconde boucle while afin que la seconde partie s’exécute avec certitude dès qu'une quelconque erreur s'est produite dans la boucle.
#	[ ] Modifier la suppression du dossier de sauvegarde en utilisant une boucle for *.sauv$n avec $n{0..9} et la suppression sur le plus ancien (si exists *.sauv9 rm *.sauv)

		# Parse CLI options
	while true
		switch "$cmd_args"
			case -v --version
				gettext "La version de ce script est : $fixgpgme_version"
				return 1
			case -h --help
				gettext -e "Ce script permet de tenter de réparer l'erreur GPGME de $(set_color -o)PACMAN$(set_color normal) ou de fournir une aide pour le faire.\n"
				gettext -e "\nIl exécute les commandes suivantes :\n"
				gettext -e "\t$(set_color -i 296DB6)sudo mv /etc/pacman.d/gnupg /etc/pacman.d/gnupg.sauv$(set_color normal)\t\t\t---> Afin de sauvegarde des clés ;\n"
				gettext -e "\t$(set_color -i 296DB6)sudo mv /var/lib/pacman/sync /var/lib/pacman/sync.sauv$(set_color normal)\t\t\t---> Afin de sauvegarder les fichiers de synchronisation ;\n"
				gettext -e "\t$(set_color -i 296DB6)sudo rm /var/cache/pacman/pkg/*.sig$(set_color normal)\t\t\t\t\t---> Afin de supprimer les signatues en cache ;\n"
				gettext -e "\t$(set_color -i 296DB6)sudo pacman-key --init$(set_color normal)\t\t\t\t\t\t\t---> Afin de d'initialiser les clés ;\n"
				gettext -e "\t$(set_color -i 296DB6)sudo pacman-key --refresh-keys$(set_color normal)\t\t\t\t\t\t---> Afin de mettre-à-jour les clés ;\n"
				gettext -e "\t$(set_color -i 296DB6)sudo pacman-key --populate$(set_color normal)\t\t\t\t\t\t---> Afin de peupler les clés ;\n"
				gettext -e "\t$(set_color -i 296DB6)update --skip-mirrorlist$(set_color normal)\t\t\t\t\t\t---> Afin de mettre-à-jour le système.\n"
				gettext -e "Si le problème persiste, essayez de réinstaller les paquets suivants $(set_color -oi)archlinux-keyring$(set_color normal) &$(set_color -oi) garuda-keyring$(set_color normal) en utilisant les commandes :\n"
				gettext -e "\t$(set_color -i 296DB6)sudo pacman -Sy archlinux-keyring && sudo pacman -Ssy garuda-keyring$(set_color normal)\t---> Afin de réinstaller les clés.\n"
				gettext -e "... Puis relancez la commande fixgpgme ou lancez les commandes précédemment citées."
				return 1
			case ""
				gettext -e "$(set_color -o red)==>$(set_color normal) $(set_color -o)Tentative de réparation du bogue GPGME...$(set_color normal)\n"
				while true
					gettext -e "$(set_color -o red)::$(set_color normal) $(set_color -o)Sauvegarde des clés... $(set_color normal)$(set_color -i)(mv /etc/pacman.d/gnupg /etc/pacman.d/gnupg.sauv)$(set_color normal)\n"
					if test -d /etc/pacman.d/gnupg.sauv/
						gettext -e "$(set_color -o red) ⚠️$(set_color normal)$(set_color -o) Un dossier de sauvegarde existe déjà, suppression en cours... $(set_color -o red) ⚠️$(set_color normal)\n"
						sudo rm -rf /etc/pacman.d/gnupg.sauv/
					end
					sudo mv /etc/pacman.d/gnupg/ /etc/pacman.d/gnupg.sauv/
					gettext -e "$(set_color -o red)::$(set_color normal) $(set_color -o)Sauvegarde des fichiers de synchronisation... $(set_color normal)$(set_color -i)(mv /var/lib/pacman/sync /var/lib/pacman/sync.sauv)$(set_color normal)\n"
					if test -d /var/lib/pacman/sync.sauv/
						gettext -e "$(set_color -o red) ⚠️$(set_color normal)$(set_color -o) Un dossier de sauvegarde existe déjà, suppression en cours... $(set_color -o red) ⚠️$(set_color normal)\n"
						sudo rm -rf /var/lib/pacman/sync.sauv/
					end
					sudo mv /var/lib/pacman/sync /var/lib/pacman/sync.sauv
					gettext -e "$(set_color -o red)::$(set_color normal) $(set_color -o)Suppression des signatures en cache... $(set_color normal)$(set_color -i)(rm /var/cache/pacman/pkg/*.sig)$(set_color normal)\n"
					if test -f /var/cache/pacman/pkg/*.sig
						sudo rm /var/cache/pacman/pkg/*.sig
					end
					gettext -e "$(set_color -o red)::$(set_color normal) $(set_color -o)Initialisation des clés... $(set_color normal)$(set_color -i)(pacman-key --init)$(set_color normal)\n"
					sudo pacman-key --init
					gettext -e "$(set_color -o red)::$(set_color normal) $(set_color -o)Mise-à-jour des clés... $(set_color normal)$(set_color -i)(pacman-key --refresh-keys)$(set_color normal)\n"
					sudo pacman-key --refresh-keys
					gettext -e "$(set_color -o red)::$(set_color normal) $(set_color -o)Peuplement des clés... $(set_color normal)$(set_color -i)(pacman-key --populate)$(set_color normal)\n"
					sudo pacman-key --populate
					gettext -e "$(set_color -o red)::$(set_color normal) $(set_color -o)Mise à jour totale via le script $(set_color -oi red)update$(set_color normal)$(set_color -o)... $(set_color normal)$(set_color -i)(update --skip-mirrorlist)$(set_color normal)\n"
					update --skip-mirrorlist
					return 1
				end
				gettext -e "$(set_color -o red)Erreur lors de la réparation du bogue...$(set_color normal)\n"
				gettext -e "Pour résoudre le problème, essayez de réinstaller les paquets suivants $(set_color -oi)archlinux-keyring garuda-keyring$(set_color normal) en utilisant les commandes :\n"
				gettext -e "$(set_color -i)sudo pacman -Sy archlinux-keyring && sudo pacman -Ssy garuda-keyring$(set_color normal)\n"
				gettext -e "... Puis relancez la commande fixgpgme ou lancez les commandes suivantes :\n"
				gettext -e "$(set_color -i)sudo mv /etc/pacman.d/gnupg /etc/pacman.d/gnupg.sauv$(set_color normal)\n"
				gettext -e "$(set_color -i)sudo mv /var/lib/pacman/sync /var/lib/pacman/sync.sauv$(set_color normal)\n"
				gettext -e "$(set_color -i)sudo rm /var/cache/pacman/pkg/*.sig$(set_color normal)\n"
				gettext -e "$(set_color -i)sudo pacman-key --init$(set_color normal)\n"
				gettext -e "$(set_color -i)sudo pacman-key --refresh-keys$(set_color normal)\n"
				gettext -e "$(set_color -i)sudo pacman-key --populate$(set_color normal)\n"
				gettext -e "$(set_color -i)update --skip-mirrorlist$(set_color normal)\n"
			case \*
				gettext -e "fixgpgme: $(set_color red)Option inconnue: \"$cmd_arg\"$(set_color -i red)\nVoir l'aide :$(set_color normal) $(set_color -i)fixgpgme --help$(set_color normal)" >&2 && return 1
		end
	end
end

## Run fastfetch if session is interactive
if status --is-interactive && type -q fastfetch
   fastfetch --load-config neofetch
end
