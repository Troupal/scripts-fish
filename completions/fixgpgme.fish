set -l commands help version
complete -c fixgpgme

# These are simple options that can be used everywhere.
commandline -f accept-autosuggestion
complete -c fixgpgme -x
complete -c fixgpgme -s h	-l help		-d "Affiche l'aide liée à la fonction fixgpgme"
complete -c fixgpgme -s v	-l version	-d "Affiche la version de la fonction fixgpgme"
