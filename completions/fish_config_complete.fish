# Complétions liées à stegimg
# 	set -l stegimg_cmd help version test cache recup detect
# 	complete -c stegimg -f
# 	complete -c stegimg -n "not __fish_seen_subcommand_from $stegimg_cmd" -a "test cache recup detect"
# 	complete -c stegimg -n "not __fish_seen_subcommand_from $stegimg_cmd" -a "test" -d "Maintain RTC in local time"
# 	# These are simple options that can be used everywhere.
# 	complete -c stegimg -s h	-l help		-d "Affiche l'aide liée à la fonction stegimg"
# 	complete -c stegimg -s v	-l version	-d "Affiche la version de la fonction stegimg"
# 	complete -c stegimg -s t	-l teste	-d "Teste si l'image peut contenir le fichier source"
# #	complete -c stegimg -s t	-l test		-d "Test if the image can contain the source file"
# 	complete -c stegimg -s c	-l cache	-d "Lance la stéganographie du fichier source dans l'image"
# #	complete -c stegimg -s H	-l hidden	-d "Starts steganography of the source file in the image"
# 	complete -c stegimg -s r	-l recup	-d "Récupère le fichier source d'une une image stéganographiée"
# 	complete -c stegimg -s d	-l detect	-d "Détecte si une image à subit une stéganographie"

# Complétions liées à tradc

# Complétions liée à fixgpgme
	# set -l fixgpgme_cmd help version
	# complete -c fixgpgme
	# complete -c fixgpgme -n "not __fish_seen_subcommand_from $fixgpgme_cmd" -a "help version"
	# complete -c fixgpgme -n "not __fish_seen_subcommand_from $fixgpgme_cmd" -a "help" -d "Maintain RTC in local time"
	# complete -c fixgpgme -s h	-l help		-d "Affiche l'aide liée à la fonction fixgpgme"
	# complete -c fixgpgme -s v	-l version	-d "Affiche la version de la fonction fixgpgme"


#	complete -c stegimg -a '(__fish_complete_path_files)' -x

function __fish_stegimg_use_file -d 'Test if snap command should have files as potential completion'
	for i in (commandline -opc)
		if contains -- $i test teste cache hidden recup detect
			return 0
		end
	end
	return 1
end

function __fish_complete_path_files --description "Complete using path"
    # set -l target
    # set -l description
    # switch (count $argv)
        # case 0
        #     # pass
        # case 1
        #     set target "$argv[1]"
        # case 2 "*"
            # set target "$argv[1]"
            # set description "$argv[2]"
    # end
    set -l targets "$target"*
    if set -q targets[1]
		# function __liste
		# 	ls
		# 	# exa -x --color=always --git --icons --group-directories-first -G --no-filesize --no-permissions --no-time --no-user --oneline $argv
		# end
		#set -l liste (exa -x --color=always --git --icons --group-directories-first -G --no-filesize --no-permissions --no-time --no-user $target)
		command ls -dp $targets
		# __liste $targets
#		eval (echo $liste)
#		command exa --icons -x --group-directories-first $targets
#		eval (ll $targets)
#        printf "%s\t$description\n" (exa -x --color=always --git --icons --octal-permissions --group-directories-first $targets)
    end
end
		
# function __fish_stegimg_use_file -d 'Test if snap command should have files as potential completion'
# 	for i in (commandline -opc)
# 		if contains -- $i test teste cache hidden recup detect
# 			return 0
# 		end
# 	end
# 	return 1
# end

# function __fish_complete_command --description 'Complete using all available commands'
# 	set -l ctoken (commandline -ct)
# 	switch $ctoken
# 		case '*=*'
# 			set ctoken (string split "=" -- $ctoken)
# 			printf '%s\n' $ctoken[1]=(complete -C "$ctoken[2]")
# 		case '-*' # do not try to complete options as commands
# 			return
# 		case '*'
# 			complete -C "$ctoken"
# 	end
# end

#
# Find directories that complete $argv[1], output them as completions
# with description $argv[2] if defined, otherwise use 'Directory'.
# If no arguments are provided, attempts to complete current commandline token.
#
# function __fish_complete_all_files -d "Complete files" --argument-names comp desc
#     if not set -q desc[1]
#         set desc Directory
#     end

#     if not set -q comp[1]
#         set comp (commandline -ct)
#     end

#     # HACK: We call into the file completions by using a non-existent command.
#     # If we used e.g. `ls`, we'd run the risk of completing its options or another kind of argument.
#     # But since we default to file completions, if something doesn't have another completion...
#     set -l dirs (complete -C"nonexistentcommandooheehoohaahaahdingdongwallawallabingbang $comp" | string match -r '.*/$')

#     if set -q dirs[1]
#         printf "%s\t$desc\n" $dirs
#     end
# end