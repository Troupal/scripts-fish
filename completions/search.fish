set -l commands help version section
complete -c search -x
complete -c search -s h -l help -d 'Affiche l\'aide'
complete -c search -s v -l version -d 'Affiche la version'
complete -c search -s t -l tool -d 'Précise l\'outil utilisé pour la recherche ("ag", "rg" ou "grep")' -x -a "{ag	,rg	,grep	}" | complete -c search -F
