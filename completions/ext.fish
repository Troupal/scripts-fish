set -l commands help version clig dsou sur rat format
complete -c ext

# These are simple options that can be used everywhere.
commandline -f accept-autosuggestion

# complete -c stegimg -n __fish_stegimg_use_file -x -a '(__fish_complete_path_files)'
complete -c ext -s h	-l help			-d "Affiche l'aide liée à la fonction aliases"
complete -c ext -s v	-l version	-d "Affiche la version de la fonction aliases"
complete -c ext -s c	-l clig	    -d "Formate le texte en le faisant clignoter"
complete -c ext -s d	-l dsou		  -d "Formate le texte en le double soulignant"
complete -c ext -s s	-l sur			-d "Formate le texte en le surlignant"
complete -c ext -s r  -l rat      -d "Formate le texte en le raturant"
complete -c ext -s f  -l format   -d "Précise le formatage appliqué" -x -a "clig dsou sur rat" 
