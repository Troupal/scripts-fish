set -l commands help version fonctions aliases all
complete -c aliases

# These are simple options that can be used everywhere.
commandline -f accept-autosuggestion

# complete -c stegimg -n __fish_stegimg_use_file -x -a '(__fish_complete_path_files)'
complete -c aliases -s h	-l help			-d "Affiche l'aide liée à la fonction aliases"
complete -c aliases -s v	-l version		-d "Affiche la version de la fonction aliases"
complete -c aliases -s f	-l fonctions	-d "Affiche la liste de toutes les fonctions connues de fish shell 🐠"
complete -c aliases -s a	-l aliases		-d "Affiche la liste de tous les alias connus de fish 🐠"
complete -c aliases -s A	-l all			-d "Affiche la liste de tous les alias et fonctions connus de fish 🐠"
