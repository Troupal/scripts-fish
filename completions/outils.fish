set -l commands help version section
complete -c outils -x
complete -c outils -s h -l help -d 'Affiche l\'aide'
complete -c outils -s v -l version -d 'Affiche la version'
complete -c outils -s s -l section -d 'Affiche uniquement la section suivante: "alternatives" ou "système" ou "utilitaires" ou "divers" ou "raccourcis" ou "commentaires"' -x -a "{alternatives	,système	,utilitaires	,divers	,raccourcis	,commentaires	}"
