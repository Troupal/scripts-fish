# TODO
#	- [ ] Limiter l'ajout des langues à deux puis ajouter des parenthèses et placer le curseur au milieu
set -l commands help version section
set --local decompte 0
complete -c tradc -s h -l help -d 'Affiche l\'aide'
complete -c tradc -s V -l version -d 'Affiche la version'
# complete -c tradc -s '' -l '' -d 'Langue d\'origine' -x -a "{Bulgare	,Chinois	,Tchèque	,Danois	,Néerlandais	,Anglais	,Estonien	,Finois	,Français	,Allemand	,Grecque	,Hongrois	,Italien	,Japonais	,Letton	,Lituanien	,Polonais	,Portugais	,Roumain	,Russe	,Slovaque	,Slovène	,Espagnole	,Suèdois	}" | complete -c tradc -x
# complete -c tradc -x -d 'Langue de destination' -a "{Bulgare	,Chinois	,Tchèque	,Danois	,Néerlandais	,Anglais	,Estonien	,Finois	,Français	,Allemand	,Grecque	,Hongrois	,Italien	,Japonais	,Letton	,Lituanien	,Polonais	,Portugais	,Roumain	,Russe	,Slovaque	,Slovène	,Espagnole	,Suèdois	}" #| complete -c tradc -f
# complete -c tradc -d 'Langue d\'origine' -n "__fish_any_arg_in" -f -a "{BG	,ZH	,CS	,DA	,NL	,EN	,ET	,FI	,FR	,DE	,EL	,HU	,IT	,JA	,LV	,LT	,PL	,PT	,RO	,RU	,SK	,SL	,ES	,SV	}" | complete -c tradc -f
# complete -c tradc -x -d 'Langue de destination' -a "{Bulgare	,Chinois	,Tchèque	,Danois	,Néerlandais	,aAglais	,Estonien	,Finois	,Français	,Allemand	,Grecque	,Hongrois	,Italien	,Japonais	,Letton	,Lituanien	,Polonais	,Portugais	,Roumain	,Russe	,Slovaque	,Slovène	,Espagnole	,Suèdois	}"; complete -c tradc -f
complete -c tradc -s s -l section -d 'Affiche uniquement la section suivante: "alternatives" ou "système" ou "utilitaires"' -x -a "{alternatives	,système	,utilitaires	}"
complete -c tradc -x -d "Bulgare" -a "BG"
complete -c tradc -x -d "Chinois" -a "ZH"
complete -c tradc -x -d "Tchèque" -a "CS"
complete -c tradc -x -d "Danois" -a "DA"
complete -c tradc -x -d "Néerlandais" -a "NL"
complete -c tradc -x -d "Anglais" -a "EN"
complete -c tradc -x -d "Estonien" -a "ET"
complete -c tradc -x -d "Finois" -a "FI"
complete -c tradc -x -d "Français" -a "FR"
complete -c tradc -x -d "Allemand" -a "DE"
complete -c tradc -x -d "Grecque" -a "EL"
complete -c tradc -x -d "Hongrois" -a "HU"
complete -c tradc -x -d "Italien" -a "IT"
complete -c tradc -x -d "Japonais" -a "JA"
complete -c tradc -x -d "Letton" -a "LV"
complete -c tradc -x -d "Lituanien" -a "LT"
complete -c tradc -x -d "Polonais" -a "PL"
complete -c tradc -x -d "Portugais" -a "PT"
complete -c tradc -x -d "Roumain" -a "RO"
complete -c tradc -x -d "Russe" -a "RU"
complete -c tradc -x -d "Slovaque" -a "SK"
complete -c tradc -x -d "Slovène" -a "SL"
complete -c tradc -x -d "Espagnol" -a "ES"
complete -c tradc -x -d "Suèdois" -a "SV"