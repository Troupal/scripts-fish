set -l commands help version test cache recup detect
complete -c stegimg
complete -c stegimg -n "not __fish_seen_subcommand_from $commands" -a "test cache recup detect"
complete -c stegimg -n "not __fish_seen_subcommand_from $commands" -a "test" -d "Maintain RTC in local time"
commandline -f accept-autosuggestion
# These are simple options that can be used everywhere.


# complete -c stegimg -n __fish_stegimg_use_file -x -a '(__fish_complete_path_files)'
complete -c stegimg -s h	-l help		-d "Affiche l'aide liée à la fonction stegimg"
complete -c stegimg -s v	-l version	-d "Affiche la version de la fonction stegimg"
complete -c stegimg -s t	-l test	-d "Teste si l'image peut contenir le fichier source"
# complete -c stegimg -s t	-l test		-d "Test if the image can contain the source file"
complete -c stegimg -s c	-l cache	-d "Lance la stéganographie du fichier source dans l'image"
# complete -c stegimg -s H	-l hidden	-d "Starts steganography of the source file in the image"
complete -c stegimg -s r	-l recup	-d "Récupère le fichier source d'une une image stéganographiée"
complete -c stegimg -s d	-l detect	-d "Détecte si une image à subit une stéganographie"
