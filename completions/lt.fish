set -l commands help version nonall ignore
# complete -c cp -s t -l target-directory -d "Target directory" -x -a "(__fish_complete_directories (commandline -ct) 'Target directory')"
# complete -x -c adduser -a "(__fish_complete_users; __fish_complete_groups)"
# complete -c adduser -l home -d 'Use specified directory as the user\'s home directory' -x -a '(__fish_complete_directories)'

complete -c lt
complete -c lt -s h -l help -d "Affiche l'aide"
complete -c lt -s v -l version -d "Affiche le numéro de version de la fonction lt"
complete -c lt -s n -l nonall -d "lt n'affiche pas les fichiers cachés ou les liens symboliques"
complete -c lt -s I -l ignore -d "lt ignore les fichiers correspondant au pattern (glob)"
# complete -c lt -a '(__fish_complete_path)' -x
