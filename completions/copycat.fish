#set -l commands help version

complete -c copycat
complete -c copycat -s h -l help -d "Affiche l'aide"
complete -c copycat -s v -l version -d "Affiche le numéro de version de la fonction copycat"
