#!/usr/bin/fish

# TODO ::
#   - [x] Ajouter ici toutes les fonctions et les définitions des raccourcis contenues dans config.fish

## Fonctions pour le binding
# Functions needed for !! and !$ https://github.com/oh-my-fish/plugin-bang-bang
function __history_previous_command
    switch (commandline -t)
    case "!"
        commandline -t $history[1]; commandline -f repaint
    case "*"
        commandline -i !
    end
end

function __history_previous_command_arguments
    switch (commandline -t)
    case "!"
        commandline -t ""
        commandline -f history-token-search-backward
    case "*"
        commandline -i '$'
    end
end

if [ "$fish_key_bindings" = fish_vi_key_bindings ];
    bind -Minsert ! __history_previous_command
    bind -Minsert '$' __history_previous_command_arguments
else
    bind ! __history_previous_command
    bind '$' __history_previous_command_arguments
end

# Function necessary to bind \ char and bypass fish aliases
#function __bypass_fish_alias
#		set -g cmdtoexec (commandline -b)
# 		commandline -r \\$cmdtoexec
#	function (eval echo \\$cmdtoexec)
#		set -l arg1 (echo $cmdtoexec | cut -d' ' -f2-5)
#		set -l arg0 (echo $cmdtoexec | awk '{print $1}')
#		set -l arg2 $(eval alias | grep "^alias $arg0" -P | cut -d' ' -f3-)
#		echo $arg0
#		echo $arg1
#		echo $arg2
#		echo "L'alias \"$arg0\" de la commande entrée \"$cmdtoexec\" a été bypassée :"
#		echo "Cette alias correspondait, à l'origine à \"$arg2\"."
#		set -l argv0 "command $cmdtoexec"
#		eval $argv0
#		set -e cmdtoexec
#	end
#	commandline -f execute
#end

#if [ "$fish_key_bindings" = fish_vi_key_bindings ];
#	bind -Minsert \\ __bypass_fish_alias
#	#	bind -Minsert '$' __history_previous_command_arguments
#else
#	bind \\ __bypass_fish_alias
#	#	bind '$' __history_previous_command_arguments
#end

function _navi_smart_replace
	set -l current_process (commandline -p | string trim)
	
	if test -z "$current_process"
		commandline -i (navi --print)
	else
		set -l best_match (navi --print --best-match --query "$current_process")
	
		if not test "$best_match" >/dev/null
			return
		end
	
		if test -z "$best_match"
			commandline -p (navi --print --query "$current_process")
		else if test "$current_process" != "$best_match"
			commandline -p $best_match
		else
			commandline -p (navi --print --query "$current_process")
		end
	end
	
	commandline -f repaint
end
	
if test $fish_key_bindings = fish_default_key_bindings
	bind \cg _navi_smart_replace
else
	bind -M insert \cg _navi_smart_replace
end

function _vim_insert_binding
	if not commandline | string length -q
		commandline -r $history[1]
	end

	# Récupérer la commande actuelle
	set -l process (commandline -p | string collect)
	set -l cmd (commandline -p)

	# Récupérer la commande actuelle
	set -l process (commandline -p | string collect)
	if string match -rq '^\s*' -- $process
		commandline -r (string replace -r '^\s*' '' -- $process)
	end

	# Vérifier si la commande commence par "cat" ou "bat"
	if string match -rq '^\s?(cat|bat)' -- $process
		commandline -r (string replace -r '^\s?(cat|bat)' 'vim' -- $process)
	# Vérifier si la commande commence par "vim"
	else if string match -rq '^\s?vim' -- $process
		commandline -r (string replace -r '^\s?vim ' '' -- $process)
	# Vérifier si la commande commence par "sudo vim" et n'enlever que le "vim"
	else if string match -rq '^\s?sudo vim' -- $process
		commandline -r (string replace -r '^\s?(sudo) vim ' '$1 ' -- $process)
	#Vérifier si la commande commence par "sudo (cat|bat)" et remplacer par "sudo vim"
	else if string match -rq '^\s?sudo (cat|bat)' -- $process
		commandline -r (string replace -r '^\s?(sudo) (cat|bat) ' '$1 vim ' -- $process)
	# Vérifier si la commande commence par "sudo" et insérer le "vim" après
	else if string match -rq '^\s?sudo' -- $process
		commandline -r (string replace -r '^\s?(sudo) ' '$1 vim ' -- $process)
	else
		# Sinon, alterner le "vim" au début
		fish_commandline_prepend "vim"
    end
end

# Lier la fonction à ALT + V
bind -s \ev _vim_insert_binding
bind -M insert \ev _vim_insert_binding

function _sudovim_insert_binding
	if not commandline | string length -q
		commandline -r $history[1]
	end

	# Récupérer la commande actuelle
	set -l process (commandline -p | string collect)
	set -l cmd (commandline -p)

	# Récupérer la commande actuelle
	set -l process (commandline -p | string collect)
	if string match -rq '^\s*' -- $process
		commandline -r (string replace -r '^\s*' '' -- $process)
	end

	# Vérifier si la commande commence par "cat" ou "bat"
	if string match -rq '^\s?(cat|bat)' -- $process
		# Remplacer "cat" ou "bat" par "vim"
		commandline -r (string replace -r '^\s?(cat|bat) ' 'sudo vim ' -- $process)
	# Vérifier si la commande commence par "vim"
	else if string match -rq '^\s?vim' -- $process
		commandline -r (string replace -r '^\s?vim ' 'sudo vim ' -- $process)
#	else if string match -rq '^\s?sudo vim' -- $process
#		commandline -r (string replace -r '^\s?(sudo) vim ' '$1 ' -- $process)
	else if string match -rq '^\s?sudo (cat|bat)' -- $process
		commandline -r (string replace -r '^\s?(sudo) (cat|bat) ' '$1 vim ' -- $process)
	# Vérifier si la commande commence par "sudo" et insérer le "vim" après
#	else if string match -rq '^\s?sudo' -- $process
#		commandline -r (string replace -r '^\s?(sudo) ' '$1 vim ' -- $process)
	else
		# Sinon, ajouter "vim" au début
		fish_commandline_prepend "sudo vim"
    end
end

# Lier la fonction à ALT + V
bind -s \eV _sudovim_insert_binding
bind -M insert \eV _sudovim_insert_binding

function _cat_insert_binding
	if not commandline | string length -q
		commandline -r $history[1]
	end

	# Récupérer la commande actuelle
	set -l process (commandline -p | string collect)
	if string match -rq '^\s*' -- $process
		commandline -r (string replace -r '^\s*' '' -- $process)
	end

	# Vérifier si la commande commence par "vim"
	if string match -rq '^\s?(vim)' -- $process
		commandline -r (string replace -r '^\s?(vim) ' 'cat ' -- $process)
	# Vérifier si la commande commence par "vim"
	else if string match -rq '^\s?sudo vim' -- $process
		commandline -r (string replace -r '^\s?(sudo) vim\s?' '$1 cat ' -- $process)
	else if string match -rq '^\s?sudo (cat|bat)' -- $process
		commandline -r (string replace -r '^\s?(sudo) (cat|bat) ' '$1 ' -- $process)
	# Vérifier si la commande commence par "sudo" et insérer le "vim" après
	else if string match -rq '^\s?sudo' -- $process
		commandline -r (string replace -r '^\s?(sudo) ' '$1 cat ' -- $process)
	else
		# Sinon, ajouter "vim" au début
		fish_commandline_prepend "cat"
	end
end

if test $fish_key_bindings = fish_default_key_bindings
	bind \eC _cat_insert_binding
else
	bind -M insert \eC _cat_insert_binding
end

function _info_insert_binding
	# Récupérer la commande actuelle
	set -l cmd (commandline -p)

	# Extraire le premier mot de la commande
	set -l cmd_name (string split ' ' -- $cmd)[1]

	# Vérifier si la commande est un exécutable
	if type -q $cmd_name
		# Vérifier si la commande a une page d'information
		if info $cmd 2>/dev/null 21>&1
			# Exécuter info pour la commande
			echo ""
		# Vérifier si la commande supporte l'option --help
		else if $cmd --help 2>/dev/null 2>&1
			# Exécuter --help pour la commande
        echo ""
        $cmd --help
        # Vérifier si la commande à une page d'aide
        else if man $cmd_name >/dev/null 2>&1
			read -P "Voulez-vous afficher le man de $cmd ? (O/n) " -d "O" réponse
			switch $réponse
				case O o Y y ""
					echo ""
					man $cmd_name
				case N n

            end
		else
			# Aucune information disponible
            gettext -e "\n$scoo\aAucune information disponible pour '$scoio$cmd_name$scoo'.$sn\n"
		end
	else
		gettext -e "'$scrio$cmd_name$scro' n'est pas une commande valide.$sn"
	end
	read -n 1 -s -P "Appuyez sur une touche pour continuer..."
	#commandline -f insert-line-under
	set last_job (jobs -p | tail -n 1)
	if test -n "$last_job"
		kill $last_job
	end
	commandline -f cancel
	commandline -b $cmd
end

# Lier la fonction à ALT + I
bind -s \ei _info_insert_binding

function _paru_insert_binding
	fish_commandline_prepend "paru"
end

bind -s \ea _paru_insert_binding
