# Fonction permettant d'étendre et de mettre en forme la commande '$ alias' ou son équivalent '$ functions -ndv'
#
#	TODO ::
#		- [ ] Trouver un moyen d'automatiser la génération du cadre du tableau
#		- [x] Ajouter la completion des options
#

function aliases --argument-names cmd_args --description "Afficher la liste des aliases connus"
	# Définition des variables
	set --local lt_version 1.0 # Définition du numéro de la version
	set --local desc "Commande $scbbo\aaliases$sn permettant d'afficher les différents aliases et fonctions existants sur le système en $si\aFishShell$sn."
	set --local cmd aliases
	set -l sortie ":Fonctions:Description"\n"├:───────────────────────────────────┼───────────────────────────────────┤"\n
	set -l sortii

	# Parse des options CLI
	argparse v/version h/help f/functions A/all a/aliases -- $argv &>/dev/null
	or gettext -e "$scr\a$cmd : \"$scrub$cmd_args$scr\" : $scr Option inconnue\nVoir l'aide :$scb aliases$scbi --help$sn" && return 121

	set --local print_help 'gettext -e "    $so\aDescription :$sn\n"
		gettext -e "    \t$desc\n"
		gettext -e "    $so\aUsage :$sn\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-h|--help}$sn	   $scro——→$sn Affiche cette aide\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-v|--version}$sn	   $scro——→$sn Affiche la version du script\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-f|--functions}$s   $scro——→$sn Affiche la toutes les fonctions avec leur description\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-A|--all}$sn	   $scro——→$sn Affiche la tous les alias & fonctions avec leur description\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-a|--aliases}$sn	   $scro——→$sn Affiche la version du script\n"'

	if test -z $cmd_args
			gettext -e "⚠ $scbroio\aVous n'avez pas entré d'arguments !$sn⚠\n$scgi\aVoici commment fonctionne la commande…$sn\n"
			eval $print_help
			gettext \n"Mais comme la variable $scmo\$fish_aliases_selected_print$sn est égale à $scoio\"$fish_aliases_selected_print\"$sn :"\n
	end

	if set -q _flag_help
		eval $print_help
		return 1
	else if set -q _flag_version
		gettext -e "$so\aLa version de ce script est : $si$lt_version"
		return 1
	else if begin set -q _flag_functions; or test "$fish_aliases_selected_print" = "functions"; or test "$fish_aliases_selected_print" = "fonctions"; end
		# Format and print known functions.
		gettext -e "$so\aListe des fonctions :$sn\n"
		echo $scro"  ╭───────────────────────────┬───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮"
		echo $scro"  │"$scb\t"   Fonctions"\t"      "$scro"│"$scyi\t\t\t\t\t\t\t"     Description"\t\t\t\t\t\t$scro"      │"$sn
		echo $scro"  ├───────────────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤"
		for func in (functions -n)
			set -l output (functions $func | string match -r -- "^function .* --description (?:(?!'alias )(.*))\$") && set output (string replace -r -- '--argument .*' '' $output)
			set output (string replace -r -a -- '^[\']' '' $output)
			set output (string replace -r -a -- '[\']$' '' $output)
			set output (string replace -r -a -- '(\s)(.*)(debug)(.*)' '\'' $output)
			if set -q output[2]
				set output (string replace -r -- '^'$func'[= ]' '' $scyi$output[2]$sn)
					# set output (string replace -r '.{1}' '' $output)
				# echo $scro"│::"$sn $scb$func$sn $scro"———→"$sn (eval echo (string escape -- $output[1]))
				set sortii $sortii (echo $scro":"$sn$scb$func$sn$scro":"$sn(eval echo (string escape -- $output[1]$scro":"$sn)))\n
				# eval printf '│:$func:│:$output[1]:│' | column -t -s ':'
			end
		end
		set sortie $sortii
		echo $sortie | column -t -e -s ':' -o $scro' │ '
		echo $scro"  ╰───────────────────────────┴───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯"
		return 0
	else if begin set -q _flag_aliases; or test "$fish_aliases_selected_print" = "aliases"; end
		# Format and print known aliases.
		gettext -e "$so\aListe des aliases :$sn\n"
		echo $scro"  ╭───────────────────────────┬───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮"
		echo $scro"  │"$scb\t"   Fonctions"\t"      "$scro"│"$scyi\t\t\t\t\t\t\t"     Description"\t\t\t\t\t\t$scro"      │"$sn
		echo $scro"  ├───────────────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤"
		for func in (functions -n)
			set -l output (functions $func | string match -r -- "^function .* --description (?:'alias (.*)'|alias\\\\ (.*))\$")
			if set -q output[2]
				set output (string replace -r -- '^'$func'[= ]' '' $scyi$output[2]$sn)
				set sortii $sortii (echo $scro":"$sn$scb$func$sn$scro":"$sn(eval echo (string escape -- $output[1]$scro":"$sn)))\n
			end
		end
		set sortie $sortii
		echo $sortie | column -t -e -s ':' -o $scro' │    '
		echo $scro"  ╰───────────────────────────┴───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯"
		return 0
	else if begin set -q _flag_all; or test "$fish_aliases_selected_print" = "all"; end
		# Format and print all known functions and aliases.
		gettext -e "$so\aListe des fonctions et aliases :$sn\n"
		echo $scro"  ╭───────────────────────────┬────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮"
		echo $scro"  │"$scb\t"   Fonctions"\t"      "$scro"│"$scyi\t\t\t\t\t\t\t\t"     Description"\t\t\t\t\t\t$scro"       │"$sn
		echo $scro"  ├───────────────────────────┼────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤"
		for func in (functions -n)
			set -l output (functions $func | string match -r -- "^function .* --description (?:(.*))\$") && set output (string replace -r -- '--argument .*' '' $output)
			set output (string replace -r -a -- '^[\']' '' $output)
			set output (string replace -r -a -- '[\']$' '' $output)
			set output (string replace -r -a -- '(\s)(.*)(debug)(.*)' '\'' $output)
			if set -q output[2]
				set output (string replace -r -- '^'$func'[= ]' '' $scyi$output[2]$sn)
				set sortii $sortii (echo $scro":"$sn$scb$func$sn$scro":"$sn(eval echo (string escape -- $output[1]$scro":")))\n
				# echo $scro"::"$sn $scb$func$sn $scro"———→"$sn (eval echo (string escape -- $output[1]))
			end
		end
		set sortie $sortii
		echo $sortie | column -t -e -s ':' -o $scro' │ '
		echo $scro"  ╰───────────────────────────┴────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯"
		return 0
	end
end


## À placer ligne 30 si besoin
	# set --local alias_flag 0
	# if not set -q argv[1]
	# 	gettext -e "⚠ $so\aVous n'avez pas entré d'arguments !$sn Voici commment fonctionne la commande…\n"
	# 	print_help
	# 	if test $fish_aliases_selected_print = "functions"
	# 		set cmd_args "Chacun ses fonctions ! →œ"
	# 		gettext \n"Mais comme la variable \$fish_aliases_selected_print est égale à \"fonctions\" :"\n
	# 	else if test $fish_aliases_selected_print = "aliases"
	# 		set cmd_args "Chacun ses aliases ! →œ"
	# 		gettext \n"Mais comme la variable \$fish_aliases_selected_print est égale à \"aliases\" :"\n
	# 	else if test $fish_aliases_selected_print = "all"
	# 		set cmd_args "Chacun sa toux ! →œ"
	# 		gettext \n"Mais comme la variable \$fish_aliases_selected_print est égale à \"all\" :"\n
	# 	end
	# end

	# while true
	# 	switch $cmd_args
	# 		case -h --help
	# 			print_help
	# 			return 1
	# 		case -v --version
	# 			gettext -e "$so\aLa version de ce script est : $si$lt_version"
	# 			return 1
	# 		case -f --functions "Chacun ses fonctions ! →œ"
	# 			# Format and print known functions.
	# 			gettext -e "$so\aListe des fonctions :$sn\n"
	# 			echo $so"Fonctions"\t" ———→"\t "Description"$sn
	# 			for func in (functions -n)
	# 				set -l output (functions $func | string match -r -- "^function .* --description (?:(?!'alias )(.*))\$") && set output (string replace -r -- '--argument .*' '' $output)
	# 				if set -q output[2]
	# 					set output (string replace -r -- '^'$func'[= ]' '' $scyi$output[2]$sn)
	# 					echo $scro"::"$sn $scb$func$sn $scro"———→"$sn (eval echo (string escape -- $output[1]))
	# 				end
	# 			end
	# 			return 0
	# 		case -a --aliases "Chacun ses aliases ! →œ"
	# 			# Format and print known aliases.
	# 			gettext -e "$so\aListe des aliases :$sn\n"
	# 			for func in (functions -n)
	# 				set -l output (functions $func | string match -r -- "^function .* --description (?:'alias (.*)'|alias\\\\ (.*))\$")
	# 				if set -q output[2]
	# 					set output (string replace -r -- '^'$func'[= ]' '' $scyi$output[2]$sn)
	# 					echo $scro"::"$sn $scb$func$sn $scro"———→"$sn (eval echo (string escape -- $output[1]))
	# 				end
	# 			end
	# 			return 0
	# 		case -A --all "Chacun sa toux ! →œ"
	# 			# Format and print all known functions and aliases.
	# 			gettext -e "$so\aListe des fonctions et aliases :$sn\n"
	# 			echo $so"Fonctions ou alias"\t" ———→"\t "Description"$sn
	# 			for func in (functions -n)
	# 				set -l output (functions $func | string match -r -- "^function .* --description (?:(.*))\$") && set output (string replace -r -- '--argument .*' '' $output)
	# 				if set -q output[2]
	# 					set output (string replace -r -- '^'$func'[= ]' '' $scyi$output[2]$sn)
	# 					echo $scro"::"$sn $scb$func$sn $scro"———→"$sn (eval echo (string escape -- $output[1]))
	# 				end
	# 			end
	# 			return 0
	# 	end
	# end
