function testee --argument-names cmd_args --description "Fonction permettant de tester d'autres fonctions"
	argparse v/version h/help 't/teste=?' e/essaie -- $argv &>/dev/null
	set -l local_arg
	set -l sortie
  set -l o false

	if set -q _flag_version
		set local_arg[1] "Drapeau version"
	end
	if set -q _flag_help
		set local_arg[2] "Drapeau help"
	end
	if set -q _flag_teste
    set o true
		set local_arg[3] "Drapeau teste = $_flag_teste[1]"
    if test "$_flag_teste[1]" = "test"
      set local_arg[5] "La valeur du drapeau teste est \"test\" !"
    else
      set local_arg[5] "La valeur du drapeau teste n'est pas \"teste\"!"
    end
    if test "$_flag_teste[1]" = "pastest"
      set local_arg[6] "La valeur du drapeau teste est \"pastest\" !"
    else
      set local_arg[6] "La valeur du drapeau teste n'est pas \"pastest\"!"
    end
	end
	if set -q _flag_essaie
		if test -z $argv[1]
			set local_arg[4] "Pas de arguments"
		else
			set local_arg[4] "Argument = $argv[1]"
		end
	end	
	for i in 1 2 3 4 5 6
		if not test -z $local_arg[$i]
			set sortie $sortie \n $local_arg[$i]
		end
	end
	echo $sortie
  if test "$o" = true
    echo "La valeur teste a été définie…"
  else
    echo "La valeur teste n'a pas été définie…"
  end
end
