#!/usr/bin/env fish

# Chemin vers le fichier .profile
set profile_file ~/.profile
set fish_profile_file ~/.fish_profile

# Vérifiez si le fichier .profile existe
if test -f $profile_file
    # Créez ou videz le fichier .fish_profile
    echo "" > $fish_profile_file

    # Lire le fichier ligne par ligne
    for line in (cat $profile_file)
        # Vérifiez si la ligne commence par "export"
        if string match -r '^export ' $line
            # Supprimez "export " et divisez la ligne en KEY et VALUE
            set key_value (string split '=' (string replace -r '^export ' '' $line))
            set key (string trim $key_value[1])
            set value (string trim $key_value[2])

            # Écrire la définition dans le fichier .fish_profile
            echo "set -Ux $key $value" >> $fish_profile_file
        end
    end
else
    echo "Le fichier $profile_file n'existe pas."
end

