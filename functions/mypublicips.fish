function mypublicips --description "Retourne mes IP publics en IPv4 et IPv6"
	set ip4 (curl -s -4 ifconfig.me)
	set ip6 (curl -s -6 ifconfig.me)
	gettext -e "$scb\aIPv4 : $scyo$ip4$scn$sco$scn\n"
	gettext -e "$scb\aIPv6 : $scbo$ip6$scn"
end
