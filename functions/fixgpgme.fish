# Fonction permettant de réparer le bogue GPGME Fonction permettant de réparer l'erreur courante GPGME qui apparaît notamment suite à un problème de réseau lors de la mise-à-jour des dépôts.
#
#	TODO ::
#		- [ ] Modifier la seconde boucle while afin que la seconde partie s’exécute avec certitude dès qu'une quelconque erreur s'est produite dans la boucle.
#		- [ ] Modifier la suppression du dossier de sauvegarde en utilisant une boucle for *.sauv$n avec $n{0..9} et la suppression sur le plus ancien (si exists *.sauv9 rm *.sauv)
#		- [x] Modifier le texte affichager dans l'aide
#		- [x] Modifier les viables de couleur pour l'affichage de l'aide
#		- [x] Ajouter la completion des options
#

function fixgpgme --argument-names cmd_args --description "Fonction permettant de réparer l'erreur courante GPGME."
	set --local fixgpgme_version 2.0
	set --local cmd fixgpgme
	set --local desc "La fonction $scbo$cmd$sn permet de $scgo\aréparer$sn l'$scruo\aerreur$sn courant $scroGPGME$sn de $scbo\aPACMAN$sn (qui arrive lorsqu'il y a une coupure de réseau pendant une mise-à-jour)\n\tou de $scgo\afournir une aide$sn pour le faire."
	set --local print_help 'gettext -e "    $so\aDescription :$sn\n"
		gettext -e "    \t$desc\n"
		gettext -e "\n\t$sio\aIl exécute les commandes suivantes :$sn\n"
		gettext -e "    \t    $scbo\asudo mv$sn $scoio/etc/pacman.d/gnupg /etc/pacman.d/gnupg.sauv$sn\t\t   $scro——→$sn Afin de sauvegarde des clés ;\n"
		gettext -e "    \t    $scbo\asudo mv$sn $scoio/var/lib/pacman/sync /var/lib/pacman/sync.sauv$sn\t\t   $scro——→$sn Afin de sauvegarder les fichiers de synchronisation ;\n"
		gettext -e "    \t    $scbo\asudo rm$sn $scoio/var/cache/pacman/pkg/*.sig$sn\t\t\t\t\t   $scro——→$sn Afin de supprimer les signatues en cache ;\n"
		gettext -e "    \t    $scbo\asudo pacman-key$sn $scbi--init$sn\t\t\t\t\t\t   $scro——→$sn Afin de d\'initialiser les clés ;\n"
		gettext -e "    \t    $scbo\asudo pacman-key$sn $scbi--refresh-keys$sn\t\t\t\t\t   $scro——→$sn Afin de mettre-à-jour les clés ;\n"
		gettext -e "    \t    $scbo\asudo pacman-key$sn $scbi--populate$sn\t\t\t\t\t\t   $scro——→$sn Afin de peupler les clés ;\n"
		gettext -e "    \t    $scbo\aupdate$sn $scbi--skip-mirrorlist$sn\t\t\t\t\t\t   $scro——→$sn Afin de mettre-à-jour le système.\n"
		gettext -e "    Si le problème persiste, essayez de réinstaller les paquets suivants $(set_color -oi)archlinux-keyring$(set_color normal) &$(set_color -oi) garuda-keyring$(set_color normal) en utilisant les commandes :\n"
		gettext -e "    \t    $scbo\asudo pacman $scbi-Sy$sn $scmo\aarchlinux-keyring$sn $scbo\a&& sudo pacman $scbi-Ssy$sn $scmo\agaruda-keyring$sn   $scro——→$sn Afin de réinstaller les clés.\n"
		gettext -e "    \t... Puis relancez la commande fixgpgme ou lancez les commandes précédemment citées.\n\n"
		gettext -e "    $so\aUsage :$sn\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-h|--help}$sn	   $scro——→$sn Affiche cette aide\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-v|--version}$sn	   $scro——→$sn Affiche la version du script\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-f|--functions}$s  $scro——→$sn Affiche la toutes les fonctions avec leur description\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-A|--all}$sn	   $scro——→$sn Affiche la tous les alias & fonctions avec leur description\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-a|--aliases}$sn	   $scro——→$sn Affiche la version du script\n"'

		# Parse CLI options
	while true
		switch "$cmd_args"
			case -v --version
				gettext -e "$so\aLa version de ce script est : $si$fixgpgme_version"
				return 1
			case -h --help
				eval $print_help
				return 1
			case ""
				gettext -e "$(set_color -o red)==>$(set_color normal) $(set_color -o)Tentative de réparation du bogue GPGME...$(set_color normal)\n"
				while true
					gettext -e "$(set_color -o red)::$(set_color normal) $(set_color -o)Sauvegarde des clés... $(set_color normal)$(set_color -i)(mv /etc/pacman.d/gnupg /etc/pacman.d/gnupg.sauv)$(set_color normal)\n"
					if test -d /etc/pacman.d/gnupg.sauv/
						gettext -e "$(set_color -o red) ⚠️$(set_color normal)$(set_color -o) Un dossier de sauvegarde existe déjà, suppression en cours... $(set_color -o red) ⚠️$(set_color normal)\n"
						sudo rm -rf /etc/pacman.d/gnupg.sauv/
					end
					sudo mv /etc/pacman.d/gnupg/ /etc/pacman.d/gnupg.sauv/
					gettext -e "$(set_color -o red)::$(set_color normal) $(set_color -o)Sauvegarde des fichiers de synchronisation... $(set_color normal)$(set_color -i)(mv /var/lib/pacman/sync /var/lib/pacman/sync.sauv)$(set_color normal)\n"
					if test -d /var/lib/pacman/sync.sauv/
						gettext -e "$(set_color -o red) ⚠️$(set_color normal)$(set_color -o) Un dossier de sauvegarde existe déjà, suppression en cours... $(set_color -o red) ⚠️$(set_color normal)\n"
						sudo rm -rf /var/lib/pacman/sync.sauv/
					end
					sudo mv /var/lib/pacman/sync /var/lib/pacman/sync.sauv
					gettext -e "$(set_color -o red)::$(set_color normal) $(set_color -o)Suppression des signatures en cache... $(set_color normal)$(set_color -i)(rm /var/cache/pacman/pkg/*.sig)$(set_color normal)\n"
					if test -f /var/cache/pacman/pkg/*.sig
						sudo rm /var/cache/pacman/pkg/*.sig
					end
					gettext -e "$(set_color -o red)::$(set_color normal) $(set_color -o)Initialisation des clés... $(set_color normal)$(set_color -i)(pacman-key --init)$(set_color normal)\n"
					sudo pacman-key --init
					gettext -e "$(set_color -o red)::$(set_color normal) $(set_color -o)Mise-à-jour des clés... $(set_color normal)$(set_color -i)(pacman-key --refresh-keys)$(set_color normal)\n"
					sudo pacman-key --refresh-keys
					gettext -e "$(set_color -o red)::$(set_color normal) $(set_color -o)Peuplement des clés... $(set_color normal)$(set_color -i)(pacman-key --populate)$(set_color normal)\n"
					sudo pacman-key --populate
					gettext -e "$(set_color -o red)::$(set_color normal) $(set_color -o)Mise à jour totale via le script $(set_color -oi red)update$(set_color normal)$(set_color -o)... $(set_color normal)$(set_color -i)(update --skip-mirrorlist)$(set_color normal)\n"
					update --skip-mirrorlist
					return 1
				end
				gettext -e "$(set_color -o red)Erreur lors de la réparation du bogue...$(set_color normal)\n"
				gettext -e "Pour résoudre le problème, essayez de réinstaller les paquets suivants $(set_color -oi)archlinux-keyring garuda-keyring$(set_color normal) en utilisant les commandes :\n"
				gettext -e "$(set_color -i)sudo pacman -Sy archlinux-keyring && sudo pacman -Ssy garuda-keyring$(set_color normal)\n"
				gettext -e "... Puis relancez la commande fixgpgme ou lancez les commandes suivantes :\n"
				gettext -e "$(set_color -i)sudo mv /etc/pacman.d/gnupg /etc/pacman.d/gnupg.sauv$(set_color normal)\n"
				gettext -e "$(set_color -i)sudo mv /var/lib/pacman/sync /var/lib/pacman/sync.sauv$(set_color normal)\n"
				gettext -e "$(set_color -i)sudo rm /var/cache/pacman/pkg/*.sig$(set_color normal)\n"
				gettext -e "$(set_color -i)sudo pacman-key --init$(set_color normal)\n"
				gettext -e "$(set_color -i)sudo pacman-key --refresh-keys$(set_color normal)\n"
				gettext -e "$(set_color -i)sudo pacman-key --populate$(set_color normal)\n"
				gettext -e "$(set_color -i)update --skip-mirrorlist$(set_color normal)\n"
			case \*
				gettext -e "fixgpgme: $(set_color red)Option inconnue: \"$cmd_arg\"$(set_color -i red)\nVoir l'aide :$(set_color normal) $(set_color -i)fixgpgme --help$(set_color normal)" >&2 && return 1
		end
	end
end