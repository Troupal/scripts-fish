function copycat --argument-names cmd_args --description "Commande permettant de copier la sortie cat d'un fichier dans le press-papier."
	set --local copy_cat_version 	1.0
	set --local desc	"\t$scbo\aOutils permettant de copier la sortie $scmo\acat$scbo d'un ou plusieurs fichier⋅s ⩙ dossier⋅s en indiquant juste son ou ses chemin⋅s.$sn"
	
	argparse v/version h/help m/multi -- $argv
	or begin
		gettext -e "$scr\alt : \"$scrub$cmd_args$scr\" : $scr Option inconnue\n"
		gettext -e "$scr\aVoir l'aide :$scb lt$scbi --help$scn"
		return 121
	end
	
	if set -q _flag_help
		gettext -e "$scm\aDescription :$scb\n"
		gettext -e "$scbo $desc$scb\n"
		gettext -e "$scm\aUsage :$scn\n"
        gettext -e "\t$scbo\acopycat $scbio\a[OPTIONS]$scb $scbio\a[CHEMIN]$sn\n"
        gettext -e "$scm\aOptions disponibles :$scn\n"
		gettext -e "\t$scbo\alt $scbi{-h|--help}$scbo 		→ Affiche cette aide\n"
		gettext -e "\t$scbo\alt $scbi{-v|--version}$scbo 	→ Affiche la version du script\n"
		return 1
	end

	# Affichage de la version
	if set -q _flag_version
		gettext -e "$so\aLa version de ce script est : $sio$copy_cat_version"
		return 1
	end

	# Vérifie si $argv est vide
	if test (count $argv) -eq 0
		gettext -e "$scoo\aProblème : Aucun chemin vers un fichier n'a été fourni !$sn"
		return 1
	end

	# Liste pour stocker les chemins valides
	set valid_paths
	set warnings ""

    # Vérifie chaque chemin fourni
    for file_path in $argv
        # Expand tilde for home directory
        set file_path (eval echo $file_path)

        if test -d $file_path
            # Si c'est un dossier, vérifier s'il contient des fichiers
            set files (eza $file_path 2>/dev/null)
            if test (count $files) -gt 0
                # Avertir l'utilisateur et ajouter tous les fichiers
                for file in $files
                    if test -f $file_path/$file
						if string match -r '.*/$' $file_path &>/dev/null
							set valid_paths $valid_paths "$file_path$file"
                        	set warnings "$warnings\n$scg\aLe fichier \"$scmio$file_path$file$scg\" va être copié.$sn"
						else
							set valid_paths $valid_paths "$file_path/$file"
                        	set warnings "$warnings\n$scg\aLe fichier \"$scmio$file_path/$file$scg\" va être copié.$sn"
						end
                    end
                end
            else
                # Dossier vide
                set warnings "$warnings\n$sco\aLe dossier \"$scmio$file_path$sco\" est vide et va donc être ignoré.$sn"
            end
        else if test -e $file_path
            # Si c'est un fichier, l'ajouter à la liste des chemins valides
            set valid_paths $valid_paths $file_path
            set warnings "$warnings\n$scg\aLe fichier \"$scmio$file_path$scg\" va être copié.$sn"
        else
            # Si ce n'est ni un fichier ni un dossier, avertir l'utilisateur
            set warnings "$warnings\n$sco\aLe chemin \"$scmio$file_path$sco\" n'existe pas et va donc être ignoré.$sn"
        end
    end

	# Afficher les avertissements
	if test -n "$warnings"
		echo -e "$warnings"
	end

	# Vérifie si des chemins valides ont été trouvés
	if test (count $valid_paths) -eq 0
		gettext -e "$scro\aErreur : Aucun fichier valide à copier dans le presse-papiers !$sn"
		return 1
	end


	# Copie le contenu des fichiers dans le presse-papiers
	cat $valid_paths | xclip -selection clipboard

	set valid_paths $scmio$valid_paths
	if test $status -eq 0
		# Afficher les fichiers copiés
		set copied_files (string join "$scmio " $valid_paths)
		gettext -e "$scgo\aLe(s) fichier(s) suivant(s) ont bien été copiés dans le presse-papiers :$scmoo $copied_files$scgo !$sn"
	else
		gettext -e "$scro\aErreur : Échec de la copie du fichier dans le presse-papiers !$sn"
		return 1
	end
end
