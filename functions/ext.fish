function ext --argument-names cmd_args --description "Fonction permettant de raturer, doublement souligner, surligner ou faire clignoter un texte"
  set -l i            "\033["
  set -l ie           "m"
  set -l c            "5"   # Clignotement du texte
  set -l d            "21"  # Double soulignement du texte
  set -l s            "53"  # Surlignement du texte
  set -l r            "9"   # Raturage du texte
  set -l g                        # Option du texte (1→c;2→d;3→s;4→r)
  set -l o            false       # Option des options (false→!_flag_ext;true→_flag_ext)
  set -l e            "\033[0m"   # Supression du formatage du texte
  set -l ext_version  0.1         # Version du script
  set -l cmd          ext         # Nom du script
  set -l desc         "Fonction permettant de raturer, doublement souligner, surligner ou faire clignoter un texte en le formattant de manière lisible"
	set --local print_help 'gettext -e "    $so\aDescription :$sn\n"
		gettext -e "    \t$desc\n"
		gettext -e "    $so\aUsage :$sn\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-h|--help}$sn      \t\t\t$scro——→$sn Affiche cette aide\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-v|--version}$sn   \t\t\t$scro——→$sn Affiche la version du script\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-f|--format}$sn    \t\t\t$scro——→$sn Précise le type de formatage appliqué ($scgi\a\033[5mclignotement$sn, $scgi\a\033[21mdouble soulignement$sn, $scgi\a\033[53msurlignement$sn ou $scgi\a\033[9mraturage$sn)\n"
    gettext -e "    \t$scbo\a$cmd $scbi{-cdsr|--clig --rdsou -- sur --rat}$sn $scro——→$sn Les types de formatages peuvent être cumulés\n"
		gettext -e "\n    \t$sio\aEn sélectionnant le formatage appliqué :$sn\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-f|--format}$sn $scgi{clig|rdsou|sur|rat}$sn $scoo\atexte à formater$sn\n"
		gettext -e "    \t\t$scro⤷$sn   $suo\aAppliquer le formatage$sn $scgi\a\033[5mclignotement$sn$suo\a,$sn $scgi\a\033[21mdouble soulignement$sn$suo,$sn $scgi\a\033[53msurlignement$sn $suo\aou$sn $scgi\a\033[9mraturage$sn $suo\aau$sn $scoo\atexte$sn\n"
    gettext -e "    \t$scbo\a$cmd $scbi{-f|--format}$sn $scgi{clig|rdsou|sur|rat}$sn $scmo\a(set_color $scmi\aoption$scmo)$sn $scoo\atexte à formater$sn\n"
		gettext -e "    \t\t$scro⤷$sn   $suo\aAppliquer le formatage$sn $scgi\a\033[5mclignotement$sn$suo\a,$sn $scgi\a\033[21mdouble soulignement$sn$suo,$sn $scgi\a\033[53msurlignement$sn $suo\aou$sn $scgi\a\033[9mraturage$sn $suo\aavec la$sn $scmo\acouleur définit$sn $suo\aau$sn $scoo\atexte$sn\n"
		gettext -e "\n    \t$sio\aAvec le formatage \033[5mClignotant$scio :$sn\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-c|--clig}$sn $scoo\atexte à formater$sn\n"
		gettext -e "    \t\t$scro⤷$sn   $suo\aAppliquer le formatage$sn $scgi\a\033[5mclignotement$sn $suo\aau$sn $scoo\atexte$sn\n"
    gettext -e "    \t$scbo\a$cmd $scbi{-c|--clig}$sn $scmo\a(set_color $scmi\aoption$scmo)$sn $scoo\atexte à formater$sn\n"
		gettext -e "    \t\t$scro⤷$sn   $suo\aAppliquer le formatage$sn $scgi\a\033[5mclignotement$sn $suo\aavec la$sn $scmo\acouleur définit$sn $suo\aau$sn $scoo\atexte$sn\n"
		gettext -e "\n    \t$sio\aAvec le formatage \033[21mDoucle soulignement$scio :$sn\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-d|--dsou}$sn $scoo\atexte à formater$sn\n"
		gettext -e "    \t\t$scro⤷$sn   $suo\aAppliquer le formatage$sn $scgi\a\033[21mdouble soulignement$sn $suo\aau$sn $scoo\atexte$sn\n"
    gettext -e "    \t$scbo\a$cmd $scbi{-d|--dsou}$sn $scmo\a(set_color $scmi\aoption$scmo)$sn $scoo\atexte à formater$sn\n"
		gettext -e "    \t\t$scro⤷$sn   $suo\aAppliquer le formatage$sn $scgi\a\033[21mdouble soulignement$sn $suo\aavec la$sn $scmo\acouleur définit$sn $suo\aau$sn $scoo\atexte$sn\n"
		gettext -e "\n    \t$sio\aAvec le formatage \033[53mSurlignement$scio :$sn\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-s|--sur}$sn $scoo\atexte à formater$sn\n"
		gettext -e "    \t\t$scro⤷$sn   $suo\aAppliquer le formatage$sn $scgi\a\033[53msurlignement$sn $suo\aau$sn $scoo\atexte$sn\n"
    gettext -e "    \t$scbo\a$cmd $scbi{-s|--sur}$sn $scmo\a(set_color $scmi\aoption$scmo)$sn $scoo\atexte à formater$sn\n"
		gettext -e "    \t\t$scro⤷$sn   $suo\aAppliquer le formatage$sn $scgi\a\033[53msurlignement$sn $suo\aavec la$sn $scmo\acouleur définit$sn $suo\aau$sn $scoo\atexte$sn\n"
		gettext -e "\n    \t$sio\aAvec le formatage \033[9mRaturage$scio :$sn\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-r|--rat}$sn $scoo\atexte à formater$sn\n"
		gettext -e "    \t\t$scro⤷$sn   $suo\aAppliquer le formatage$sn $scgi\a\033[9mraturage$sn $suo\aau$sn $scoo\atexte$sn\n"
    gettext -e "    \t$scbo\a$cmd $scbi{-r|--rat}$sn $scmo\a(set_color $scmi\aoption$scmo)$sn $scoo\atexte à formater$sn\n"
		gettext -e "    \t\t$scro⤷$sn   $suo\aAppliquer le formatage$sn $scgi\a\033[9mraturage$sn $suo\aavec la$sn $scmo\acouleur définit$sn $suo\aau$sn $scoo\atexte$sn\n"'
  argparse v/version h/help c/clig d/dsou s/sur r/rat "f/format=?" -- $argv &>/dev/null
  set -l a1           $argv[1]
  set -l a2           $argv[2]

  if set -q _flag_help
    eval $print_help
    return 1
  else if set -q _flag_version
    gettext -e "$so\a La version de ce script est : $si$ext_version"
    return 1
  end
  if set -q _flag_clig
    if test -z $g
      set g "$g$c"
    else
      set g "$g;$c"
    end
  end
  if set -q _flag_dsou
    if test -z $g
      set g "$g$d"
    else
      set g "$g;$d"
    end
  end
  if set -q _flag_sur
    if test -z $g
      set g "$g$s"
    else
      set g "$g;$s"
    end
  end
  if set -q _flag_rat
      if test -z $g
      set g "$g$r"
    else
      set g "$g;$r"
    end
  end
  if set -q _flag_format
    set o true
    if test "$_flag_format[1]" = clig
      set g "$c"
    else if test "$_flag_format[1]" = dsou
      set g "$d"
    else if test "$_flag_format[1]" = sur
      set g "$s"
    else if test "$_flag_format[1]" = rat
      set g "$r"
    end
  else if test -z $cmd_args
		gettext -e "⚠ $scbroio\aVous n'avez pas entré d'arguments !$sn⚠\n$scgi\aVoici commment fonctionne la commande…$sn\n"
     eval $print_help
    return 1
  end

  set g "$i$g$ie"

  if test (count $argv) = 1
    set g "$g$a1$e"
    echo -e "$g"
  else if test (count $argv) = 2
    set g "$a1$g$a2$e"
    echo -e "$g"
  else if test (count $argv) > 2
	  gettext -e "⚠ $scbroio\aVous n'avez pas entré le bon nombre d'arguments !$sn⚠\n$scgi\aVoici commment fonctionne la commande…$sn\n"
    eval $print_help
    return 1
  end
end
