# Fonction utilisant la bibliothèque Python deepl-translate et permettant de traduire un texte dans différents langages et de le copier par la même occasion dans le presse-papier.
#
#	TODO ::
#		- [x] Add the translation list of supported languages shortcut for the help
#		- [ ] Remove the code that is not used !
#		- [ ] Make a GUI alternative for tradc
#		- [ ] Add the all the options of the deepl-translate library for the script
#		- [ ] Make a completion file for the script
#

function tradc --argument-names cmd_arg --description "Outil de traduction utilisant la librairie python \"deepl-translate\" et recopiant le résultat dans le presse-papier."
	set --local tradc_version 1.0
	set --local cmd tradc
	set --local supported_languages BG ZH CS DA NL EN ET FI FR DE EL HU IT JA LV LT PL PT RO RU SK SL ES SV
	set --local langages_supportes '$scmo\aBG$sn = $scoo\"bulgare\"$sn' '$scmo\aZH$sn = $scoo\"chinois\"$sn' '$scmo\aCS$sn = $scoo\"tchèque\"$sn' '$scmo\aDA$sn = $scoo\"danois\"$sn' '$scmo\aNL$sn = $scoo\"néerlandais\"$sn' '$scmo\aEN$sn = $scoo\"anglais\"$sn' '$scmo\aET$sn = $scoo\"estonien\"$sn' '$scmo\aFI$sn = $scoo\"finois\"$sn' '$scmo\aFR$sn = $scoo\"français\"$sn' '$scmo\aDE$sn = $scoo\"allemand\"$sn' '$scmo\aEL$sn = $scoo\"grecque\"$sn' '$scmo\aHU$sn = $scoo\"hongrois\"$sn' '$scmo\aIT$sn = $scoo\"italien\"$sn' '$scmo\aJA$sn = $scoo\"japonais\"$sn' '$scmo\aLV$sn = $scoo\"letton\"$sn' '$scmo\aLT$sn = $scoo\"lituanien\"$sn' '$scmo\aPL$sn = $scoo\"polonais\"$sn' '$scmo\aPT$sn = $scoo\"portugais\"$sn' '$scmo\aRO$sn = $scoo\"roumain\"$sn' '$scmo\aRU$sn = $scoo\"russe\"$sn' '$scmo\aSK$sn = $scoo\"slovaque\"$sn' '$scmo\aSL$sn = $scoo\"slovène\"$sn' '$scmo\aES$sn = $scoo\"espagnole\"$sn' '$scmo\aSV$sn = $scoo\"suèdois\"$sn'
	set --local desc "Ce script permet de traduire un texte via la librairie $si\apython$sn $sccio\adeepl-translate$sn avant de le copier dans le presse-papier."

	# Check if the deepl-translate python librairy and the command line interface to the X11 clipboard xclip exists, if not, install it
	if ! python -c "import deepl" &> /dev/null
		echo La librairie python (set_color -ui)deepl-translate(set_color normal) n\'a pas été trouvée, installation...
		pipx install deepl-translate
	end

	# Check if the command line interface to the X11 clipboard xclip exists, if not, install it
	if ! type xclip &> /dev/null
		echo La commande (set_color -ui)xclip(set_color normal) n\'a pas été trouvée, installation \(veuillez rentrer votre mot-de-passe afin d\'excuter pacman\)...
		pkexec pacman -Sq --noconfirm xclip
	end

	set --local sortie \n" "
	for i in 1 2 3 4
		set sortie $sortie (echo ,(eval echo $langages_supportes[(math 6'*'$i-6+1)]),(eval echo $langages_supportes[(math 6'*'$i-6+2)]),(eval echo $langages_supportes[(math 6'*'$i-6+3)]),(eval echo $langages_supportes[(math 6'*'$i-6+4)]),(eval echo $langages_supportes[(math 6'*'$i-6+5)]),(eval echo $langages_supportes[(math 6'*'$i-6+6)]),)\n" "
	end

	# Parse CLI options
	while true
		switch "$cmd_arg"
			case -v --version
				gettext -e "$so\aLa version de ce script est : $si$lt_version"
				return 1
			case "" -h --help
				gettext -e "    $so\aDescription :$sn\n"
				gettext -e "    \t$desc\n"
				gettext -e "    \t\tPour l'utiliser, veuillez d'abord préciser la $scmo\alangue d'origine$sn suivie de la $scmo\alangue de destination$sn, suivit du $scoio\atexte$sn.\n\n"
				gettext -e "    \t\tVous pouvez préciser les langues soit $scgio\aen entier$sn, soit $scgio\avia une abbréviation avec deux caractères$sn :\n"
				echo \t$scro"  ╭──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮"
				echo \t$scro"  │"$scb\t\t\t\t\t\t\t" Langages supportés"\t\t\t\t\t\t\t$scro"     │"$sn
				echo \t$scro"  ├────────────────────┬────────────────────┬────────────────────┬─────────────────────┬───────────────────────┬─────────────────────┤"
				echo \t$sortie | column -t -s ',' -o $scro"  │   "$sn
				echo \t$scro"  ╰────────────────────┴────────────────────┴────────────────────┴─────────────────────┴───────────────────────┴─────────────────────╯"
				gettext -e "    $so\aUsage :$sn\n"
				gettext -e "    \t$scbo\a$cmd $scbi{-h|--help}$sn\t\t\t\t 		→ Affiche cette aide\n"
				gettext -e "    \t$scbo\a$cmd $scbi{-v|--version}$sn\t\t\t\t 		→ Affiche la version du script\n"
				gettext -e "    \t$scbo\a$cmd $scmo\$origine_lang$sn $scmo\$destination_lang$sn $scoio\"Le texte à traduire\"$sn\t→ Lance une traduction et copie le resultat dans le presse-papier."
				return 1
			case $supported_languages
				break
			case \*
				gettext -e "tradc: $(set_color red)Option inconnue: \"$cmd_arg\"$(set_color -i red)\nVoir l'aide :$(set_color normal) $(set_color -i)tradc --help$(set_color normal)" >&2 && return 1
		end
	end
	#	set args (getopt -s sh abc: $argv); or help_exit
	#	set args (fish -c "for el in $args; echo \$el; end")

	#	set PARSED_OPTIONS $(getopt --options="h,v" --longoptions="help,version" --name "$0" -- "$argv")
	#	set PARSED_OPTIONS $(argparse --name=tradc 'h/help' 'v/version' -- $argv; or return)
	#	if [[ $status -ne 0 ]]; then
	#		echo -e "\033[1;31m\nFailed to parse CLI options\n\033[0m"
	#	end
	#	eval set -- "$PARSED_OPTIONS"
	#	while true
	#		switch (eval $argv[1])
	#			case -h or --help
	#			    echo -e "Ce script permet de traduire un texte via la librairie python \"deepl-translate\""
	#				echo -e "\tPour l'utiliser, veuillez d'abord préciser la langue d'origine suivie de la langue de destination, suivit du texte :"
	#				echo -e "\tVous pouvez préciser les langues soit en entier, soit via une abbréviation avec deux caractères."
	#			case -v or --version
	#			    echo -e $version
	#			case  --
	#				break
	#		end
	#	end
	#	while true
	#	switch (eval $argv[1])
	#		case -h or --help
	#		    echo -e "Ce script permet de traduire un texte via la librairie python \"deepl-translate\""
	#			echo -e "\tPour l'utiliser, veuillez d'abord préciser la langue d'origine suivie de la langue de destination, suivit du texte :"
	#			echo -e "\tVous pouvez préciser les langues soit en entier, soit via une abbréviation avec deux caractères."
	#		case -v or --version
	#		    echo -e $version
	#		case  --
	#			break
	#		end
	#	end
	# set traduction (python -m deepl $argv[1] $argv[2] -t $argv[3])
	set traduction (deepltrad $argv[1] $argv[2] -t $argv[3])
	if test $argv[1] = "BG"
		set lang1 "bulgare"
	else if test $argv[1] = "ZH"
		set lang1 "chinois"
	else if test $argv[1] = "CS"
		set lang1 "tchèque"
	else if test $argv[1] = "DA"
		set lang1 "danois"
	else if test $argv[1] = "NL"
		set lang1 "néérlandais"
	else if test $argv[1] = "EN"
		set lang1 "anglais"
	else if test $argv[1] = "ET"
		set lang1 "estonien"
	else if test $argv[1] = "FI"
		set lang1 "finois"
	else if test $argv[1] = "FR"
		set lang1 "français"
	else if test $argv[1] = "DE"
		set lang1 "allemand"
	else if test $argv[1] = "EL"
		set lang1 "grecque"
	else if test $argv[1] = "HU"
		set lang1 "hongrois"
	else if test $argv[1] = "IT"
		set lang1 "italien"
	else if test $argv[1] = "JA"
		set lang1 "japonais"
	else if test $argv[1] = "LV"
		set lang1 "letton"
	else if test $argv[1] = "LT"
		set lang1 "lituianien"
	else if test $argv[1] = "PL"
		set lang1 "polonais"
	else if test $argv[1] = "PT"
		set lang1 "portugais"
	else if test $argv[1] = "RO"
		set lang1 "roumain"
	else if test $argv[1] = "RU"
		set lang1 "russe"
	else if test $argv[1] = "SK"
		set lang1 "slovaque"
	else if test $argv[1] = "SL"
		set lang1 "slovène"
	else if test $argv[1] = "ES"
		set lang1 "espagnole"
	else if test $argv[1] = "SV"
		set lang1 "suèdois"
	end
	if test $argv[2] = "BG"
		set lang2 "bulgare"
	else if test $argv[2] = "ZH"
		set lang2 "chinois"
	else if test $argv[2] = "CS"
		set lang2 "tcheque"
	else if test $argv[2] = "DA"
		set lang2 "danois"
	else if test $argv[2] = "NL"
		set lang2 "néérlandais"
	else if test $argv[2] = "EN"
		set lang2 "anglais"
	else if test $argv[2] = "ET"
		set lang2 "estonien"
	else if test $argv[2] = "FI"
		set lang2 "finois"
	else if test $argv[2] = "FR"
		set lang2 "français"
	else if test $argv[2] = "DE"
		set lang2 "allemand"
	else if test $argv[2] = "EL"
		set lang2 "grecque"
	else if test $argv[2] = "HU"
		set lang2 "hongrois"
	else if test $argv[2] = "IT"
		set lang2 "italien"
	else if test $argv[2] = "JA"
		set lang2 "japonais"
	else if test $argv[2] = "LV"
		set lang2 "letton"
	else if test $argv[2] = "LT"
		set lang2 "lituianien"
	else if test $argv[2] = "PL"
		set lang2 "polonais"
	else if test $argv[2] = "PT"
		set lang2 "portugais"
	else if test $argv[2] = "RO"
		set lang2 "roumain"
	else if test $argv[2] = "RU"
		set lang2 "russe"
	else if test $argv[2] = "SK"
		set lang2 "slovaque"
	else if test $argv[2] = "SL"
		set lang2 "slovène"
	else if test $argv[2] = "ES"
		set lang2 "espagnole"
	else if test $argv[2] = "SV"
		set lang2 "suèdois"
	end
	echo $traduction | xclip -selection clipboard && echo $scwo"Le texte "$scmo$lang1$sn" $scy"\"$argv[3]\"$sn \n$scgo"⤷"$sn $scwo"Se traduit en "$scmo$lang2$sn $scwo"par :" $scoo\"$traduction\"$sn; echo \t$scgo"🗸 Traduction copiée dans le presse-papier ! 🗸"$sn;
	set -e lang1
	set -e lang2
end
