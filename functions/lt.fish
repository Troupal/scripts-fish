# Commande ls avec de nombreux détails et en arborescence.
#
#	TODO ::
#		- [x] Pousser les modifications sur le git
#		- [ ] 
#
#	FIXME ::
#		- [x] Ségmentation des noms de fichiers avec espace :
#			PROBLÈME :
#				Lorsque l'on utilise l'auto-complétion, les fichiers avec les espaces indiquent une erreur, il segmente à chaque espace :
#					$ lt Nouveau\ document\ 1.2022_11_04_04_44_01.0.svg		\
#						"Nouveau": No such file or directory (os error 2)	\
#						"document": No such file or directory (os error 2)
#			RÉSOLUTION :
#				La façon dont gère la fonctions les arguments ne fonctionne pas comme ça devrait. Chaque espaces suivis de caractères sont
#					considérés comme de nouveaux arguments. Pour régler ce problème, il faut limiter les arguments aux seuls premiers
#					chiffres isolés (il existe une désubigüation pour ça) et aux vrais arguments ayant une complétion.
#		- [x] Non prise en compte du cas où il n'y a pas d'arguments
#			PROBLÈME :
#				Lorsqu'il n'y pas d'argument de donné, le script dit que l'argument n'est ni un entier ni un chemin.
#			RÉSOLUTION :
#				Prendre en compte le cas où le nombre d'argument est nul 
#
#	VERSION ::
#		1.0 :: 11-04-2022
#			Première version complète
#		1.1 :: 13-01-2025
#			Ajout de la possibilité d'ignorer des fichiers ou dossiers spécifiés.
#			Ajout de la prise en compte des autres options de eza
#		1.2 :: 26-01-2025
#			Correction du problème de zéro arguments, mise-à-jour de la documentation et des version.

function lt --argument-names cmd_args --description "Commande ls avec de nombreux détails et en arborescence."

	# Définition des variables
	set --local lt_version 	1.2 										# Définition du numéro de la version
	set --local desc 		"\t$scb\aCommande $scbo\als$scb via sa réecriture $sco io\aRUST$scb $scmo\aeza$scb avec de nombreux détails, \n\ten arborescence avec la possibilité d'afficher les fichiers cachés \n\tainsi que de choisir la profondeur d'exploration récursive des dossiers."
#	set lsn 				"exa -@lxhTF --color-scale=all --color=always --header --icons --octal-permissions --group-directories-first"
#	set lst 				"exa -a@lxhTF --color-scale=all --color=always --header --icons --octal-permissions --group-directories-first"
  set default_opts 		"-a@lxhTF --color-scale=all --color=always --header --icons --octal-permissions --group-directories-first"
  set nonall_opts 		"-@lxhTF --color-scale=all --color=always --header --icons --octal-permissions --group-directories-first"
  set lst 				"eza $default_opts"
  set --local depth
  set --local path "."
  set --local unknown_options ""

	# Analyse des options via argparse
  argparse -i v/version h/help n/nonall I/ignore= -- $argv
  # argparse v/version h/help n/nonall I/ignore= --min-args=0 --max-args=-1 -- $argv &>/dev/null
	or begin
		gettext -e "$scr\alt : \"$scrub$cmd_args$scr\" : $scr Option inconnue\n"
		gettext -e "$scr\aVoir l'aide :$scb lt$scbi --help$scn"
		return 121
	end

	if set -q _flag_help
		gettext -e "$scm\aDescription :$scb\n"
		gettext -e "$scbo $desc$scb\n"
		gettext -e "$scm\aUsage :$scn\n"
    gettext -e "\t$scbo\alt $scbio\a[OPTIONS]$scb $scbio\a[NIVEAU]$scb $scbio\a[CHEMIN]$sn\n"
    gettext -e "$scm\aOptions disponibles :$scn\n"
		gettext -e "\t$scbo\alt $scbi{-n|--nonall}$scb 	→ N'affiche pas les fichiers cachés\n"
		gettext -e "\t$scbo\alt $scbi{-I|--ignore}$scb 	→ Ignore les fichiers correspondant au pattern (glob)\n"
		gettext -e "\t$scbo\alt $scbi{[OPTIONS]}$scb 		→ Vous pouvez également ajouter les autres options de $scmo\aeza$scb sous la forme \"{$scbio-o|--option$scb} {$scbio\"argument\"$scb}\" avec les doubles quote s'il y a un argument\n"
		gettext -e "\t$scbo\alt $scbi{-h|--help}$scb 		→ Affiche cette aide\n"
		gettext -e "\t$scbo\alt $scbi{-v|--version}$scb 	→ Affiche la version du script\n"
    gettext -e "$scm\aUtilisation :$scn\n"
		gettext -e "\t$scbo\alt$scb 			→ Liste des fichiers et dossiers (dont les fichiers cachés et liens) avec une arboresence complète\n" 
		gettext -e "\t$scbo\alt $scbu{/chemin/}$scb 		→ Liste des fichiers et dossiers (dont les fichiers cachés et liens) avec une arboresence complète depuis le chemin $scbu\a/chemin/$scb\n" 
		gettext -e "\t$scbo\alt $scbu{0..n}$scb 		→ Liste des fichiers et dossiers (dont les fichiers cachés et liens) avec une arboresence sur $scu\an$scn niveau de profondeur$scb\n" 
		gettext -e "\t$scbo\alt $scbu{0..n}$scb $scbu{/chemin/}$scb 	→ Liste des fichiers et dossiers (dont les fichiers cachés et liens) avec une arboresence sur $scu\an$scn niveau depuis le chemin $scbu\a/chemin/$scb\n" 
		gettext -e "\t$scbo\alt $scbi-n$scb 			→ Liste des fichiers et dossiers sans les fichiers cachés avec une arboresence complète$scb\n" 
		gettext -e "\t$scbo\alt $scbi-n $scbu{/chemin/}$scb	→ Liste des fichiers et dossiers sans les fichiers cachés avec une arboresence complète depuis le chemin $scbu\a/chemin$scb\n" 
		gettext -e "\t$scbo\alt $scbi-n $scbu{0..n}$scb 		→ Liste des fichiers et dossiers sans les fichiers cachés avec une arboresence sur $scu\an$scn niveau de profondeur\n"
		gettext -e "\t$scbo\alt $scbi-n $scbu{0..n}$scb $scbu{/chemin/}$scb → Liste des fichiers et dossiers sans les fichiers cachés avec une arboresence sur $scu\an$scn niveau depuis le chemin $scbu\a/chemin/$sscb\n"
		gettext -e "\t$scbo\alt $scbi-I \"GLOB\" $scn{$scbi-n$scn}	→ Liste des fichiers et dossiers avec une arboresence ne prennant pas en compte les dossiers ou fichiers correspondant au paterne \"GLOB\"$scn\n" 
		return 1
	end

	# Affichage de la version
	if set -q _flag_version
		gettext -e "$so\aLa version de ce script est : $si$lt_version"
		return 1
	end
	
	# Ignore les fichiers cachés
	if set -q _flag_nonall
		set lst "eza $nonall_opts"
		#DEBUG echo "position 1 : $lst"
	end

	# Ajout de l'option `--ignore-glob` si spécifiée
	if set -q _flag_ignore
    set lst "$lst --ignore-glob='$_flag_ignore'"
  end

  # Collecte des arguments restants pour profondeur et chemin
  set --local remaining_args $argv
	set --local other_options ""
	set --local non_options ""

	# Étape 1 : Extraction des options et de leurs arguments associés
	while test (count $remaining_args) -gt 0
		# DEBUG echo $remaining_args[1]
		# DEBUG echo $remaining_args[2]
		# DEBUG echo $remaining_args[1]
	  if string match -qr -- '^-' $remaining_args[1]
			# Ajouter l'option à $other_options
			set other_options "$other_options $remaining_args[1]"
			# DEBUG echo "remaining_args = $remaining_args"
			# DEBUG echo "other_options 1 = $other_options"
			# DEBUG echo "remaining_args 1 = $remaining_args"
			
			# # Vérifier si l'option a un argument associé
			if test (count $remaining_args) -ge 2; and string match -qr '^".*"' $remaining_args[2]
				set other_options "$other_options $remaining_args[2]"
				# DEBUG echo "other_options 2 = $other_options"
				set remaining_args $remaining_args[3..-1]
				# DEBUG echo "remaining_args 2 = $remaining_args"
			else
				set remaining_args $remaining_args[2..-1]
				# DEBUG echo "remaining_args 3 = $remaining_args"
			end
		else
			# Si ce n'est pas une option, l'ajouter à $non_options
			set non_options $remaining_args
			break
		end
	end

	# Étape 2 : Ajout des options collectées à $lst
	if test -n "$other_options"
		set lst "$lst $other_options"
	end

	# Étape 3 : Traitement des arguments non optionnels (profondeur et chemin)
	set arg_count (count $non_options)

	#DEBUG 
	echo "argv1 : $argv et le total d'argument est : $(count $argv)"
	echo "non_options : $non_options"
	echo "depth : $depth"
	echo "path : $path"

	if test $(count $argv) -eq 0
		set path "."
	else if test $arg_count -eq 1
		if command test $non_options[1] -eq $non_options[1] 2>/dev/null
			set depth $non_options[1]
		else if command test -e $non_options[1]
			if test $non_options[1] -eq ""
				set path "."
			else
				set path $non_options[1]
			end
		else
    	gettext -e "$scro\aErreur : \"$non_options[1]\" n'est ni un chemin valide ni un entier.$scn\n"
      return 121
    end
  else if test $arg_count -eq 2
    if command test $non_options[1] -eq $non_options[1] 2>/dev/null -a -e $non_options[2]
      set depth $non_options[1]
      set path $non_options[2]
    else if command test $non_options[2] -eq $non_options[2] 2>/dev/null -a -e $non_options[1]
      # Inversion des arguments profondeur/chemin
      set depth $non_options[2]
      set path $non_options[1]
    else
  		gettext -e "$scro\aErreur : Ordre des arguments incorrect.$scn\n"
			return 121
  	end
  else if test $arg_count -eq 0
    set path "."
  else
    gettext -e "$scr\aErreur : Trop d'arguments ou arguments invalides.$scn\n"
    return 121
  end

  # Confirmation pour désambiguïsation
  if test -n "$depth" -a -n "$path"
	if command test $depth -eq $depth 2>/dev/null -a -e $path -a $depth -eq $path
    echo "$(set_color red)\$depth::\"$depth\"$(set_color yellow) est un chiffre et un dossier, veuillez préciser si :"
    gettext -e "$scr\$argv::\"$depth\"$scy est un chiffre et un dossier, veuillez préciser si :$scn\n"
		gettext -e "\t$scbVeuillez préciser si \"$scr$depth$scb\" est :\n"
    gettext -e "\t$scbo[P]$scbu → Profondeur de l'arborescence$scn\n"
    gettext -e "\t$scbo[C]$scbu → Chemin$scn\n"
		while true
			read -l -P $(printf "\t$scb\aChoix$scb [$scbo\aP$scb/$scbo\ac$scb] ! [$scbo\aP$scb/c] ") confirm
			switch $confirm
				case c C
					set path $depth
					set depth ""
					break
				case "" p P
					set path .
					break
				case "*"
					gettext -e "$scro\aErreur : Sélection invalide. $scoo\aRéessayez…$scn\n"
				end
			end
		end
	end

  # Construction de la commande `eza`
  if test -n "$depth"
  	set lst "$lst -L=$depth"
  end
  if test -n "$path"
    set lst "$lst $path"
  end

  # Exécution finale
	# DEBUG echo "remaining_args = $remaining_args"
	# DEBUG echo "other_options = $other_options"
	# DEBUG echo "non_options = $non_options"
	# DEBUG echo "unknown_options = $unknown_options"
	# DEBUG echo "depth = $depth"
	# DEBUG echo "path = $path"
  eval $lst
end

# 	if test (count $argv) -ge 2
# 		if command test $argv[1] -ge 0 -a -e $argv[2]
# 			#DEBUG echo "argv1 est un chiffre et argv2 est un fichier qui existe"
# 			eval $lst -L=$argv[1] $argv[2]
# 		else if command test $argv[2] -ge 0 -a -e $argv[1]
# 			gettext -e "$scr\aErreur dans l'ordre des argument, voir $scb\altest $scbi--help\n"
# 			gettext -e "\n$sco\aTentative de correction:: "
# 			for i in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22
# 				gettext -e "$scro."
# 				sleep 0.01
# 			end
# 			gettext -e "\n$scyo\aInversion des argument $scbo\$argv[1]=$argv[1] $scyo&$scbo \$argv[2]=$argv[2]$scyo …\n"
# 			gettext -e "⤷$scg Commande exécutée :$scbo lt $scbi-L=$argv[2] $scbu$argv[1]\n"
# 			eval $lst -L=$argv[2] $argv[1]
# 			return 1
# 		else 
# 			gettext -e "$scr\aCommande inconnue : \"$scb\alt $scrub$argv$scr\" $scri\aArguments \"$scrub$argv$scr\" inapproprié.\nVoir l'aide :$scb lt$scbi --help$scn"
# 		end
# 	else if command test -d $argv -a $argv -ge 0 &>/dev/null
# 		echo "$(set_color red)\$argv::\"$argv\"$(set_color yellow) est un chiffre et un dossier, veuillez préciser si :"
# 		while true
# 			read -l -P $(printf "\t$scb\aVous voulez préciser la $scbu\aprofondeur de la liste$scb [$scbo\aP$scb] ou $scbu\aouvrir le dossier$scb [$scbo\ad$scb] ! [$scbo\aP$scb/d] ") confirm
# 			switch $confirm
# 				case d D
# 					eval $lst $argv
# 					break
# 				case "" p P
# 					eval $lst -L=$argv
# 					break
# 			end
# 		end
# 	else if command test $argv -ge 0 &>/dev/null
# 		#DEBUG echo "argv est un chiffre"
# 		eval $lst -L=$argv
# 	else if command test -e $argv &>/dev/null
# 		#DEBUG echo "argv est un fichier qui existe"
# 		echo $stderr $stdin $stdout
# 		eval $lst $argv
# 	else
# 		gettext -e "lt :$scr Option inconnue : \"$cmd_args\"$scri\nVoir l'aide :$scb lt$scbi --help$scn" >&2
# 		gettext -e "$scr\aCommande inconnue : \"$scb\alt $scrub$argv$scr\" $scri\aArguments \"$scrub$argv$scr\" inapproprié.\nVoir l'aide :$scb lt$scbi --help$scn"
# 		return 121
# 	end
# end
