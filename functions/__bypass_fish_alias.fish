# NOTE-description ::
# Fonction permettant de bypasser les aliases avec un simple raccourci (<\\>)
# Plusieurs cas s'offre à nous :
#	- Le prompt est vide → inscrit dans le prompt "$ command "
#	- Le prompt n'est pas vide :
#		- La commande est issue d'une fonction ou d'un alias existant
#			- La commande a des arguments → Affiche un message d'alerte en prévenant que les commandes ne sont peut-être pas compatibles et demande à l'utilisateur s'il veut poursuivre
#			- La commande n'a pas d'arguments → Remplace la commande
#		- La commande n'est pas issue d'une fonction ou d'un alias existant → prévenir qu'il ne s'agit pas d'un alias et que continuer ne servira à rien.
#
#
# TODO ::
#
function __bypass_fish_alias2 --description "fonction permettant de bypasser les aliases avec un simple raccourci"
	set -g cmdch (commandline -b)
	# ↓ Test si le prompte est vide ↓
	if test $cmdch = ""
		commandline -r "command" # ← Remplacer par "$ command"
	else
		set -l cmd (echo $cmdch | cut -d " " -f1)
		# ↓ Test si, lorsque le prompt n'est pas vide, il est bien issu d'un alias ou d'une fonction ↓
		if functions -q $cmd
			# ↓ Test si c'est bien un alias ↓
			if test (functions -HDv $cmd | cut -d \n -f5 | cut -d " " -f1) = "alias"
				echo
				echo -e "$scoo\aIl est possible que les arguments ne correspondent pas entre les fonctions…"
				while read --nchars 1 -l response --prompt-str="Voulez-vous continuer ? O/n "
					or return 1 # if the read was aborted with ctrl-c/ctrl-d
					switch $response
						case y Y o O "\r" ""
							commandline -r "command $cmdch"
							commandline -f execute
							# We break out of the while and go on with the function
							break
						case n N
							# We return from the function without printing
							echo -e "$scoo\aRemplacement annulé…"
							commandline -r $cmdch
							break
						case '*'
							# We go through the while loop and ask again
							echo "$scro\aEntrée invalide…"
							continue
					end
				end
			else
				echo
				echo -e "La commande est une fonction, pas un alias, donc aucun bypass n'est nécessaire…"
				commandline -f complete
				commandline -r $cmdch
			end
		# ↓ Demande quoi faire dans le cas inverse ↓
		else
			echo
			echo -e "La commande n'est issue ni d'une fonction, ni d'un alias, un bypass n'est donc pas nécessaire…"
			while read --nchars 1 -l response --prompt-str="Voulez-vous quand-même utiliser la fonction “command” ? O/n "
				or return 1 # if the read was aborted with ctrl-c/ctrl-d
				switch $response
				case y Y o O "\r" ""
					commandline -r "command $cmdch"
					# We break out of the while and go on with the function
					break
				case n N
					# We return from the function without printing
					echo -e "$scoo\aRemplacement annulé…"
					commandline -r $cmdch
					break
				case '*'
					# We go through the while loop and ask again
					echo "$scro\aEntrée invalide…"
					continue
				end
			end
		end
	end
end

if [ "$fish_key_bindings" = fish_vi_key_bindings ];
  bind -M insert \x1c __bypass_fish_alias2
  set cmdtoexec (commandline -b)
else
  bind \x1c __bypass_fish_alias2
end


#=================================================================================
# Ancienne version !!!
#=================================================================================
# function __bypass_fish_alias
# #	set leprompt (commandline -b)
# #	echo "le prompt : $leprompt"
# #	set -g ix (math $ix+1)
# #	echo numéro $ix
# #	echo "cas0"
# #	echo "Je suis cmdtoexec : $cmdtoexec"
# 	set -g cmdch (commandline -b)
# #	echo "  Je suis cmdch : $cmdch"
# 	switch (commandline -b)
# 	case "$cmdtoexec *" "* "
# 	     commandline -i \\
# 		 echo "1er cas"
# #	case \\$cmdtoexec
# 		switch $ref
# 		case 1
# #			echo "2ème cas"
#  			commandline -r \\$cmdtoexec
# 			function (eval echo \\$cmdtoexec)
# 				set -l arg0 (echo $cmdtoexec | awk '{print $1}')
# 				echo "L'alias \"$arg0\" de la commande entrée \"$cmdtoexec\" a été bypassée :"
# 				set -l argv0 "command $cmdtoexec"
# 				eval $argv0
# 				set -e cmdtoexec
# 			end
# 			commandline -f execute
# 		case "*"
# 			commandline -i \\
# 		end
# 	case "*"
# #		echo "3ème cas"
# 		switch $ref
# 		case 1
# #			echo "3.1ème cas"
# #			echo "Dans 3.1 cmdch = $cmdch"
# #			commandline -r \"\\$cmdch\"
# # 			set 31prompt (commandline -b | tr " " "_")
# #			set -g 31prompt "\"\\$cmdch\""
# 			set -g cle (echo $cmdch | cut -d" " -f1)
# 			set -g valeur (echo $cmdch | cut -d" " -f2-8)
# # 			echo "valeur = $valeur"
# 			for i in 1 2 3 4 5 6 7
# 				set -l h$i (echo $valeur | cut -d " " -f$i)
# 				set -l k (eval echo \u2007(echo (eval printf %s \$(eval echo h(eval echo $i)))))
# #				echo k = $k
# 				set -g l (string join \u2007 $l $k)
# 				set -g l (string replace -a \u2007\u2007 \u2007 (echo $l))
# 				set -g l (string replace -a " " "" (echo $l))
# # 				echo l =$l
# # 				echo h1 = $h1
# #				echo h2 = $h2
# #				echo h3 = $h3
# #				echo h4 = $h4
# #				echo h5 = $h5
# #				echo h6 = $h6
# #				echo h7 = $h7
# 			end
# #			set -g 31prompt \\$cle\x5d$valeur
# 			set -g 3prompt (echo \\$cle\u2007$l)
# #			set -g 3prompt (echo \\$cle\u2007$valeur)
# 			set -g 31prompt (string join \u2007 $3prompt)
# 			commandline -r $31prompt
# #			commandline -r \\$cle\ $valeur
# #			echo "prompt : $31prompt"
# 			function (eval echo $31prompt)
# #			function lance
# #				commandline -r \\$cmdch
# #				echo ""
# 				echo "L'alias \"$cle\" de la commande entrée \"$cmdch\" a été bypassée :"
# 				set -l argv0 "command $cmdch"
# 				eval $argv0
# 				set -e cmdch
# 				set -e 3prompt
# 				set -e 31prompt
# 				set -e l
# 				set -e cle
# 				set -e valeur
# 				set -e ref
# #				commandline -r \\$cmdch
# 			end
# #			set 311prompt (commandline -b)
# #			echo "prompt2 : $311prompt"
# #			commandline -f execute ;; commandline -r \\$cmdch
# #			commandline -r \\$cmdch
# 			commandline -f execute
# #			lance
# 		case "*"
# #			echo "3.2ème cas"
# 			commandline -i
# 		end
# 		set -g ref 1
# #		set -g cmdtoexec (commandline -b)
# #		commandline -C 0
# #		commandline -i \\
# #		__bypass_fish_alias
#
# 	end
# end
#
# if [ "$fish_key_bindings" = fish_vi_key_bindings ];
#   bind -Minsert \\ __bypass_fish_alias
#   set cmdtoexec (commandline -b)
# else
#   bind \\ __bypass_fish_alias
# end
