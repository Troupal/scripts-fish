# Fonction servant de pense-bête permettant d'afficher une liste de certains utilitaires pratique à utiliser
#
#	TODO ::
#		- [ ] Ajouter un mécanisme permettant d'afficher automatiquement la description de l'utilitaire
#		- [ ] Ajouter un mécanisme de sélection (comme pour paru par exemple) avec des chiffres et les flèches
#		- [ ] Ajouter des raccourcis claviers permettant d'afficher des informations supplémentaires (tel que le manuel ou l'aide) ainsi que d'insérer l'utilitaire sélection dans une commande
#		- [x] Ajouter des complétions :
#			- [x] Pour les drapeaux h/help v/version t/tool
#			- [x] Pour les arguments nécessaires pour le drapeau t/tool de sélection de l'outil de recherche
#		- [ ] Ajouter les outils suivants :
#			- [ ] Ranger
#			- [x] AG
#			- [ ] Skim
#			- [ ] 
#			- [ ] 
#	Liste des cas :
#	1.	$ search
#	2.	$ search $dossier
#	3.	$ search $recherche $dossier
#	4.	$ search -t ag
#	5.	$ search -t ag $dossier
#	6.	$ search -t ag $recherche $dossier
#	7.	$ search -t rg
#	8.	$ search -t rg $dossier
#	9.	$ search -t rg $recherche $dossier
#	10.	$ search -t ack 
#	11.	$ search -t ack $dossier
#	12.	$ search -t ack $recherche $dossier
#	13.	$ search -t grep
#	14.	$ search -t grep $dossier
#	15.	$ search -t grep $recherche $dossier
#	16.	$ search -v
#	17.	$ search -h

function search --argument-names cmd_args --description "Fonction servant à rechercher une chaîne de caractère dans les fichiers"
	set --local search_version 1.0
	set --local cmd search
	set --local desc "Fonction servant à rechercher une chaîne de caractère dans les fichiers en utilisant $scbo\askim$sn et $scbo\aag$sn."
	set --local print_help 'gettext -e "    $so\aDescription :$sn\n"
		gettext -e "    \t$desc\n"
		gettext -e "    $so\aUsage :$sn\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-h|--help}$sn      $scro——→$sn Affiche cette aide\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-v|--version}$sn   $scro——→$sn Affiche la version du script\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-t|--tool}$s      $scro——→$sn Précise l\'outil utilisé pour la recherche ($scgi\aag$sn, $scgi\arg$sn ou $scgi\agrep$sn)\n"
		gettext -e "\n    \t$sio\aEn sélectionnant l\'outil utilisé pour effectuer la recherche :$sn\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-t|--tool}$sn $scgi{ag|rg|grep}$sn\n"
		gettext -e "    \t\t$scro⤷$sn   $suo\aRechercher avec l\'outils$sn $scgi\aag$suo, $scgi\arg$suo ou$scgi grep$sn\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-t|--tool}$sn $scgi{ag|rg|grep}$sn $scmo\$Chemin$sn\n"
		gettext -e "    \t\t$scro⤷$sn   $suo\aRechercher avec l\'outils$sn $scgi\aag$suo, $scgi\arg$suo ou$scgi grep$sn dans le $scmo\$dossier à partir d\'où chercher$sn\n"
		gettext -e "    \t$scbo\a$cmd $scbi{-t|--tool}$sn $scgi{ag|rg|grep}$sn $scoo\"expression\"$sn $scmo\$Chemin$sn\n"
		gettext -e "    \t\t$scro⤷$sn   $suo\aRechercher avec l\'outils$sn $scgi\aag$suo, $scgi\arg$suo ou$scgi grep$sn l\'$scoo\"expression à chercher\"$sn dans le $scmo\$dossier à partir d\'où chercher$sn\n"'
	set --local tool
	argparse v/version h/help 't/tool=?' -- $argv &>/dev/null
#	17.	$ search -h
	if set -q _flag_help
		eval $print_help
		return 1
#	16.	$ search -v
	else if set -q _flag_version
		gettext -e "$so\aLa version de ce script est : $si$lt_version"
		return 1
	else if set -q _flag_tool
		if test "$argv[1]" = "ag"
			echo 1.1
			#	6.	$ search -t ag $recherche $dossier
			if test (count $argv) = 3
				echo 1.1.1 argv1=$argv[1] argv2=$argv[2] argv3=$argv[3]
				sk --ansi -i -c (eval echo 'ag -u --stats --color $argv[2] $argv[3]') --delimiter : --preview 'bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}' --preview-window +{2}-/2
				#	5.	$ search -t ag $dossier
			else if test (count $argv) = 2
				echo 1.1.2 argv1=$argv[1] argv2=$argv[2]
				sk --ansi -i -c (eval echo 'ag -u --stats --color \"{}\" $argv[2]') --delimiter : --preview 'bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}' --preview-window +{2}-/2
				#	4.	$ search -t ag
			else if test (count $argv) = 3
				echo 1.1.3 argv1=$argv[1]
				sk --ansi -i -c 'ag -u --stats --color "{}"' --delimiter : --preview 'bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}' --preview-window +{2}-/2
			end
		else if test "$argv[1]" = "rg"
			#	9.	$ search -t rg $recherche $dossier 
			if test (count $argv) = 3
				sk --ansi -i -c (eval echo 'rg -. --stats --color=always --line-number $argv[2] $argv[3]') --delimiter : --preview 'bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}' --preview-window +{2}-/2
				#	8.	$ search -t rg $dossier
			else if test (count $argv) = 2
				sk --ansi -i -c (eval echo 'rg -. --stats --color=always --line-number \"{}\" $argv[2]') --delimiter : --preview 'bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}' --preview-window +{2}-/2
				#	7.	$ search -t rg
			else if test (count $argv) = 3
				sk --ansi -i -c 'rg -. --stats --color=always --line-number "{}"' --delimiter : --preview 'bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}' --preview-window +{2}-/2
				# sk --ansi -i -c 'rg --color=always --line-number "{}"' --delimiter : --preview 'bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}' --preview-window +{2}-/2
				# sk --ansi -i -c 'grep -rI --color=always --line-number "{}" .' --delimiter : --preview 'bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}' --preview-window +{2}-/2
			end
		else if test "$argv[1]" = "grep"
#	15.	$ search -t grep $recherche $dossier
			if test (count $argv) = 3
				sk --ansi -i -c (eval echo 'grep -rIn --color=always $argv[2] $argv[3]') --delimiter : --preview 'bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}' --preview-window +{2}-/2
#	14.	$ search -t grep $dossier
			else if test (count $argv) = 2
				sk --ansi -i -c (eval echo 'grep -rIn --color=always \"{}\" $argv[2]') --delimiter : --preview 'bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}' --preview-window +{2}-/2
#	13.	$ search -t grep
			else if test (count $argv) = 3
				sk --ansi -i -c 'grep -rIn --color=always "{}" . ' --delimiter : --preview 'bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}' --preview-window +{2}-/2
			end
		else 
			gettext -e "⚠ $scbroio\aVous n'avez pas entré d'arguments à l'option $scbi{-t|--tool}$scbroio !$sn⚠\n$scgi\aVoici commment fonctionne la commande…$sn\n"
			eval $print_help
		end
		return 1
	else if not set -q $cmd_args
		#	3.	$ search $recherche $dossier
		if test (count $argv) = 2
			sk --ansi -i -c (eval echo 'ag -u --stats --color $argv[1] $argv[2]') --delimiter : --preview 'bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}' --preview-window +{2}-/2
			#	2.	$ search $dossier
		else if test (count $argv) = 1
			sk --ansi -i -c (eval echo 'ag -u --stats --color "{}" $argv[1]') --delimiter : --preview 'bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}' --preview-window +{2}-/2
			#	1.	$ search
		else if test -z $cmd_args
			sk --ansi -i -c 'ag -u --stats --color "{}"' --delimiter : --preview 'bat --style header --style rule --style snip --style changes --style numbers --style header --color=always --highlight-line {2} {1}' --preview-window +{2}-/2
		end
		return 1
	end
end