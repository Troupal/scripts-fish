# Fonction servant de pense-bête permettant d'afficher une liste de certains utilitaires pratique à utiliser
#
#	NOTE-version ::
#		- [x] 0.3 :
#			- Ajout des sections sous forme d'un appel $eval à une grand 'gettext -e "xxx\n" gettext -e "xxx"...'
#			- Meilleure prise en charge de la colorisation
#		- [x] 0.4.0 :
#			- Ajout des sections Divers, Commentaires et Raccourcis
#		- [x] 0.4.1 :
#			- Modification de a forme des sections → Se sont maintenant des tableaux d'une dimension comprenant chaque entrée d'une même section
#			- Ajout d'une fonction analysant automatiquement les tableaux et retournant chaque entrée de celui-ci
#			- Ajout de différentes sections (comme Pense-bête et FMTUI)
#			- Modification de l'aide avec l'ajout de l'argument {-s|--section} avec ses différentes complétions
#		- 0.4.2 :
#			- Finalisation de l'ajout des outils en commentaires
#		- 0.5.0 :
#			- Création d'un fichier (YAML, JSON, XML ou TXT) dans ~/.config comprenant toutes les entrées du pense-bête actuelles
#		- 0.5.1 :
#			- Ajout des différentes fonctions d'ajout d'entrées dans le pense-bête via le fichier dans ~/.config
#		- 0.6.0 :
#			- Ajout d'un mécanisme permettant d'afficher automatiquement la description de l'utilitaire
#		- 0.7.0 :
#			- Ajout d'un mécanisme de sélection avec des chiffres  et les flèches (comme pour paru) lançant automatiquement l'utilitaire dans l'invité de commande
#		- 0.7.1 :
#			- Ajout de raccourcis claviers permettant d'afficher des informations supplémentaires (tel que le manuel ou l'aide)
#		- 1.0.0 → 💕 Version finale !!!! 💕
#		- 1.0.1 → Ajout de nouveaux outils et raccourcis
#
#	TODO ::
#		- [ ] Ajouter un mécanisme permettant d'afficher automatiquement la description de l'utilitaire
#		- [ ] Ajouter un mécanisme de sélection (comme pour paru par exemple) avec des chiffres et les flèches
#		- [ ] 50 % Ajouter des raccourcis claviers permettant d'afficher des informations supplémentaires (tel que le manuel ou l'aide) ainsi que d'insérer l'utilitaire sélection dans une commande
#		- [x] Ajouter des complétions, notamment pour les paramètres (ex: $ outils <TAB> → $ outils alternatives) :
#		- [x] Modifier l'appel aux descriptions en simplifiant leur affichage :
#			- [x] Modifier les gettext ultra longs en les mettant dans une variable sous forme de tableau
#			- [x] Ajouter une fonction qui fasse une boucle dans toutes les colonnes du tableau
#		- [ ] Ajouter une fonction qui compile les plus petites parties (par exemple, “Gestionnaires de fichiers TUI”)
#		- [ ] Ajouter les outils suivants :
#			- [x] Ranger
#			- [x] AG
#			- [x] Skim
#			- [ ] Teehee
#			- [ ] Xcp → Remplacement ou compagnon de cp écrit en 🦀 Rust 🦀
#			- [x] Helix → Alternative à Vim (https://helix-editor.com/)
#			- [ ] Viddy → Alternative à watch écrite en 🦀 Rust 🦀
#			- [ ] Hwatch → Alternative à watch écrite en 🦀 Rust 🦀
#			- [ ] Spacedrive → Gestionnaire de fichier écrit en 🦀 Rust 🦀 (https://spacedrive.com/)
#			- [x] xplr → Gestionnaire de fichier TUI écrit en 🦀 Rust 🦀 (https://xplr.dev/)
#			- [ ] rustdesk → Bureau à distance sécurité et prêt à fonctionné écrit en 🦀 Rust 🦀(https://rustdesk.com)
#			- [ ] RustPython → Interpréteur 🐍 Python 🐍écrit en 🦀 Rust 🦀 (https://github.com/RustPython/RustPython)
#			- [x] Vikunja → Outils d'organisation Kamban
#
#
#	Add ::
#			- [ ] Ajouter une fonction permettant d'ajouter automatiquement une entrée :
#				- [ ] Soit au pense-bête
#					- [ ] Sous la forme ::→ $ outils {-p|--add-to-pensebete|--add2pb} "$titre" "$texte"
#				- [ ] Soit aux sections
#					- [ ] Sous la forme ::→ $ outils {-a|--add-to-section|--add2sct} "$section" "$titre" "$texte"
#				- [ ] Soit une nouvelle sections
#					- [ ] Sous la forme ::→ $ outils {-n|--add-new-section} "$section-name"
#				- [ ] Soit une nouvelle section avec la ou les entrée⋅s à ajouter
#					- [ ] Sous la forme ::→ $ outils {-t|--add-new-array} "$section-name" {-e1|--entry1} "$titre" "$texte"  {-e2|--entry2} "$titre" "$texte"  {-eN{1..9}|--entryN{1..9}} "$titre" "$texte"
#				- [ ] Pour se faire, ajouter un fichier dans lequel la fonction va aller lire, écrire et mettre en forme et où l'utilisateur pourra écrire simplement
#				- [ ] Ajouter une fonction qui ajoute la nouvelle section à :
#					- [ ] L'aide ({-h|--help} dans ~/.config/fish/functions/outils.fish)
#					- [ ] L'autocomplétion (~/.config/fish/completions/outils.fish)
#
#	Aide ::
#			- [x] Ajouter les fonctions de sélections de sections “-s”
#
#	Alternatives ::
#			- [ ] Topgrade → Outils permettant de mettre un jour nimporte quel système avec n'importe quel gestionnaire de paquets
#			- [ ] Fend → Outils de calcule e de conversion (https://github.com/printfn/fend)
#			- [ ] HTTPie → Alternative moderne à wget ou curl
#			- [ ] Kalker → Outils de calcule et de conversion très poussé (https://github.com/PaddiM8/kalker)
# 			- [ ] Hexil → Alternative à Hexdump écrite en 🦀 Rust 🦀 (https://github.com/sharkdp/hexyl)
# 			- [ ] Sic → Alternative en ligne de commande à ImageMagic écrite en 🦀 Rust 🦀 (https://github.com/foresterre/sic)
# 			- [ ] Stew → Alternative en ligne de commande à Sic écrite en 🦀 Rust 🦀 (https://github.com/foresterre/stew)
# 			- [ ] Ytop → Alternative en ligne de commande à Top écrite en 🦀 Rust 🦀 (https://github.com/cjbassi/ytop)
# 			- [ ] Appflowy → Alternative à Notion écrite en 🦀 Rust 🦀 (https://appflowy.io/)
# 			- [ ] Lapce → Éditeur de texte/IDE alternative à NeoVim en 🦀 Rust 🦀 (https://lapce.dev/)
#
#	Raccourcis clavier utiles ::
#			- [x] “!!” → Ajoute au prompt la commande précédemment utilisée dans l'historique avec ses arguments
#			- [x] “!$” → Ajoute au prompt les arguments de la commande précédemment utilisée dans l'historique ou uniquement la commande si elle ne comprenait pas d'arguments
#			- [x] !$ → Ajoute au prompt la dernière commande de l'historique sans arguments ou uniquement la commande si elle était seule
#			- [x] <Ctrl>+<G> → Utilise l'utilitaire «navi» pour trouver la commande la plus approprier, la compléter &| préciser son utilisation
#			- [x] <Alt>+<v> → Ajoute au prompt la commande vim ou remplace cat/bat. Conserve le sudo s'il est présent
#			- [x] <Alt>+<V> → Ajoute au prompt la commande sudo vim
#			- [x] <Alt>+<C> → Ajoute au prompt la commande cat

function outils --argument-names cmd_args --description "Fonction servant de pense-bête listant certains utilitaires"
	set --local outils_version "1.0.1"
	set --local cmd outils
	set --local desc "Fonction servant de pense-bête permettant d'afficher une liste certains utilitaires pratiques à utiliser."
	## Help
	set --local print_help[1] "    $so\aDescription :$sn\n"
	set --local print_help[2] "    \t$desc\n"
	set --local print_help[3] "    $so\aUsage :$sn\n"
	set --local print_help[4] "\t$scbo\a$cmd $scbi{-h|--help}$sn\t$scro——→$sn Affiche cette aide\n"
	set --local print_help[5] "\t$scbo\a$cmd $scbi{-v|--version}$sn\t$scro——→$sn Affiche la version du script\n"
	set --local print_help[6] "\t$scbo\a$cmd $scbi{-s|--section}$sn\t$scro——→$sn Affiche une section particulière du pense-bête\n"
	set --local print_help[7] "\t$so\a⮩    Affiche uniquement la section suivante: \"alternatives\" ou \"système\" ou \"utilitaires\" ou \"divers\" ou \"raccourcis\" ou \"commentaires\" ::$sn\n"
	set --local print_help[8] "\t     $so\a⮡\t$scbo\a$cmd $scbi{-s|--section}$sn $scm{\"$scmo\aalternatives$scm\"|\"$scmo\asystème$scm\"|\"$scmo\autilitaires$scm\"|\"$scmo\adivers$scm\"|\"$scmo\araccourcis$scm\"|\"$scmo\acommentaires$scm\"}$sn\n"
	## Alternatives
	set --local alternatives[1] "    $so\aAlternatives :$sn\n"
	set --local alternatives[2] "    \t$scbo\achoose$sn\t\t$scro——→$sn Alternative human-friendly et rapide à $scbo\acut$sn et (parfois) $scbo\aawk$sn permettant de $sio\achoisir$sn des sections à partir de chaque ligne d'un fichier écrite en 🦀 $si\aRust$sn 🦀\n"
	set --local alternatives[3] "    \t$scbo\aeva$sn\t\t$scro——→$sn Alternative plus évoluée, colorisée avec une mémoire persistante à  $scbo\abc$sn  écrite en 🦀$si\aRust$sn 🦀\n"
	set --local alternatives[4] "    \t$scbo\azoxide$sn\t\t$scro——→$sn Alternative plus évoluée, colorisée et interractive à  $scbo\acd$sn  écrite en 🦀$si\aRust$sn 🦀\t\t\t\t\t    ($scmi\ahttps://github.com/ajeetdsouza/zoxide$sn)\n" # Voir https://github.com/ajeetdsouza/zoxide
	set --local alternatives[5] "    \t\033[5m$scbo\ahelix$sn\t\t$scro——→$sn Alternative dîte post-moderne à l'IDE avec LSP  $scbo\avim$sn  écrite en 🦀$si\aRust$sn 🦀\n"
	set --local alternatives[6] "    \t$scbo\adust$sn\t\t$scro——→$sn Alternative plus évoluée, colorisée et rapide à  $scbo\adu$sn  écrite en 🦀$si\aRust$sn 🦀\n"
	set --local alternatives[7] "    \t$scbo\aexa$sn\t\t$scro——→$sn Alternative plus évoluée, colorisée et rapide à  $scbo\als$sn  écrite en 🦀$si\aRust$sn 🦀\n" # Alternative : lfs en mode TUI
	set --local alternatives[8] "    \t$scbo\afd$sn\t\t$scro——→$sn Alternative plus évoluée, colorisée et rapide à $scbo\afind$sn écrite en 🦀$si\aRust$sn 🦀\n"
	set --local alternatives[9] "    \t$scbo\aprocs$sn\t\t$scro——→$sn Alternative plus évoluée, colorisée et rapide à  $scbo\aps$sn  écrite en 🦀$si\aRust$sn 🦀\t\t\t\t\t\t    ($scmi\ahttps://github.com/dalance/procs$sn)\n"
	set --local alternatives[10] "    \t$scbo\arg$scyo | $scbo\aripgrep\t$scro——→$sn Alternative plus évoluée, colorisée et rapide à $scbo\agrep$sn écrite en 🦀$si\aRust$sn 🦀\n"
	set --local alternatives[11] "    \t$scbo\arip$sn\t\t$scro——→$sn Alternative plus évoluée, colorisée et  sûre  à  $scbo\arm$sn  écrite en 🦀$si\aRust$sn 🦀\n"
	set --local alternatives[12] "    \t$scbo\adfrs$sn\t\t$scro——→$sn Alternative plus évoluée, colorisée et rapide à  $scbo\adf$sn  écrite en 🦀$si\aRust$sn 🦀\n" # Alternatives : gdu, dua, ncdu en mode TUI
	set --local alternatives[13] "    \t$scbo\aduf$sn\t\t$scro——→$sn Alternative plus évoluée, colorisée et rapide à  $scbo\adf$sn  écrite en 🦀$si\aRust$sn 🦀\n"
	set --local alternatives[14] "    \t$scbo\adfc$sn\t\t$scro——→$sn Alternative plus évoluée, colorisée et rapide à  $scbo\adf$sn  écrite en $scyo$sn  $si\aC$sn   $scyo$sn\n"
	set --local alternatives[15] "    \t$scbo\aviddy$sn\t\t$scro——→$sn Alternative plus évoluée, colorisée et interractive à $scbo\awatch$sn écrite en  $si\aGo$sn \t\t\t\t\t    ($scmi\ahttps://github.com/sachaos/viddy$sn)\n"
	## Informations sur le système
	set --local systeme[1] "    $so\aInformations sur le système :$sn\n"
	set --local systeme[2] "    \t$scbo\abtop$sn\t\t$scro——→$sn Alternative plus évoluée, colorisée et rapide à $scbo\ahtop$sn écrite en $scyo$sn  $si\aC$sn   $scyo$sn\n"
	set --local systeme[3] "    \t$scbo\abtm$sn\t\t$scro——→$sn Alternative plus évoluée, colorisée et rapide à $scbo\ahtop$sn écrite en $scyo🦀$sn$si\aRust$sn $scyo🦀$sn\t\t\t\t\t\t    ($scmi\ahttps://github.com/ClementTsang/bottom$sn)\n"
	set --local systeme[4] "    \t$scbo\anvtop$sn\t\t$scro——→$sn Affiche des informations sur l'utilisation du⋅es GPU⋅s\n"
	set --local systeme[5] "    \t$scbo\anirqtop$sn\t\t$scro——→$sn Affiche des informations sur les interruptions\n"
	set --local systeme[6] "    \t$scbo\awavemon$sn\t\t$scro——→$sn Analyse le réseau sans fil du système et affiche des informations\n"
	set --local systeme[7] "    \t$scbo\asystemd-cgls$sn\t$scro——→$sn Afficher récursivement le contenu du groupe de contrôle de $scbo\asystemd$sn\n"
	# 	set --local systeme[7] "    \t$scbo\asystemd-cgls$sn\t$scro——→$sn Afficher récursivement le contenu du groupe de contrôlede $scbo\asystemd$sn\n"
	set --local systeme[8] "   $scbio\asudo $scbo\apowertop$sn\t$scro——→$sn Outil de diagnostic de la consommation et de la gestion de l'énergie\n"
	set --local systeme[9] "    \t$scbo\abandwhich$sn\t$scro——→$sn Outil écrit en 🦀$si\aRust$sn 🦀 affichant des informations sur l'utilisation de la bande passante\t\t\t\t    ($scmi\ahttps://github.com/imsnif/bandwhich$sn)\n"
	## Divers utilitaires
	set --local utilitaires[1] "    $so\aDivers utilitaires :$sn\n"
	set --local utilitaires[2] "    \t$scbo\adelta$sn\t\t$scro——→$sn Pager écrit en 🦀$si\aRust$sn 🦀 permettant la mise en évidence de la syntaxe pour les sorties git et diff\n"
	set --local utilitaires[3] "    \t$scbo\arustscan$sn\t$scro——→$sn Outil écrit en 🦀$si\aRust$sn 🦀 permettant de scanner rapidement les ports ouvert et leur utilisation\t\t\t    ($scmi\ahttps://github.com/RustScan/RustScan$sn)\n"
	set --local utilitaires[4] "    \t$scbo\apipr$sn\t\t$scro——→$sn Outil écrit en 🦀$si\aRust$sn 🦀 permettant de construire de manière interactive et dans un environnement sécurisé des pipelines shell complexes\n"
	set --local utilitaires[5] "    \t$scbo\agrex$sn\t\t$scro——→$sn Outil écrit en 🦀$si\aRust$sn 🦀 permettant de générer des regex à partir de cas de test fournis par l'utilisateur\t\t    ($scmi\ahttps://github.com/pemistahl/grex$sn)\n"
	set --local utilitaires[6] "    \t$scbo\arnr$sn\t\t$scro——→$sn Outil écrit en 🦀$si\aRust$sn 🦀 permettant de renommer en toute sécurité plusieurs fichiers et répertoires et qui prend en charge les expressions régulières\n"
	set --local utilitaires[7] "    \t$scbo\atealdeer$sn\t$scro——→$sn Outil écrit en 🦀$si\aRust$sn 🦀 permettant l'affichage de manpages simplifiées, basées sur des exemples, alternative de $scbo\atldr$sn   ($scmi\ahttps://github.com/dbrgn/tealdeer$sn)\n"
	set --local utilitaires[8] "    \t$scbo\arnr$sn\t\t$scro——→$sn Outil écrit en 🦀$si\aRust$sn 🦀 permettant l'affichage de manpages simplifiées, basées sur des exemples et gérées par la communauté, alternative de $scbo\atldr$sn\n"
	set --local utilitaires[9] "    \t$scbo\anavi$sn\t\t$scro——→$sn Outil écrit en 🦀$si\aRust$sn 🦀 permettant l'affichage interactif d'aide-mémoire pour la ligne de commande basés sur des exemples\n"
	set --local utilitaires[10] "    \t$scbo\ateehee$sn\t\t$scro——→$sn Outil écrit en 🦀$si\aRust$sn 🦀 permettant l'édition de fichier hexadécimaux en ligne de commande\t\t\t\t    ($scmi\ahttps://github.com/Gskartwii/teehee$sn)\n"
	set --local utilitaires[11] "    \t$scbo\aranger$sn\t\t$scro——→$sn Gestionnaire de fichier TUI écrit en $sn$(set_color -o fbff08)$si Python$sn $(set_color -o fbff08)$sn alternative à $scbo\amc$sn et $scbo\axplr$sn permettant d'explorer les fichiers et d'en avoir un aperçu\n"
	set --local utilitaires[12] "    \t$scbo\axplr$sn\t\t$scro——→$sn Gestionnaire de fichier TUI écrit en 🦀$si\aRust$sn 🦀 alternative à $scbo\amc$sn et $scbo\aranger$sn\t\t\t\t\t\t    ($scmi\ahttps://xplr.dev/$sn)\n"
	set --local utilitaires[13] "    \t$scbo\abroot$sn\t\t$scro——→$sn Gestionnaire de fichier TUI écrit en 🦀$si\aRust$sn 🦀 alternative à $scbo\amc$sn et $scbo\aranger$sn avec aperçu des fichiers, images et binaires  ($scmi\ahttps://dystroy.org/broot/$sn)\n"
	set --local utilitaires[14] "    \t$scbo\avikunja$sn\t\t$scro——→$sn Outil de gestion des tâches et pense-bête en ligne via un serveur \t\t\t\t\t\t\t    ($scmi\ahttps://vikunja.io/$sn)\n"
	## Diver/sions → Partie de tests
	set --local diversion[1] 'gettext -e "    $so\aDivers :$sn\n'
	set --local diversion[2] 'gettext -e "\t$scbo\aQQC$sn\t\t$scro——→$sn QQC \n "'
	set --local diversion[3] 'gettext -e "\t$scbo\aQQC$sn\t\t$scro——→$sn QQC \n "'
	set --local diversion[4] 'gettext -e "\t$scbo\aQQC$sn\t\t$scro——→$sn QQC \n "'
	set --local diversion[5] 'gettext -e "\t$scbo\aQQC$sn\t\t$scro——→$sn QQC \n "'
	set --local diversion[6] 'gettext -e "\t$scbo\aQQC$sn\t\t$scro——→$sn QQC \n "'
	set --local diversion[7] 'gettext -e "\t$scbo\aQQC$sn\t\t$scro——→$sn QQC \n "'
	set --local diversion[8] 'gettext -e "\t$scbo\aQQC$sn\t\t$scro——→$sn QQC \n "'
	set --local diversion[9] 'gettext -e "\t$scbo\aQQC$sn\t\t$scro——→$sn QQC \n "'
	## Outils en commentaires (donc à tester ou explorer)
	set --local commentaires[1] "    $so\aCommentaires :$sn\n"
	set --local commentaires[2] "\t$scoo\afelix, joshuto, nnn$sn\t\t$scro——→$sn Divers explorateurs/gestionnaires de fichiers en TUI \n "
	set --local commentaires[3] "\t$scoo\annn$sn\t\t$scro——→$sn Gestionnaire/explorateur de fichier en TUI avec preview écrit en $scyo$sn  $si\aC$sn   $scyo$sn \t\t\t\t\t\t    ($scmi\ahttps://github.com/jarun/nnn$sn) \n "
	set --local commentaires[4] "\t$scco\alf$sn\t\t$scro——→$sn Gestionnaire/explorateur de fichier en TUI écrit en  $si\aGo$sn \t\t\t\t\t\t\t\t    ($scmi\ahttps://github.com/gokcehan/lf$sn) \n "
	## Divers outils
	set --local divers[1] "    $so\aDivers :$sn\n"
	## Raccourcis clavier
	set --local raccourcis[1] "    $so\aRaccourcis clavier :$sn\n"
	set --local raccourcis[2] "\t$scvo!! $sn\t\t$scro——→$sn Ajoute au prompt la commande précédemment utilisée dans l'historique avec ses arguments\n"
	set --local raccourcis[3] "\t$scvo!\$ $sn\t\t$scro——→$sn Ajoute au prompt les arguments de la commande précédemment utilisée dans l'historique ou uniquement la commande si elle ne comprenait pas d'arguments\n"
	set --local raccourcis[4] "\t$scvo\aCtrl $sn+$scvo G $sn\t$scro——→$sn Utilise l'utilitaire « navi » pour trouver la commande la plus approprier, la compléter &| préciser son utilisation\n"
	set --local raccourcis[5] "\t$scvo\aCtrl $sn+$scvo \ $sn\t$scro——→$sn Utilise la fonction $scbi\a__bypass_fish_alias$sn afin de bypasser les alias et utiliser les fonctions d'origine\n"
	set --local raccourcis[6] "\t$scvo\aAlt $sn+$scvo v $sn\t$scro——→$sn Utilise la fonction $scbi\a__vim_insert_binding$sn afin d'ajouter \"vim\" au début du prompt ou remplace \"(cat|bat)\" ou le passe après \"sudo\"\n"
	set --local raccourcis[7] "\t$scvo\aAlt $sn+$scvo V $sn\t$scro——→$sn Utilise la fonction $scbi\a__sudovim_insert_binding$sn afin d'ajouter \"sudo vim\" au début du prompt ou remplace \"(cat|bat)\"\n"
	set --local raccourcis[8] "\t$scvo\aAlt $sn+$scvo C $sn\t$scro——→$sn Utilise la fonction $scbi\a__cat_insert_binding$sn afin d'ajouter \"cat\" au début du prompt ou remplace \"vim\"\n"
	set --local raccourcis[9] "\t$scvo\aAlt $sn+$scvo I $sn\t$scro——→$sn Utilise la fonction $scbi\a__info_insert_binding$sn afin d'ajouter \"info\" au début du prompt si une info existe pour la commande\n"
	## Gestionnaires de fichiers TUI
	set --local fmtui[1] "    $so\aDivers explorateurs/gestionnaires de fichiers en TUI :$sn\n"
	set --local fmtui[2] "\t$scoo\annn$sn\t\t$scro——→$sn Gestionnaire/explorateur de fichier en TUI avec preview écrit en $scyo$sn  $si\aC$sn   $scyo$sn \t\t\t\t\t\t    ($scmi\ahttps://github.com/jarun/nnn$sn) \n "
	set --local fmtui[3] "\t$scco\alf$sn\t\t$scro——→$sn Gestionnaire/explorateur de fichier en TUI écrit en  $si\aGo$sn \t\t\t\t\t\t\t\t    ($scmi\ahttps://github.com/gokcehan/lf$sn) \n "
	set --local fmtui[4] "\t$scoo\afelix$sn\t\t$scro——→$sn Gestionnaire/explorateur de fichier en TUI avec preview \n "
	set --local fmtui[5] "\t$scco\ajoshuto$sn\t\t$scro——→$sn Gestionnaire/explorateur de fichier en TUI avec preview\n "
	set --local fmtui[6] "\t$scco\amc$sn\t\t$scro——→$sn Le  gestionnaire/explorateur de fichier en TUI $siou\aoriginal$sn !!!\n "
	set --local fmtui[7] "\t$scco\aranger$sn\t\t$scro——→$sn Gestionnaire/explorateur de fichier en TUI avec preview\n "
	## Pense-bête !
	set --local pensebete[1] "    $so\aPense-bête :$sn\n"
	set --local pensebete[2] "\t"
	set --local pensebete[3] "\t"
	set --local pensebete[4] "\t"
	set --local pensebete[5] "\t"
	set --local pensebete[6] "\t"
	set --local pensebete[7] "\t"
	set --local pensebete[8] "\t"
	set --local pensebete[9] "\t"

	argparse v/version h/help s/section -- $argv &>/dev/null

	if set -q _flag_help
		looptable $print_help
		return 1
	else if set -q _flag_version
		gettext -e "$so\aLa version de ce script est : $si$outils_version"
		return 1
	else if set -q _flag_section
		if test (count $argv) = 1
			if test "$argv[1]" = "alternatives"
				looptable $alternatives
				return 1
			else if test "$argv[1]" = "système"
				looptable $systeme
				return 1
			else if test "$argv[1]" = "utilitaires"
				looptable $utilitaires
				return 1
			else if test "$argv[1]" = "divers"
				looptable $divers
				return 1
			else if test "$argv[1]" = "raccourcis"
				looptable $raccourcis
				return 1
			else if test "$argv[1]" = "commentaires"
				looptable $commentaires
				return 1
			else if test "$argv[1]" = "fmtui"
				looptable $fmtui
				return 1
			end
		end
	else if test "$cmd_args" = ""
		looptable $alternatives; echo
		looptable $systeme; echo
		looptable $utilitaires; echo
		looptable $commentaires; echo
		looptable $raccourcis; echo
		looptable $fmtui; echo
	end
end

# Fonction permettant de faire une boucle sur l'ensemble des membres d'un tableau et d'en retourner le résultat
function looptable
	# Ajouter un contrôle du nombre d'arguments donné à la fonction looptable et une sortie sur stderr s'il dépasse 1, tout en comptant la taille du array :
	#	if test (count $argv) = 1
	#	else
	#		echo "Bad number of arguments !" >&2
	#		return 0
	#	end
	set -l argnbr (count $argv)
	if test $argnbr = 1
		for i in (seq 1 $argnbr)
			gettext -e $argv[$i]
		end
		gettext -e "$scoo\tLa section est vide et peut être à compléter !$sn"
	else
		for i in (seq 1 $argnbr)
			gettext -e $argv[$i]
		end
	end
end
