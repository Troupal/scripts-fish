# Fonction utilisant le bibliothèque Python stego-lsb et  permettant de cacher un document dans une image par stéganographie.
#
#	TODO ::
#		- [ ] Ajouter une option qui permette de d'abord chiffrer le fichier source avant d'effectuer la stéganographie
#		- [ ] Ajouter une option pour le nombre de LSB
#		- [ ] Ajouter le support i18n
#		- [ ] Supprimer les lignes commentées inutiles
#		- [x] Rendre globales certaines des variables raccourcies pour $(set_color)
#		- [x] Ajouter la completion des options
#		- [x] Reprendre les variables concernant la mise en forme et en couleur du texte
#

function stegimg --argument-names cmd_args --description "Cacher un document dans une image par stéganographie"
	set --local stegimg_version	2.0
	set --local cmd 			stegimg
	set --local desc			"Cacher un document dans une image par stéganographie via la librairie $si\apython$sn $sccio\"stego-lsb\"$sn\n"
	set --local plib			stego-lsb
	set --local pcmd			stegolsb
	set --local img				"image"
	set --local src				"fichier source"
	set --local out				"fichier de destination"
	set --local ptf				"chemin d'accès"
	set --local option

	# Parse CLI options
	while true
		switch "$cmd_args"
			case -v --version
				gettext -e "La version de ce script est : $stegimg_version"
				return 1
			case "" -h --help
				gettext -e "    $so\aDescription :$sn\n"
				gettext -e "    \t$desc\n"
				gettext -e "\tPour l'utiliser, veuillez d'abord $scgio\apréciser le type de manipulation à effectuer$sn, à savoir :\n"
				gettext -e "\t\t $scbi-t$sn ou$scbi --test$sn\t $scro——→$sn pour tester si$scb l'$img$sn est assez grande pour cacher le$scv $src$sn;\n"
#				gettext -e "\t\t $scbi-t$scij,$scbi --teste$scij ou$scoj --test$sn pour tester si$scb l'$img$sn est assez grande pour cacher le$scv $src$sn;\n"
				gettext -e "\t\t $scbi-c$sn ou$scbi --cache$sn\t $scro——→$sn pour effectuer l'opération précédente et cacher le$scv $src$sn;\n"
#				gettext -e "\t\t $scbi-c$scij,$scbi --cache$scij,$scoj -H$scij ou$scoj --hidden$sn pour effectuer l'opération précédente et cacher le$scv $src$sn;\n"
				gettext -e "\t\t $scbi-r$sn ou$scbi --recup$sn\t $scro——→$sn pour récupérer le $src caché dans une$scb $img$sn;\n"
#				gettext -e "\t\t $scbi-r$scij,$scbi --recup$scij ou$scoj --recover$sn pour récupérer le $src caché dans une$scb $img$sn;\n"
				gettext -e "\t\t $scbi-d$sn ou$scbi --detect$sn\t $scro——→$sn afin de tester un fichier et détecter s'il contient des données stéganographiques.\n"
				gettext -e "\tSuivi du $ptf vers le$scv fichier d'origine (l'$img)$sn suivie du$scv fichier à cacher ($src)$sn et enfin, du$scv $out$sn\n"
#				gettext -e "\tUne dernière option facultative permet de préciser le nombre de LSB concernés par la stéganographie.\n"

				gettext -e "    $so\aUsage :$sn\n"
				gettext -e "$suo\tPour afficher cette aide :\n$sn"
				gettext -e "    \t    $scro⤷$sn   $scbo\a$cmd $scbi{-h|--help}$sn\n"
				gettext -e "$suo\tPour afficher la version du script :\n$sn"
				gettext -e "    \t    $scro⤷$sn   $scbo\a$cmd $scbi{-v|--version}$sn\n"
				gettext -e "$suo\tPour tester simplement un fichier :\n$sn"
				gettext -e "    \t    $scro⤷$sn   $scbo\a$cmd $scbi{$scbi-t$scbi,$scbi--test$sbi}$sn\t$scoio\${$scouo$ptf vers l'$img$scoio}$sn $scoio\${$scouo$ptf vers le $src$scoio}$sn\n"
#				gettext -e "$scon\t\tstegimg $scij{$scoj-t$scij,$scoj--teste$scij,$scoj--test}$sn $scv\${$scviu$ptf vers l'$img$scv} \${$scviu$ptf vers le $src$scv}$sn\n"
				gettext -e "$suo\tPour cacher un fichier dans une $img :$sn\n"
				gettext -e "    \t    $scro⤷$sn   $scbo\a$cmd $scbi{$scbi-c$scbi,$scbi--cache$scbi}$sn\t$scoio\${$scouo$ptf vers l'$img$scoio}$sn $scoio\${$scouo$ptf vers le $src$scoio} $scoio\${$scouo$ptf vers le $out$scoio}$sn\n"
				gettext -e "    \t    $scro⤷$sn   $scbo\a$cmd $scbi{$scbi-H$scbi,$scbi--hidden$scbi}$sn\t$scoio\${$scouo$ptf vers l'$img$scoio}$sn $scoio\${$scouo$ptf vers le $src$scoio} $scoio\${$scouo$ptf vers le $out$scoio}$sn\n"
				#				gettext -e "$scon\t\tstegimg $scij{$scoj-c$scij,$scoj--cache$scij,$scoj-H$scij,$scoj--hidden$scij} $scv\${$scviu$ptf vers l'$img$scv} \${$scviu$ptf vers le $src$scv} \${$scviu$ptf vers le $out$scv}\n"
				gettext -e "$suo\tPour détecter la présence de données stéganographiques dans un fichier :$sn\n"
				gettext -e "    \t    $scro⤷$sn   $scbo\a$cmd $scbi{$scbi-d$scbi,$scbi--detect$scbi}$sn\t$scoio\${$scouo$ptf vers l'$img$scoio}$sn\n"
				gettext -e "$suo\tPour récupérer un $src caché dans une $img :$sn\n"
				gettext -e "    \t    $scro⤷$sn   $scbo\a$cmd $scbi{$scbi-r$scbi,$scbi--recup$scbi}$sn\t$scoio\${$scouo$ptf vers le $src$soi}$sn $scoio\${$scouo$ptf vers le $out$scoio}$sn\n"
				#				gettext -e "$scon\t\tstegimg $scij{$scoj-r$scij,$scoj--recup$scij,$scoj--recover$scij} $scv\${$scviu$ptf vers le $src$scv} \${$scviu$ptf vers le $out$scv}\n"
				return 1
			case -t --test
				set option 1
				break
			case -c --cache -H --hidden
				set option 2
				break
			case -r --recup -- recover
				set option 3
				break
			case -d --detect
				set option 4
				break
			case \*
				gettext -e "stegimg: $(set_color red)Option inconnue: \"$cmd_arg\"$(set_color -i red)\nVoir l'aide :$(set_color normal) $(set_color -i)stegimg --help$(set_color normal)" >&2 && return 1
		end
	end

	# Check if the stego-lsb python librairy and the command line interface to the X11 clipboard xclip exists, if not, install it
	if ! pip list | grep stego-lsb &> /dev/null
		echo La librairie python (set_color -ui)$plib(set_color normal) n\'a pas été trouvée, installation...
		pip install $plib
	end


#	while true
#		switch "$argv[1]"
#			case -t --teste --test
#				set --local option 1
#			case -c --cache -H --hidden
#				set --local option 2
#			case -r --recup -- recover
#				set --local option 3
#			case -d --detect
#				set --local option 4
#		end
#	end

#	if test $option = 1
#		stegolsb steglsb -a -i $argv[2] -s $argv[3] -n 2
#	else if test $option = 2
#		stegolsb steglsb -h -i $argv[2] -s $argv[3] -o $argv[4] -n 2 -c 1
#	else if test $option = 3
#		stegolsb steglsb -r -i $argv[2] -o $argv[2] -n 2
#	else if test $option = 4
#		stegolsb stegdetect -i $argv[2] -n 2
#	end

	if test "$option" = 1
		stegolsb steglsb -a -i $argv[2] -s $argv[3] -n 2
	else if test "$option" = 2
		stegolsb steglsb -h -i $argv[2] -s $argv[3] -o $argv[4] -n 2 -c 1
	else if test "$option" = 3
		stegolsb steglsb -r -i $argv[2] -o $argv[3] -n 2
	else if test "$option" = 4
		stegolsb stegdetect -i $argv[2] -n 2
	end

end
