## Set values
# Hide welcome message
set fish_greeting
set VIRTUAL_ENV_DISABLE_PROMPT "1"
set -x MANPAGER "bat -l man -p"
set -x PAGER "less -RF"
set -x EDITOR "/usr/bin/nvim"

## Export variable need for qt-theme
if type "qtile" >> /dev/null 2>&1
	set -x QT_QPA_PLATFORMTHEME "qt5ct"
end

# Set settings for https://github.com/franciscolourenco/done
set -U __done_min_cmd_duration 10000
set -U __done_notification_urgency_level low

## Environment setup
# Apply .profile: use this to put fish compatible .profile stuff in
if test -f ~/.fish_profile
	source ~/.fish_profile
end

# Apply user bindings
if test -f ~/.config/fish/bindings.fish
	source ~/.config/fish/bindings.fish
end

# Add ~/.local/bin to PATH
if test -d ~/.local/bin
	if not contains -- ~/.local/bin $PATH
		set -p PATH ~/.local/bin
	end
end

# Add depot_tools to PATH
if test -d ~/Applications/depot_tools
	if not contains -- ~/Applications/depot_tools $PATH
		set -p PATH ~/Applications/depot_tools
	end
end

# Apply .profile: use this to put fish compatible .profile stuff in
if test -f ~/.config/fish/aliases.fish
	source ~/.config/fish/aliases.fish
end

## Starship prompt
if status --is-interactive
	source (/usr/bin/starship init fish --print-full-init | psub)
	atuin init fish | source
end

## Advanced command-not-found hook
source /usr/share/doc/find-the-command/ftc.fish


## Functions

# Fish command history
function history
	builtin history --show-time='%F %T '
end

function backup --argument filename
	cp $filename $filename.bak
end

# Copy DIR1 DIR2
function copy
	set count (count $argv | tr -d \n)
	if test "$count" = 2; and test -d "$argv[1]"
	set from (echo $argv[1] | trim-right /)
	set to (echo $argv[2])
		command cp -r $from $to
	else
		command cp $argv
	end
end

# MkdirThenCd Create a new folder and go into it, or jump into a folder first.
function mkcd --description "Créer un nouveau dossier et aller dedans, ou bien sauter d'abord dans un dossier."
	set count (count $argv)
	if test $count = 1
		mkdir -p $argv[1] && cd $argv[1]
	else if test $count = 2
		if test -d "$argv[1]"
			cd $argv[1]
			mkdir -p $argv[2]
			cd $argv[2]
		else
			echo -e "$scoo\aLe dossier \"$(pwd)/$argv[1]\" n'existe pas !"
			return 0
		end
	else
		echo -e "\033[5m⚠\033[0m$scro Vous avez indiqué trop d'arguments (seulement $(dsou 1)$scro ou $(dsou 2)$scro sont requis) ! \033[5m⚠\033[0m"
		echo -e $so ⮡ (dsou $suo "\tUsage :")
		gettext -e "\t$scoo\a1 argument  ——→                \"$scbo\amkdir $scmo\$argv[$scro\a1$scmo] $scoo\a&& $scbo\acd $scmo\$argv[$scro\a1$scmo]$scoo\"\n\t2 arguments ——→ \"$scbo\acd $scmo\$argv[$scro\a1$scmo] $scoo\a&& $scbo\amkdir $scmo\$argv[$scro\a2$scmo\a] $scoo\a&& $scbo\acd $scmo\a\$argv[$scro\a2$scmo\a]$scoo\"\n"
		return 0
	end
end

set -x _ZO_ECHO '1'
set -x _ZO_RESOLVE_SYMLINKS  '1'
zoxide init fish | source

# Useful formating variables
#:: Définition des couleurs —————————————————————————————————————————————————————————————————————————————————————————————————————
set bl 			55BBFF											# Définition de la couleur bleu
set org			EA9253											# Définition de la couleur orange
set brorg		E28513											# Définition de la couleur orange foncé
set mgnt		7606B2											# Définition de la couleur magenta foncée
set blc			DDDDDD											# Définition de la couleur blanc
set vrt			8CFF5A											# Définition de la couleur vert
#:: Couleurs normales ———————————————————————————————————————————————————————————————————————————————————————————————————————————
set sn 			(set_color normal)								# Définir le formatage du texte en normal
set si 			(set_color normal)(set_color -i)				# Définir le formatage du texte en italique
set su 			(set_color normal)(set_color -u)				# Définir le formatage du texte en souligné
set so 			(set_color normal)(set_color -o)				# Définir le formatage du texte en gras
set sui			(set_color normal)(set_color -ui)				# Définir le formatage du texte en souligné italique
set suo			(set_color normal)(set_color -uo)				# Définir le formatage du texte en souligné gras
set sio			(set_color normal)(set_color -io)				# Définir le formatage du texte en italique gras
set siou		(set_color normal)(set_color -iou)				# Définir le formatage du texte en souligné italique gras
#:: Couleurs claires ————————————————————————————————————————————————————————————————————————————————————————————————————————————
# Noir #0C0A1A
set scnr		(set_color normal)(set_color black)				# Définir le formatage du texte en noir
set scnri 		(set_color normal)(set_color -i black)			# Définir le formatage du texte en noir italique
set scnru 		(set_color normal)(set_color -u black)			# Définir le formatage du texte en noir souligné
set scnro 		(set_color normal)(set_color -o black)			# Définir le formatage du texte en noir gras
set scnruo	 	(set_color normal)(set_color -uo black)			# Définir le formatage du texte en noir souligné gras
set scnriu	 	(set_color normal)(set_color -iu black)			# Définir le formatage du texte en noir italique souligné
set scnrio	 	(set_color normal)(set_color -io black)			# Définir le formatage du texte en noir italique gras
set scnriuo	 	(set_color normal)(set_color -iuo black)		# Définir le formatage du texte en noir italique souligné gras
# Bleu défini #55BBFF
set scb 		(set_color normal)(set_color $bl)				# Définir le formatage du texte en bleu
set scbi 		(set_color normal)(set_color -i $bl)			# Définir le formatage du texte en bleu italique
set scbu 		(set_color normal)(set_color -u $bl)			# Définir le formatage du texte en bleu souligné
set scbo 		(set_color normal)(set_color -o $bl)			# Définir le formatage du texte en bleu gras
set scbuo 		(set_color normal)(set_color -uo $bl)			# Définir le formatage du texte en bleu souligné gras
set scbiu 		(set_color normal)(set_color -iu $bl)			# Définir le formatage du texte en bleu italique souligné
set scbio 		(set_color normal)(set_color -io $bl)			# Définir le formatage du texte en bleu italique gras
set scbiuo 		(set_color normal)(set_color -iuo $bl)			# Définir le formatage du texte en bleu italique souligné gras
# Bleu #0E47F1
set scbb		(set_color normal)(set_color blue)				# Définir le formatage du texte en bleu
set scbbi		(set_color normal)(set_color -i blue)			# Définir le formatage du texte en bleu italique
set scbbu		(set_color normal)(set_color -u blue)			# Définir le formatage du texte en bleu souligné
set scbbo		(set_color normal)(set_color -o blue)			# Définir le formatage du texte en bleu gras
set scbbuo		(set_color normal)(set_color -uo blue)			# Définir le formatage du texte en bleu souligné gras
set scbbiu		(set_color normal)(set_color -iu blue)			# Définir le formatage du texte en bleu italique souligné
set scbbio		(set_color normal)(set_color -io blue)			# Définir le formatage du texte en bleu italique gras
set scbbiuo		(set_color normal)(set_color -iuo blue)			# Définir le formatage du texte en bleu italique souligné gras
# Cyan #34BEF9
set scc			(set_color normal)(set_color cyan)				# Définir le formatage du texte en cyan
set scci		(set_color normal)(set_color -i cyan)			# Définir le formatage du texte en cyan italique
set sccu		(set_color normal)(set_color -u cyan)			# Définir le formatage du texte en cyan souligné
set scco		(set_color normal)(set_color -o cyan)			# Définir le formatage du texte en cyan gras
set sccuo		(set_color normal)(set_color -uo cyan)			# Définir le formatage du texte en cyan souligné gras
set scciu		(set_color normal)(set_color -iu cyan)			# Définir le formatage du texte en cyan italique souligné
set sccio		(set_color normal)(set_color -io cyan)			# Définir le formatage du texte en cyan italique gras
set scciuo		(set_color normal)(set_color -iuo cyan)			# Définir le formatage du texte en cyan italique souligné gras
# Vert défini #8CFF5A
set scv			(set_color normal)(set_color $vrt)				# Définir la formatage du texte en vert
set scvi		(set_color normal)(set_color -i $vrt)			# Définir la formatage du texte en vert italique
set scvu		(set_color normal)(set_color -u $vrt)			# Définir la formatage du texte en vert souligné
set scvo		(set_color normal)(set_color -o $vrt)			# Définir la formatage du texte en vert gras
set scvuo		(set_color normal)(set_color -uo $vrt)			# Définir la formatage du texte en vert souligné gras
set scviu		(set_color normal)(set_color -iu $vrt)			# Définir la formatage du texte en vert italique souligné
set scvio		(set_color normal)(set_color -io $vrt)			# Définir la formatage du texte en vert italique gras
set scviuo		(set_color normal)(set_color -iuo $vrt)			# Définir la formatage du texte en vert italique souligné gras
# Vert #3EF934
set scg			(set_color normal)(set_color green) 			# Définir le formatage du texte en vert
set scgi 		(set_color normal)(set_color -i green) 			# Définir le formatage du texte en vert italique
set scgu 		(set_color normal)(set_color -u green) 			# Définir le formatage du texte en vert souligné
set scgo 		(set_color normal)(set_color -o green) 			# Définir le formatage du texte en vert gras
set scguo 		(set_color normal)(set_color -uo green) 		# Définir le formatage du texte en vert souligné gras
set scgio 		(set_color normal)(set_color -io green) 		# Définir le formatage du texte en vert italique gras
set scgiu 		(set_color normal)(set_color -iu green) 		# Définir le formatage du texte en vert italique souligné
set scgiuo 		(set_color normal)(set_color -iuo green)		# Définir le formatage du texte en vert italique souligné gras
# Magenta #E234F9
set scm			(set_color normal)(set_color magenta) 			# Définir le formatage du texte en magenta
set scmi 		(set_color normal)(set_color -i magenta) 		# Définir le formatage du texte en magenta italique
set scmu 		(set_color normal)(set_color -u magenta) 		# Définir le formatage du texte en magenta souligné
set scmo 		(set_color normal)(set_color -o magenta) 		# Définir le formatage du texte en magenta gras
set scmuo 		(set_color normal)(set_color -uo magenta) 		# Définir le formatage du texte en magenta souligné gras
set scmio 		(set_color normal)(set_color -io magenta) 		# Définir le formatage du texte en magenta italique gras
set scmiu 		(set_color normal)(set_color -iu magenta) 		# Définir le formatage du texte en magenta italique souligné
set scmiuo 		(set_color normal)(set_color -iuo magenta) 		# Définir le formatage du texte en magenta italique souligné gras
# Rouge #F95834
set scr 		(set_color normal)(set_color red)				# Définir le formatage du texte en rouge
set scri 		(set_color normal)(set_color -i red) 			# Définir le formatage du texte en rouge italique
set scru 		(set_color normal)(set_color -u red) 			# Définir le formatage du texte en rouge souligné
set scro 		(set_color normal)(set_color -o red) 			# Définir le formatage du texte en rouge gras
set scruo 		(set_color normal)(set_color -uo red) 			# Définir le formatage du texte en rouge souligné gras
set scrio 		(set_color normal)(set_color -io red) 			# Définir le formatage du texte en rouge italique gras
set scriu 		(set_color normal)(set_color -iu red) 			# Définir le formatage du texte en rouge italique souligné
set scriuo 		(set_color normal)(set_color -iuo red) 			# Définir le formatage du texte en rouge italique souligné gras
# Blanc défini #FFFFFF
set scw			(set_color normal)(set_color $blc)				# Définir le formatage du texte en blanc
set scwi 		(set_color normal)(set_color -i $blc) 			# Définir le formatage du texte en blanc italique
set scwu 		(set_color normal)(set_color -u $blc) 			# Définir le formatage du texte en blanc souligné
set scwo 		(set_color normal)(set_color -o $blc) 			# Définir le formatage du texte en blanc gras
set scwuo 		(set_color normal)(set_color -uo $blc) 			# Définir le formatage du texte en blanc souligné gras
set scwiu 		(set_color normal)(set_color -iu $blc) 			# Définir le formatage du texte en blanc italique souligné
set scwio 		(set_color normal)(set_color -io $blc) 			# Définir le formatage du texte en blanc italique gras
set scwiuo 		(set_color normal)(set_color -iuo $blc) 		# Définir le formatage du texte en blanc italique souligné gras
# Blanc #FFFFFF
set scww		(set_color normal)(set_color white)				# Définir le formatage du texte en blanc
set scwwi 		(set_color normal)(set_color -i white)			# Définir le formatage du texte en blanc italique
set scwwu 		(set_color normal)(set_color -u white)			# Définir le formatage du texte en blanc souligné
set scwwo 		(set_color normal)(set_color -o white)			# Définir le formatage du texte en blanc gras
set scwwuo 		(set_color normal)(set_color -uo white)			# Définir le formatage du texte en blanc souligné gras
set scwwiu 		(set_color normal)(set_color -iu white)			# Définir le formatage du texte en blanc italique souligné
set scwwio 		(set_color normal)(set_color -io white)			# Définir le formatage du texte en blanc italique gras
set scwwiuo 	(set_color normal)(set_color -iuo white)		# Définir le formatage du texte en blanc italique souligné gras
# Jaune #E7CD4B
set scy			(set_color normal)(set_color yellow) 			# Définir le formatage du texte en jaune
set scyi 		(set_color normal)(set_color -i yellow) 		# Définir le formatage du texte en jaune italique
set scyu 		(set_color normal)(set_color -u yellow) 		# Définir le formatage du texte en jaune souligné
set scyo 		(set_color normal)(set_color -o yellow) 		# Définir le formatage du texte en jaune gras
set scyuo 		(set_color normal)(set_color -uo yellow) 		# Définir le formatage du texte en jaune souligné gras
set scyiu 		(set_color normal)(set_color -iu yellow) 		# Définir le formatage du texte en jaune italique souligné
set scyio 		(set_color normal)(set_color -io yellow) 		# Définir le formatage du texte en jaune italique gras
set scyiuo 		(set_color normal)(set_color -iuo yellow) 		# Définir le formatage du texte en jaune italique souligné gras
# Orange #EA9253
set sco			(set_color normal)(set_color $org)				# Définir le formatage du texte en orange
set scoi		(set_color normal)(set_color -i $org)			# Définir le formatage du texte en orange italique
set scou		(set_color normal)(set_color -u $org)			# Définir le formatage du texte en orange souligné
set scoo		(set_color normal)(set_color -o $org)			# Définir le formatage du texte en orange gras
set scouo		(set_color normal)(set_color -uo $org)			# Définir le formatage du texte en orange souligné gras
set scoiu		(set_color normal)(set_color -iu $org)			# Définir le formatage du texte en orange italique souligné
set scoio		(set_color normal)(set_color -io $org)			# Définir le formatage du texte en orange italique gras
set scoiuo		(set_color normal)(set_color -iuo $org)			# Définir le formatage du texte en orange italique souligné gras
#:: Couleurs foncées ————————————————————————————————————————————————————————————————————————————————————————————————————————————
# Noir #000000
set scbrnr		(set_color normal)(set_color brblack)			# Définir le formatage du texte en noir foncé
set scbrnri		(set_color normal)(set_color -i brblack)		# Définir le formatage du texte en noir foncé italique
set scbrnru		(set_color normal)(set_color -u brblack)		# Définir le formatage du texte en noir foncé souligné
set scbrnro		(set_color normal)(set_color -o brblack)		# Définir le formatage du texte en noir foncé gras
set scbrnruo	(set_color normal)(set_color -uo brblack)		# Définir le formatage du texte en noir foncé souligné gras
set scbrnriu	(set_color normal)(set_color -iu brblack)		# Définir le formatage du texte en noir foncé italique souligné
set scbrnrio	(set_color normal)(set_color -io brblack)		# Définir le formatage du texte en noir foncé italique gras
set scbrnriuo	(set_color normal)(set_color -iuo brblack)		# Définir le formatage du texte en noir foncé italique souligné gras
# Bleu foncé #1B43BB
set scbrb		(set_color normal)(set_color brblue)			# Définir le formatage du texte en bleu foncé
set scbrbi		(set_color normal)(set_color -i brblue)			# Définir le formatage du texte en bleu foncé italique
set scbrbu		(set_color normal)(set_color -u brblue)			# Définir le formatage du texte en bleu foncé souligné
set scbrbo		(set_color normal)(set_color -o brblue)			# Définir le formatage du texte en bleu foncé gras
set scbrbuo		(set_color normal)(set_color -uo brblue)		# Définir le formatage du texte en bleu foncé souligné gras
set scbrbiu		(set_color normal)(set_color -iu brblue)		# Définir le formatage du texte en bleu foncé italique souligné
set scbrbio		(set_color normal)(set_color -io brblue)		# Définir le formatage du texte en bleu foncé italique gras
set scbrbiuo	(set_color normal)(set_color -iuo brblue)		# Définir le formatage du texte en bleu foncé italique souligné gras
# Cyan foncé #2E7D99
set scbrc		(set_color normal)(set_color brcyan)			# Définir le formatage du texte en cyan foncé
set scbrci		(set_color normal)(set_color -i brcyan)			# Définir le formatage du texte en cyan foncé italique
set scbrcu		(set_color normal)(set_color -u brcyan)			# Définir le formatage du texte en cyan foncé souligné
set scbrco		(set_color normal)(set_color -o brcyan)			# Définir le formatage du texte en cyan foncé gras
set scbrcuo		(set_color normal)(set_color -uo brcyan)		# Définir le formatage du texte en cyan foncé souligné gras
set scbrciu		(set_color normal)(set_color -iu brcyan)		# Définir le formatage du texte en cyan foncé italique souligné
set scbrcio		(set_color normal)(set_color -io brcyan)		# Définir le formatage du texte en cyan foncé italique gras
set scbrciuo	(set_color normal)(set_color -iuo brcyan)		# Définir le formatage du texte en cyan foncé italique souligné gras
# Vert foncé #358246
set scbrv		(set_color normal)(set_color brgreen)			# Définir le formatage du texte en vert foncé
set scbrvi		(set_color normal)(set_color -i brgreen)		# Définir le formatage du texte en vert foncé italique
set scbrvu		(set_color normal)(set_color -u brgreen)		# Définir le formatage du texte en vert foncé souligné
set scbrvo		(set_color normal)(set_color -o brgreen)		# Définir le formatage du texte en vert foncé gras
set scbrvuo		(set_color normal)(set_color -uo brgreen)		# Définir le formatage du texte en vert foncé souligné gras
set scbrviu		(set_color normal)(set_color -iu brgreen)		# Définir le formatage du texte en vert foncé italique souligné
set scbrvio		(set_color normal)(set_color -io brgreen)		# Définir le formatage du texte en vert foncé italique gras
set scbrviuo	(set_color normal)(set_color -iuo brgreen)		# Définir le formatage du texte en vert foncé italique souligné gras
# Magenta foncé #763989
set scbrm		(set_color normal)(set_color brmagenta)			# Définir le formatage du texte en magenta foncé
set scbrmi		(set_color normal)(set_color -i brmagenta)		# Définir le formatage du texte en magenta foncé italique
set scbrmu		(set_color normal)(set_color -u brmagenta)		# Définir le formatage du texte en magenta foncé souligné
set scbrmo		(set_color normal)(set_color -o brmagenta)		# Définir le formatage du texte en magenta foncé gras
set scbrmuo		(set_color normal)(set_color -uo brmagenta)		# Définir le formatage du texte en magenta foncé souligné gras
set scbrmiu		(set_color normal)(set_color -iu brmagenta)		# Définir le formatage du texte en magenta foncé italique souligné
set scbrmio		(set_color normal)(set_color -io brmagenta)		# Définir le formatage du texte en magenta foncé italique gras
set scbrmiuo	(set_color normal)(set_color -iuo brmagenta)	# Définir le formatage du texte en magenta foncé italique souligné gras
# Magenta foncé défini #7606B2
set scbrmm		(set_color normal)(set_color $mgnt)				# Définir le formatage du texte en magenta foncé défini
set scbrmmi 	(set_color normal)(set_color -i $mgnt)			# Définir le formatage du texte en magenta foncé défini italique
set scbrmmu 	(set_color normal)(set_color -u $mgnt)			# Définir le formatage du texte en magenta foncé défini souligné
set scbrmmo 	(set_color normal)(set_color -o $mgnt)			# Définir le formatage du texte en magenta foncé défini gras
set scbrmmuo 	(set_color normal)(set_color -uo $mgnt)			# Définir le formatage du texte en magenta foncé défini souligné gras
set scbrmmiu 	(set_color normal)(set_color -iu $mgnt)			# Définir le formatage du texte en magenta foncé défini italique souligné
set scbrmmio 	(set_color normal)(set_color -io $mgnt)			# Définir le formatage du texte en magenta foncé défini italique gras
set scbrmmiuo 	(set_color normal)(set_color -iuo $mgnt)		# Définir le formatage du texte en magenta foncé défini italique souligné gras
# Rouge foncé #91273A
set scbrr		(set_color normal)(set_color brred)				# Définir le formatage du texte en rouge foncé
set scbrri 		(set_color normal)(set_color -i brred)			# Définir le formatage du texte en rouge foncé italique
set scbrru 		(set_color normal)(set_color -u brred)			# Définir le formatage du texte en rouge foncé souligné
set scbrro 		(set_color normal)(set_color -o brred)			# Définir le formatage du texte en rouge foncé gras
set scbrruo 	(set_color normal)(set_color -uo brred)			# Définir le formatage du texte en rouge foncé souligné gras
set scbrriu 	(set_color normal)(set_color -iu brred)			# Définir le formatage du texte en rouge foncé italique souligné
set scbrrio 	(set_color normal)(set_color -io brred)			# Définir le formatage du texte en rouge foncé italique gras
set scbrriuo 	(set_color normal)(set_color -iuo brred)		# Définir le formatage du texte en rouge foncé italique souligné gras
# Blanc foncé #A1A1A1
set scbrw		(set_color normal)(set_color brwhite)			# Définir le formatage du texte en blanc foncé
set scbrwi 		(set_color normal)(set_color -i brwhite)		# Définir le formatage du texte en blanc foncé italique
set scbrwu 		(set_color normal)(set_color -u brwhite)		# Définir le formatage du texte en blanc foncé souligné
set scbrwo 		(set_color normal)(set_color -o brwhite)		# Définir le formatage du texte en blanc foncé gras
set scbrwuo 	(set_color normal)(set_color -uo brwhite)		# Définir le formatage du texte en blanc foncé souligné gras
set scbrwiu 	(set_color normal)(set_color -iu brwhite)		# Définir le formatage du texte en blanc foncé italique souligné
set scbrwio 	(set_color normal)(set_color -io brwhite)		# Définir le formatage du texte en blanc foncé italique gras
set scbrwiuo 	(set_color normal)(set_color -iuo brwhite)		# Définir le formatage du texte en blanc foncé italique souligné gras
# Jaune #B29506
set scbry		(set_color normal)(set_color bryellow)			# Définir le formatage du texte en jaune foncé
set scbryi 		(set_color normal)(set_color -i bryellow)		# Définir le formatage du texte en jaune foncé italique
set scbryu 		(set_color normal)(set_color -u bryellow)		# Définir le formatage du texte en jaune foncé souligné
set scbryo 		(set_color normal)(set_color -o bryellow)		# Définir le formatage du texte en jaune foncé gras
set scbryuo 	(set_color normal)(set_color -uo bryellow)		# Définir le formatage du texte en jaune foncé souligné gras
set scbryiu 	(set_color normal)(set_color -iu bryellow)		# Définir le formatage du texte en jaune foncé italique souligné
set scbryio 	(set_color normal)(set_color -io bryellow)		# Définir le formatage du texte en jaune foncé italique gras
set scbryiuo 	(set_color normal)(set_color -iuo bryellow)		# Définir le formatage du texte en jaune foncé italique souligné gras
# Orange #E28513
set scbro		(set_color normal)(set_color $brorg)			# Définir le formatage du texte en orange foncé
set scbroi 		(set_color normal)(set_color -i $brorg)			# Définir le formatage du texte en orange foncé italique
set scbrou 		(set_color normal)(set_color -u $brorg)			# Définir le formatage du texte en orange foncé souligné
set scbroo 		(set_color normal)(set_color -o $brorg)			# Définir le formatage du texte en orange foncé gras
set scbrouo 	(set_color normal)(set_color -uo $brorg)		# Définir le formatage du texte en orange foncé souligné gras
set scbroiu 	(set_color normal)(set_color -iu $brorg)		# Définir le formatage du texte en orange foncé italique souligné
set scbroio 	(set_color normal)(set_color -io $brorg)		# Définir le formatage du texte en orange foncé italique gras
set scbroiuo 	(set_color normal)(set_color -iuo $brorg)		# Définir le formatage du texte en orange foncé italique souligné gras

function clig --description "Fonction permettant de faire clignoter un texte"
	set -l d "\033[5m"
	set -l f "\033[0m"
	set -l a1 $argv[1]
	set -l a2 $argv[2]
	if test (count $argv) = 1
		echo -e "$d$a1$f"
	else
		echo -e "$a1$d$a2$f"
	end
end

function sur --description "Fonction permettant de surligner un texte"
	set -l d "\033[53m"
	set -l f "\033[0m"
	set -l a1 $argv[1]
	set -l a2 $argv[2]
	if test (count $argv) = 1
		echo -e "$d$a1$f"
	else
		echo -e "$a1$d$a2$f"
	end
end

function dsou --description "Fonction permettant de doublement souligner un texte"
	set -l d "\033[21m"
	set -l f "\033[0m"
	set -l a1 $argv[1]
	set -l a2 $argv[2]
	if test (count $argv) = 1
		echo -e "$d$a1$f"
	else
		echo -e "$a1$d$a2$f"
	end
end

function rat --description "Fonction permettant de raturer un texte"
	set -l d "\033[9m"
	set -l f "\033[0m"
	set -l a1 $argv[1]
	set -l a2 $argv[2]
	if test (count $argv) = 1
		echo -e "$d$a1$f"
	else
		echo -e "$a1$d$a2$f"
	end
end

## Run fastfetch if session is interactive
if status --is-interactive && type -q fastfetch
	fastfetch --load-config dr460nized_new.jsonc
end

string match -q "$TERM_PROGRAM" "vscode"
and . (code --locate-shell-integration-path fish)

# opam configuration
source /home/troupal/.opam/opam-init/init.fish > /dev/null 2> /dev/null; or true

# Created by `pipx` on 2024-06-04 10:46:57
set PATH $PATH /home/troupal/.local/bin

set PYTHONPATH $PYTHONPATH /home/troupal/.local/share/pipx/venvs/pyvisa/lib
