## Set values
# Hide welcome message
set fish_greeting
set VIRTUAL_ENV_DISABLE_PROMPT "1"
set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

## Export variable need for qt-theme
if type "qtile" >> /dev/null 2>&1
   set -x QT_QPA_PLATFORMTHEME "qt5ct"
end

# Set settings for https://github.com/franciscolourenco/done
set -U __done_min_cmd_duration 10000
set -U __done_notification_urgency_level low


## Environment setup
# Apply .profile: use this to put fish compatible .profile stuff in
if test -f ~/.fish_profile
  source ~/.fish_profile
end

# Add ~/.local/bin to PATH
if test -d ~/.local/bin
	if not contains -- ~/.local/bin $PATH
		set -p PATH ~/.local/bin
	end
end

# Add depot_tools to PATH
if test -d ~/Applications/depot_tools
	if not contains -- ~/Applications/depot_tools $PATH
		set -p PATH ~/Applications/depot_tools
	end
end

# Apply .profile: use this to put fish compatible .profile stuff in
if test -f ~/.config/fish/aliases.fish
  source ~/.config/fish/aliases.fish
end

## Starship prompt
if status --is-interactive
   source ("/usr/bin/starship" init fish --print-full-init | psub)
end

## Advanced command-not-found hook
source /usr/share/doc/find-the-command/ftc.fish


## Functions
# Functions needed for !! and !$ https://github.com/oh-my-fish/plugin-bang-bang
function __history_previous_command
  switch (commandline -t)
  case "!"
	commandline -t $history[1]; commandline -f repaint
  case "*"
	commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
	commandline -t ""
	commandline -f history-token-search-backward
  case "*"
	commandline -i '$'
  end
end

if [ "$fish_key_bindings" = fish_vi_key_bindings ];
  bind -Minsert ! __history_previous_command
  bind -Minsert '$' __history_previous_command_arguments
else
  bind ! __history_previous_command
  bind '$' __history_previous_command_arguments
end


# Function necessary to bind \ char and bypass fish aliases
#function __bypass_fish_alias
#		set -g cmdtoexec (commandline -b)
# 		commandline -r \\$cmdtoexec
#	function (eval echo \\$cmdtoexec)
#		set -l arg1 (echo $cmdtoexec | cut -d' ' -f2-5)
#		set -l arg0 (echo $cmdtoexec | awk '{print $1}')
#		set -l arg2 $(eval alias | grep "^alias $arg0" -P | cut -d' ' -f3-)
#		echo $arg0
#		echo $arg1
#		echo $arg2
#		echo "L'alias \"$arg0\" de la commande entrée \"$cmdtoexec\" a été bypassée :"
#		echo "Cette alias correspondait, à l'origine à \"$arg2\"."
#		set -l argv0 "command $cmdtoexec"
#		eval $argv0
#		set -e cmdtoexec
#	end
#	commandline -f execute
#end

#if [ "$fish_key_bindings" = fish_vi_key_bindings ];
#	bind -Minsert \\ __bypass_fish_alias
#	#	bind -Minsert '$' __history_previous_command_arguments
#else
#	bind \\ __bypass_fish_alias
#	#	bind '$' __history_previous_command_arguments
#end

# Fish command history
function history
	builtin history --show-time='%F %T '
end

function backup --argument filename
	cp $filename $filename.bak
end

# Copy DIR1 DIR2
function copy
	set count (count $argv | tr -d \n)
	if test "$count" = 2; and test -d "$argv[1]"
	set from (echo $argv[1] | trim-right /)
	set to (echo $argv[2])
		command cp -r $from $to
	else
		command cp $argv
	end
end

# Useful formating variables
#:: Définition des couleurs —————————————————————————————————————————————————————————————————————————————————————————————————————
set bl 			55BBFF											# Définition de la couleur bleu
set org			EA9253											# Définition de la couleur orange
set brorg		E28513											# Définition de la couleur orange foncé
set mgnt		7606B2											# Définition de la couleur magenta foncée
#:: Couleurs normales ———————————————————————————————————————————————————————————————————————————————————————————————————————————
set sn 			(set_color normal) 								# Définir le formatage du texte en normal
set su 			(set_color normal)(set_color -u) 				# Définir le formatage du texte en souligné
set si 			(set_color normal)(set_color -i) 				# Définir le formatage du texte en italique
set so 			(set_color normal)(set_color -o) 				# Définir le formatage du texte en gras
#:: Couleurs claires ————————————————————————————————————————————————————————————————————————————————————————————————————————————
# Noir #0C0A1A
set scnr		(set_color normal)(set_color black) 			# Définir le formatage du texte en noir
set scnri 		(set_color normal)(set_color -i black) 			# Définir le formatage du texte en noir italique
set scnru 		(set_color normal)(set_color -u black) 			# Définir le formatage du texte en noir souligné
set scnro 		(set_color normal)(set_color -o black) 			# Définir le formatage du texte en noir gras
set scnrio 	(set_color normal)(set_color -io black)	 			# Définir le formatage du texte en noir italique gras
# Bleu défini #55BBFF
set scb 		(set_color normal)(set_color $bl) 				# Définir le formatage du texte en bleu
set scbi 		(set_color normal)(set_color -i $bl) 			# Définir le formatage du texte en bleu italique
set scbu 		(set_color normal)(set_color -u $bl) 			# Définir le formatage du texte en bleu souligné
set scbo 		(set_color normal)(set_color -o $bl) 			# Définir le formatage du texte en bleu gras
# Bleu #0E47F1
set scbb		(set_color normal)(set_color blue) 					# Définir le formatage du texte en bleu
set scbbi 		(set_color normal)(set_color -i blue) 			# Définir le formatage du texte en bleu italique
set scbbu 		(set_color normal)(set_color -u blue) 			# Définir le formatage du texte en bleu souligné
set scbbo 		(set_color normal)(set_color -o blue) 			# Définir le formatage du texte en bleu gras
set scbbio 	(set_color normal)(set_color -io blue)		 		# Définir le formatage du texte en bleu italique gras
# Cyan #34BEF9
set scc			(set_color normal)(set_color cyan) 				# Définir le formatage du texte en cyan
set scci 		(set_color normal)(set_color -i cyan) 			# Définir le formatage du texte en cyan italique
set sccu 		(set_color normal)(set_color -u cyan) 			# Définir le formatage du texte en cyan souligné
set scco 		(set_color normal)(set_color -o cyan) 			# Définir le formatage du texte en cyan gras
set sccio 		(set_color normal)(set_color -io cyan) 			# Définir le formatage du texte en cyan italique gras
# Vert #3EF934
set scg			(set_color normal)(set_color green) 			# Définir le formatage du texte en vert
set scgi 		(set_color normal)(set_color -i green) 			# Définir le formatage du texte en vert italique
set scgu 		(set_color normal)(set_color -u green) 			# Définir le formatage du texte en vert souligné
set scgo 		(set_color normal)(set_color -o green) 			# Définir le formatage du texte en vert gras
set scgio 		(set_color normal)(set_color -io green) 		# Définir le formatage du texte en vert italique gras
# Magenta #E234F9
set scm			(set_color normal)(set_color magenta) 			# Définir le formatage du texte en magenta
set scmi 		(set_color normal)(set_color -i magenta) 		# Définir le formatage du texte en magenta italique
set scmu 		(set_color normal)(set_color -u magenta) 		# Définir le formatage du texte en magenta souligné
set scmo 		(set_color normal)(set_color -o magenta) 		# Définir le formatage du texte en magenta gras
set scmio 		(set_color normal)(set_color -io magenta) 		# Définir le formatage du texte en magenta italique gras
# Rouge #F95834
set scr 		(set_color normal)(set_color red) 				# Définir le formatage du texte en rouge
set scri 		(set_color normal)(set_color -i red) 			# Définir le formatage du texte en rouge italique
set scru 		(set_color normal)(set_color -u red) 			# Définir le formatage du texte en rouge souligné
set scro 		(set_color normal)(set_color -o red) 			# Déifnir le formatage du texte en rouge gras
set scrub 		(set_color normal)(set_color -uo red) 			# Définir le formatage du texte en rouge souligné gras
set scrib 		(set_color normal)(set_color -io red) 			# Définir le formatage du texte en rouge italique gras
# Blanc #FFFFFF
set scw			(set_color normal)(set_color white)				# Définir le formatage du texte en blanc
set scwi 		(set_color normal)(set_color -i white) 			# Définir le formatage du texte en blanc italique
set scwu 		(set_color normal)(set_color -u white) 			# Définir le formatage du texte en blanc souligné
set scwo 		(set_color normal)(set_color -o white) 			# Définir le formatage du texte en blanc gras
set scwio 		(set_color normal)(set_color -io white) 		# Définir le formatage du texte en blanc italique gras
# Jaune #E7CD4B
set scy			(set_color normal)(set_color yellow) 			# Définir le formatage du texte en jaune
set scyi 		(set_color normal)(set_color -i yellow) 		# Définir le formatage du texte en jaune italique
set scyu 		(set_color normal)(set_color -u yellow) 		# Définir le formatage du texte en jaune souligné
set scyo 		(set_color normal)(set_color -o yellow) 		# Définir le formatage du texte en jaune gras
set scyio 		(set_color normal)(set_color -io yellow) 		# Définir le formatage du texte en jaune italique gras
# Orange #EA9253
set sco			(set_color normal)(set_color $org) 				# Définir le formatage du texte en orange
set scoi 		(set_color normal)(set_color -i $org) 			# Définir le formatage du texte en orange italique
set scou 		(set_color normal)(set_color -u $org) 			# Définir le formatage du texte en orange souligné
set scoo 		(set_color normal)(set_color -o $org) 			# Définir le formatage du texte en orange gras
set scoio 		(set_color normal)(set_color -io $org) 			# Définir le formatage du texte en orange italique gras
#:: Couleurs foncées ————————————————————————————————————————————————————————————————————————————————————————————————————————————
# Noir #000000
set scbrnr		(set_color normal)(set_color brblack) 			# Définir le formatage du texte en noir foncé
set scbrnri 	(set_color normal)(set_color -i brblack) 		# Définir le formatage du texte en noir foncé italique
set scbrnru 	(set_color normal)(set_color -u brblack) 		# Définir le formatage du texte en noir foncé souligné
set scbrnro 	(set_color normal)(set_color -o brblack) 		# Définir le formatage du texte en noir foncé gras
set scbrnrio 	(set_color normal)(set_color -io brblack)		# Définir le formatage du texte en noir foncé italique gras
# Bleu foncé #1B43BB
set scbrb		(set_color normal)(set_color brblue) 			# Définir le formatage du texte en bleu foncé
set scbrbi 		(set_color normal)(set_color -i brblue) 		# Définir le formatage du texte en bleu foncé italique
set scbrbu 		(set_color normal)(set_color -u brblue) 		# Définir le formatage du texte en bleu foncé souligné
set scbrbo 		(set_color normal)(set_color -o brblue) 		# Définir le formatage du texte en bleu foncé gras
set scbrbio 	(set_color normal)(set_color -io brblue)	 	# Définir le formatage du texte en bleu foncé italique gras
# Cyan foncé #2E7D99
set scbrc		(set_color normal)(set_color brcyan) 			# Définir le formatage du texte en cyan foncé
set scbrci 		(set_color normal)(set_color -i brcyan) 		# Définir le formatage du texte en cyan foncé italique
set scbrcu 		(set_color normal)(set_color -u brcyan) 		# Définir le formatage du texte en cyan foncé souligné
set scbrco 		(set_color normal)(set_color -o brcyan) 		# Définir le formatage du texte en cyan foncé gras
set scbrcio 	(set_color normal)(set_color -io brcyan) 		# Définir le formatage du texte en cyan foncé italique gras
# Vert foncé #358246
set scbrv		(set_color normal)(set_color brgreen) 			# Définir le formatage du texte en vert foncé
set scbrvi 		(set_color normal)(set_color -i brgreen) 		# Définir le formatage du texte en vert foncé italique
set scbrvu 		(set_color normal)(set_color -u brgreen) 		# Définir le formatage du texte en vert foncé souligné
set scbrvo 		(set_color normal)(set_color -o brgreen) 		# Définir le formatage du texte en vert foncé gras
set scbrvio 	(set_color normal)(set_color -io brgreen) 		# Définir le formatage du texte en vert foncé italique gras
# Magenta foncé #763989
set scbrm		(set_color normal)(set_color brmagenta) 		# Définir le formatage du texte en magenta foncé
set scbrmi 		(set_color normal)(set_color -i brmagenta) 		# Définir le formatage du texte en magenta foncé italique
set scbrmu 		(set_color normal)(set_color -u brmagenta) 		# Définir le formatage du texte en magenta foncé souligné
set scbrmo 		(set_color normal)(set_color -o brmagenta) 		# Définir le formatage du texte en magenta foncé gras
set scbrmio 	(set_color normal)(set_color -io brmagenta) 	# Définir le formatage du texte en magenta foncé italique gras
# Magenta foncé défini #7606B2
set scbrmm		(set_color normal)(set_color $mgnt) 			# Définir le formatage du texte en magenta foncé défini
set scbrmmi 	(set_color normal)(set_color -i $mgnt) 			# Définir le formatage du texte en magenta foncé défini italique
set scbrmmu 	(set_color normal)(set_color -u $mgnt) 			# Définir le formatage du texte en magenta foncé défini souligné
set scbrmmo 	(set_color normal)(set_color -o $mgnt) 			# Définir le formatage du texte en magenta foncé défini gras
set scbrmmio 	(set_color normal)(set_color -io $mgnt) 		# Définir le formatage du texte en magenta foncé défini italique gras
# Rouge foncé #91273A
set scbrr		(set_color normal)(set_color brred) 			# Définir le formatage du texte en rouge foncé
set scbrri 		(set_color normal)(set_color -i brred) 			# Définir le formatage du texte en rouge foncé italique
set scbrru 		(set_color normal)(set_color -u brred) 			# Définir le formatage du texte en rouge foncé souligné
set scbrro 		(set_color normal)(set_color -o brred) 			# Définir le formatage du texte en rouge foncé gras
set scbrrio 	(set_color normal)(set_color -io brred) 		# Définir le formatage du texte en rouge foncé italique gras
# Blanc foncé #A1A1A1
set scbrw		(set_color normal)(set_color brwhite) 			# Définir le formatage du texte en blanc foncé
set scbrwi 		(set_color normal)(set_color -i brwhite) 		# Définir le formatage du texte en blanc foncé italique
set scbrwu 		(set_color normal)(set_color -u brwhite) 		# Définir le formatage du texte en blanc foncé souligné
set scbrwo 		(set_color normal)(set_color -o brwhite) 		# Définir le formatage du texte en blanc foncé gras
set scbrwio 	(set_color normal)(set_color -io brwhite) 		# Définir le formatage du texte en blanc foncé italique gras
# Jaune #B29506
set scbry		(set_color normal)(set_color bryellow) 			# Définir le formatage du texte en jaune foncé
set scbryi 		(set_color normal)(set_color -i bryellow) 		# Définir le formatage du texte en jaune foncé italique
set scbryu 		(set_color normal)(set_color -u bryellow) 		# Définir le formatage du texte en jaune foncé souligné
set scbryo 		(set_color normal)(set_color -o bryellow) 		# Définir le formatage du texte en jaune foncé gras
set scbryio 	(set_color normal)(set_color -io bryellow) 		# Définir le formatage du texte en jaune foncé italique gras
# Orange #E28513
set scbro		(set_color normal)(set_color $brorg) 			# Définir le formatage du texte en orange foncé
set scbroi 		(set_color normal)(set_color -i $brorg) 		# Définir le formatage du texte en orange foncé italique
set scbrou 		(set_color normal)(set_color -u $brorg) 		# Définir le formatage du texte en orange foncé souligné
set scbroo 		(set_color normal)(set_color -o $brorg) 		# Définir le formatage du texte en orange foncé gras
set scbroio 	(set_color normal)(set_color -io $brorg) 		# Définir le formatage du texte en orange foncé italique gras

## Run fastfetch if session is interactive
if status --is-interactive && type -q fastfetch
   fastfetch --load-config neofetch
end

string match -q "$TERM_PROGRAM" "vscode"
and . (code --locate-shell-integration-path fish)
