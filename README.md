# Description

Une série de scripts personnels pour le 🐠 shell FISH 🐠 avec leur complétion.
Contient également les fichiers de configurations ainsi que l'arborescence utilisée

# Contenu

## Arborescence

1. completions
2. conf.d
3. functions

## Fonctions

- **__by_pass_fish_aliases.fish** → Permet de bypasser les alias configuré pour utiliser directement les commandes de base. Pour cela, utilise un raccourci clavier dans le shell
- **__convert_profile.fish** → Permet de convertir les exports d'un fichier ~/.profile en fish dans un fichier ~/.fish_profile
- **aliases.fish** → Fonction permettant d'étendre et de mettre en forme la commande '$ alias' ou son équivalent '$ functions -ndv'
- **copycat.fish** → Fonction permettant de copier dans le presse-papier le résultat de la commande qui suit
- **ext.fish** → Fonction permettant de raturer, doublement souligner, surligner ou faire clignoter un texte
- **fish_ssh_agent.fish** → Script permettant de définir automatiquement l'agent SSH dans FISH
- **fixgpgme.fish** → Fonction permettant de réparer l'erreur courante GPGME qui apparaît notamment suite à un problème de réseau lors de la mise-à-jour des dépôts
- **lt.fish** → Commande ls avec de nombreux détails et en arborescence
- **outils.fish** → Fonction servant de pense-bête permettant d'afficher une liste de certains utilitaires pratique à utiliser
- **search.fish** → Fonction servant à rechercher une chaîne de caractère dans les fichiers en utilisant skim et ag
- **stegimg.fish** → Fonction utilisant le bibliothèque Python stego-lsb et  permettant de cacher un document dans une image par stéganographie
- **testee.fish** → Fonction permettant de tester certaines fonctionnalité de FISH
- **tradc.fish** → Fonction utilisant la bibliothèque Python deepl-translate et permettant de traduire un texte dans différents langages et de le copier par la même occasion dans le presse-papier

## Fichiers anexes

- _aliases.fish_ → Fichier contenant tous mes aliases (au lieu de se retrouver dans fish.config
- _fish_variables.fish_ → Fichier contenant toutes les variables (notamment celles permettant de coloriser le prompt
- _bindings.fish_ → Fichier contenant les raccourcis clavier et leur fonctions associées
  - [x] TODO :: Déplacer ces fonctions et définitions préentent pour l'instant dans **_config.fish_**

# À faire

- [x] Déplacer les raccourcis clavier et leurs fonctions associées présentes dans **__config.fish__** dans **__bindings.fish__**
